//
//  AlertManager.swift
//  KOL
//
//  Created by Rupak on 17/08/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

struct CustomAction {
    var index = 0
    var title: String?
    var style: UIAlertAction.Style = .default
}

class AlertManager: NSObject {
    
    static func showAlertWith(title: String?, message: String?, actions: [CustomAction] = [],_ completion: ((_ action: CustomAction) -> Void)?) {
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            alertController.addAction(UIAlertAction.init(title: action.title, style: action.style, handler: { (alertAction) in
                completion?(action)
            }))
        }
        if actions.count == 0 {
            alertController.addAction(UIAlertAction(title: "Ok".localized, style: .cancel, handler: nil))
        }
        
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
}
