//
//  RequestManager.swift
//  KOL
//
//  Created by Rupak on 17/08/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import Alamofire

class RequestManager: SessionManager {
    
    static let manager: RequestManager = {
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "api.kol.com.sa": .disableEvaluation
        ]
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 30.0
        
        return RequestManager(configuration: configuration, serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }()
    
    override func request(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil)
        -> DataRequest
    {
        
        var originalRequest: URLRequest?
        
        do {
            let currentUrl = AppConstants.BaseURL.Current
            var urlString = url as! String
            if let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                urlString = encodedString
            }
            originalRequest = try URLRequest(url: URL.init(string: urlString, relativeTo: URL(string: currentUrl))!, method: method, headers: headers)
            
            let encodedURLRequest = try encoding.encode(originalRequest!, with: parameters)
            return request(encodedURLRequest)
        } catch {
            return request(originalRequest!)
        }
    }
}
