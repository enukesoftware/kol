//
//  TaskManager.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import AVKit
import Contacts
import CoreLocation

struct ContactDetails {
    var name: String?
    var numbers = [String]()
    var kolUserDetails: GroupContactDetails?
    
    init() {}
    init(name: String, numbers: [String]) {
        self.name = name
        self.numbers = numbers
    }
    
    init(contact: CNContact) {
        name = contact.givenName + " " + contact.familyName
        numbers = []
        for phone in contact.phoneNumbers {
            numbers.append(phone.value.stringValue)
        }
    }
    
    static func getContacts(contact: CNContact) -> [ContactDetails] {
        var contacts = [ContactDetails]()
        let contactName = contact.givenName + " " + contact.familyName
        for phone in contact.phoneNumbers {
            var contact = ContactDetails()
            contact.name = contactName
            contact.numbers = [phone.value.stringValue]
            contacts.append(contact)
        }
        return contacts
    }
}

class TaskManager: NSObject {
    static let shared = TaskManager()
    
    var imagePickerAlert = UIAlertController(title: "Choose image from", message: nil, preferredStyle: .actionSheet)
    var imagePicker = UIImagePickerController()
    var pickImageCallback : ((UIImage) -> ())?
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var getLocationCallback : ((CLLocation?) -> ())?
    var getLatestLocation = false
    
    static func moveToViewController(with controllerClass: AnyClass) {
        if let controllers = (UIApplication.topViewController as? UINavigationController)?.viewControllers {
            for controller in controllers {
                if controller.isKind(of: controllerClass) == true {
                    (UIApplication.topViewController as? UINavigationController)?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    static func updateRefreshTokenIfNeeded(_ completion: @escaping(_ needed: Bool) -> Void) {
        
        if let fetchedTime = DataManager.shared.loginDetails?.fetchedTime.convertDate(AppConstants.DateFormate.Default), let tokenValidity = DataManager.shared.loginDetails?.accessTokenLifetime, Date().timeIntervalSince(fetchedTime) > Double(tokenValidity-10) {
            RequestManager.getRefreshToken(completion: { (success) in
                completion(success)
            }) { (error, errorCode, message) in
                completion(false)
            }
        } else {
            completion(true)
        }
    }
}

//MARK: Image picker and camera related
extension TaskManager: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func pickImage(source: UIView? = nil, _ completion: @escaping(_ image: UIImage?) -> Void) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            pickImageCallback = completion
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            let cameraAction = UIAlertAction(title: "Camera", style: .default){
                UIAlertAction in
                self.openCamera()
            }
            let gallaryAction = UIAlertAction(title: "Gallary", style: .default){
                UIAlertAction in
                self.openGallery()
            }
            let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            
            if imagePickerAlert.actions.count == 0 {
                imagePickerAlert.addAction(cameraAction)
                imagePickerAlert.addAction(gallaryAction)
                imagePickerAlert.addAction(cancelAction)
            }
            
            if let presentationController = imagePickerAlert.popoverPresentationController {
                presentationController.sourceView = source
                presentationController.sourceRect = source?.bounds ?? CGRect.zero
            }
            
            UIApplication.topViewController()?.present(imagePickerAlert, animated: true, completion: nil)
        }
    }
    
    func openCamera(){
        imagePickerAlert.dismiss(animated: true, completion: nil)
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            imagePicker.sourceType = .camera
            UIApplication.topViewController()?.present(imagePicker, animated: true, completion: nil)
        } else {
            AppUtility.showAlertWithProperty("Warning", messageString: "Sorry, camera not available.")
        }
    }
    
    func openGallery(){
        imagePickerAlert.dismiss(animated: true, completion: nil)
        imagePicker.sourceType = .photoLibrary
        UIApplication.topViewController()?.present(imagePicker, animated: true, completion: {
            self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.isEnabled = true
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            pickImageCallback?(image)
        }
    }
    
    static func checkCameraPermission() {
        let mediaType = AVMediaType.video
        let authStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
        if authStatus == .authorized {
            // have camera permission
        } else if authStatus == .denied {
            showCameraPermissionAlert()
        } else if authStatus == .restricted {
            showCameraPermissionAlert()
        } else if authStatus == .notDetermined {
            AVCaptureDevice.requestAccess(for: mediaType, completionHandler: { granted in
                if granted {
                    print("Granted access to \(mediaType)")
                } else {
                    print("Not granted access to \(mediaType)")
                    self.showCameraPermissionAlert()
                }
            })
        } else {
            // impossible, unknown authorization status
        }
    }
    
    static func showCameraPermissionAlert() {
        let alertController = UIAlertController.init(title: "Alert", message: "Sorry, please provide camera access to proceed rest operations, thanks.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Ok".localized, style: .default, handler: { (alertAction) in
            if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }))
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
}

//MARK: Contact list related
extension TaskManager {
    
    static func requestContactAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            TaskManager.showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            CNContactStore().requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        TaskManager.showSettingsAlert(completionHandler)
                    }
                }
            }
        @unknown default:
            fatalError()
        }
    }
    
    static func getContactList(completion: @escaping(_ contacts: [ContactDetails]) -> Void) {
        var contacts = [ContactDetails]()
        requestContactAccess { (accessGranted) in
            if accessGranted {
                let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),CNContactPhoneNumbersKey] as [Any]
                let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
                
                do {
                    try CNContactStore().enumerateContacts(with: request) {
                        (contact, stop) in
                        // Array containing all unified contacts from everywhere
                        contacts.append(contentsOf: ContactDetails.getContacts(contact: contact))
                    }
                }
                catch {
                    print("unable to fetch contacts")
                }
                completion(contacts.filter({$0.numbers.count != 0 && $0.name != " "}))
            } else {
                completion(contacts)
            }
        }
    }
    
    static func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Would you like to open settings and grant permission to contacts?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
            completionHandler(false)
            if let url = URL(string: UIApplication.openSettingsURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        UIApplication.topViewController()?.present(alert, animated: true)
    }
}

//MARK: Google place api and location related
extension TaskManager: CLLocationManagerDelegate {
    
    func locationAuthorizationStatus(_ completion: @escaping(_ authorized: Bool) -> Void) {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            completion(false)
            break
        case .restricted, .denied:
            completion(false)
            break
        case .authorizedWhenInUse, .authorizedAlways:
            completion(true)
            break
        }
    }
    
    func requestForAccessingLocation(_ completion: @escaping(_ location: CLLocation?) -> Void) {
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        if currentLocation != nil {
            completion(currentLocation)
        } else {
            getLatestLocation = false
            getLocationCallback = completion
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocation = manager.location else { return }
        currentLocation = location
        if !getLatestLocation {
            getLocationCallback?(currentLocation)
            getLatestLocation = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.locationServicesEnabled() {
            switch status {
            case .restricted, .denied:
                print("No access")
                getLocationCallback?(nil)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            case .notDetermined:
                print("Access not determined")
            }
        } else {
            print("\n\nLocation services are not enabled\n\n")
            getLocationCallback?(nil)
        }
    }
    
    func getAddressFromLatLon(coordinate: CLLocationCoordinate2D,_ completion: @escaping(_ address: String?) -> Void) {
        let geocoder: CLGeocoder = CLGeocoder()
        let location: CLLocation = CLLocation(latitude:coordinate.latitude, longitude: coordinate.longitude)
        geocoder.reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let placemark = placemarks?.first, let country = placemark.country {
                    var city = ""
                    if let locality = placemark.locality {
                        city = locality + ","
                    }
                    let address = city + country
                    completion(address)
                } else {
                    completion(nil)
                }
        })
    }
}

//MARK: Notification related
extension TaskManager {
    
    static func openTappedNotificationScreen() {
        if let notification = DataManager.shared.haveToShowNotificationDetails {
            dismissAlertIfAvailable { (success) in
                if notification.notificationAction == "NEW_FOOD_ITEM_INVITATION" {
                    if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AcceptSharedViewController") as? AcceptSharedViewController {
                        controller.modalPresentationStyle = .overFullScreen
                        controller.modalTransitionStyle = .crossDissolve
                        controller.notificationDetails = notification
                        controller.takenAction = {(accepted) in
                            if UIApplication.topViewController()?.isKind(of: ShopDetailsViewController.classForCoder()) == true || UIApplication.topViewController()?.isKind(of: ProductDetailsViewController.classForCoder()) == true {
                                if let controller = UIApplication.topViewController() as? ShopDetailsViewController {
                                    controller.loadInitialApiContentsAndUpdateUI()
                                } else if let controller = UIApplication.topViewController() as? ProductDetailsViewController {
                                    controller.updateProductWithItemDetails()
                                    controller.refreshOrderItems()
                                }
                            }
                        }
                        if UIApplication.topViewController()?.isKind(of: AcceptSharedViewController.classForCoder()) == false {
                            UIApplication.topViewController()?.present(controller, animated: true, completion: nil)
                        }
                    }
                } else if notification.notificationAction == "NEW_INVITATION" {
                    if let controller = Storyboard.controllerWith(name: "InvitationAlertViewController") as? InvitationAlertViewController {
                        if UIApplication.topViewController()?.isKind(of: InvitationAlertViewController.classForCoder()) == false {
                            controller.modalPresentationStyle = .overFullScreen
                            controller.modalTransitionStyle = .crossDissolve
                            controller.notificationDetails = notification
                            UIApplication.topViewController()?.present(controller, animated: true, completion: nil)
                            controller.takenAction = { (accepted, object) in
                                if accepted {
                                    if let controller = Storyboard.controllerWith(name: "ShopDetailsViewController") as? ShopDetailsViewController {
                                        controller.tableScanDetails = DashboardTableScanDetails(restaurantId: object?.restaurantId, tableId: object?.order)
                                        
                                        if UIApplication.topViewController()?.isKind(of: ShopDetailsViewController.classForCoder()) == false {
                                            UIApplication.topViewController()?.navigationController?.pushViewController(controller, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if notification.notificationAction == "NEW_ITEM" || notification.notificationAction == "ORDER_ITEM_EDITED" || notification.notificationAction == "FOOD_ITEM_INVITATION_ACCEPTED" || notification.notificationAction == "REMOVED_ITEM" || notification.notificationAction == "FOOD_ITEMS_CONFIRMED" {
                    if UIApplication.topViewController()?.isKind(of: ShopDetailsViewController.classForCoder()) == true || UIApplication.topViewController()?.isKind(of: ProductDetailsViewController.classForCoder()) == true {
                        if let controller = UIApplication.topViewController() as? ShopDetailsViewController {
                            controller.refreshOrderItems { (success) in}
                        } else if let controller = UIApplication.topViewController() as? ProductDetailsViewController {
                            controller.refreshOrderItems()
                        }
                    }
                } else if notification.notificationAction == "ORDER_LEFT" || notification.notificationAction == "INVITATION_ACCEPTED" {
                    if UIApplication.topViewController()?.isKind(of: ShopDetailsViewController.classForCoder()) == true || UIApplication.topViewController()?.isKind(of: ProductDetailsViewController.classForCoder()) == true {
                        if let controller = UIApplication.topViewController() as? ShopDetailsViewController {
                            controller.loadInitialApiContentsAndUpdateUI()
                        } else if let controller = UIApplication.topViewController() as? ProductDetailsViewController {
                            controller.updateProductWithItemDetails()
                            controller.refreshOrderItems()
                        }
                    }
                } else if notification.notificationAction == "ORDER_ALL_BILL_PAID" {
                    dismissAlertIfAvailable { (success) in
                        if let controller = Storyboard.controllerWith(name: "RatingViewController") as? RatingViewController {
                            controller.orderId = notification.orderId
                            UIApplication.topViewController()?.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                } else if notification.notificationAction == "ORDER_MARKED_AS_CHECKOUT_CUSTOMER" {
                    if let controller = Storyboard.controllerWith(name: "CheckoutViewController") as? CheckoutViewController {
                        controller.orderDetails = DataManager.shared.runningOrderDetails
                        UIApplication.topViewController()?.navigationController?.pushViewController(controller, animated: true)
                    }
                } else if notification.notificationAction == "ORDER_REJECTED" {
                    AppUtility.showAlertWithProperty((notification.title ?? "").localized, messageString: (notification.body ?? "").localized)
                } else if notification.notificationAction == "ORDER_SINGLE_USER_PAID" {
                    if UIApplication.topViewController()?.isKind(of: PaymentSelectionViewController.classForCoder()) == true {
                        if let controller = UIApplication.topViewController() as? PaymentSelectionViewController {
                            controller.updateAndRefreshPaymentList()
                        }
                    } else {
                        DataManager.shared.shouldUpdatePaymentSelection = true
                    }
                }
                DataManager.shared.haveToShowNotificationDetails = nil
            }
        }
    }
    
    static func dismissAlertIfAvailable(_ completion: @escaping(_ success : Bool) -> Void) {
        if UIApplication.topViewController()?.isKind(of: UIAlertController.classForCoder()) == true {
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                completion(true)
            })
        } else {
            completion(true)
        }
    }
}


//MARK: Request related
extension TaskManager {
    
    static func loadUserDetails(_ showLoading: Bool = false, _ completion: @escaping(_ success: Bool) -> Void) {
        if let id = DataManager.shared.loginDetails?.user?.first?.id {
            if showLoading {AppUtility.showProgress(nil, title: nil)}
            RequestManager.getUserDetails(id: id, completion: { (object) in
                if showLoading {AppUtility.hideProgress(nil)}
                completion(object?.id != nil)
            }) { (error, code, message) in
                if showLoading {AppUtility.hideProgress(nil)}
                if let errorMessage = message {
                    AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
                }
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    
    static func getRestaurantDetails(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ object: RestaurantDetails?) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getRestaurantDetails(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
    
    static func getRestaurantFoodItemDetails(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ object: FoodItemDetails?) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getRestaurantFoodItemDetails(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
    
    static func getOrderParticipants(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ object: [ParticipantDetails]) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getOrderParticipants(params: params, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion([])
        }
    }
    
    static func getActiveOrderDetails(_ showLoading: Bool = true, _ completion: @escaping(_ object: ActiveOrderDetails?) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getActiveOrder(completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
    
    static func getOrderDetails(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ object: OrderDetails?) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getOrderDetailsBy(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
    
    static func inviteOrder(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.inviteOrder(params: params, completion: { (success) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    static func leaveOrder(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.leaveOrder(id: id, completion: { (success) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    static func getOrderItems(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ object: [OrderItemDetails]) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getOrderItems(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion([])
        }
    }
    
    static func cancelOrderItem(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.cancelOrderItem(id: id, completion: { (success) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    static func confirmOrder(_ showLoading: Bool = true, id: Int, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.confirmOrder(id: id, params: params, completion: { (success) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    static func getInvoiceDetails(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ object: InvoiceDetails?) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getInvoiceDetails(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
}
