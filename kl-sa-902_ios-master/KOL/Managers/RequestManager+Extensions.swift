//
//  RequestManager+Extensions.swift
//  KOL
//
//  Created by Rupak on 17/08/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

extension RequestManager {

    static func loginWith(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        
        RequestManager.manager.request(AppConstants.Api.Auth.login, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let responseValue = response.result.value {
                    let json = JSON(responseValue)
                    if let objectDictionary = json.dictionaryObject, let object = Mapper<LoginDetails>().map(JSON: objectDictionary) {
                        
                        DataManager.shared.loginDetails = object
                        AuthManager.saveLoginDetails()
                        completion(true)
                    } else {
                        completion(false)
                    }
                } else {
                    completion(false)
                }
                break
            case .failure(let error):
                failure(error,response.response?.statusCode,getErrorMessage(response: response, false))
            }
        }
    }
    
    static func getRefreshToken(completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        let params: [String: Any] = ["refresh" : DataManager.shared.loginDetails?.refresh ?? ""]
        
        RequestManager.manager.request(AppConstants.Api.Auth.refreshToken, method: .post, parameters: params, encoding: URLEncoding(), headers: nil).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let responseValue = response.result.value {
                    let json = JSON(responseValue)
                    if let resultDictionary = json.dictionaryObject, let refreshToken = Mapper<RefreshTokenDetails>().map(JSON: resultDictionary) {
                        DataManager.shared.loginDetails?.access = refreshToken.access
                        DataManager.shared.loginDetails?.refresh = refreshToken.refresh
                        DataManager.shared.loginDetails?.fetchedTime = Date().dateStringWithFormat(AppConstants.DateFormate.Default, local: false)
                        AuthManager.saveLoginDetails()
                        completion(true)
                    } else {
                        completion(false)
                    }
                } else {
                    completion(false)
                }
                break
            case .failure(let error):
                failure(error,response.response?.statusCode,getErrorMessage(response: response))
            }
        }
    }
    
    static func registerWith(modify: Bool = false, params: [String:Any], fileDic: [[String:FileDetails]], completion: @escaping(_ registerDetails: RegisterResponseDetails?, _ userDetails: UserDetails?) -> Void, failure: @escaping (_ error: Error?, _ message: String?) -> Void) {
        
        var urlString = AppConstants.BaseURL.Current
        if modify {
            urlString += String(format: AppConstants.Api.User.update, DataManager.shared.loginDetails?.user?.first?.id ?? 0)
        } else {
            urlString += AppConstants.Api.Auth.regitration
        }
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil, nil)
                return
            }
            requestWith(modify: modify, sendHeader: false, reqUrl: urlString, filesDic: fileDic, parameters: params, onCompletion: { (responseJSON) in
                if let responseValue = responseJSON {
                    if !modify, let objectDictionary = responseValue.dictionaryObject, let object = Mapper<RegisterResponseDetails>().map(JSON: objectDictionary), object.user?.id != nil {
                        completion(object, nil)
                    } else if modify, let responseDictionary = responseValue.dictionaryObject?["user"] as? [String:Any], let object = Mapper<UserDetails>().map(JSON: responseDictionary), object.id != nil {
                        completion(nil, object)
                    } else {
                        if let responseDictionary = responseValue.dictionaryObject, responseDictionary.count > 0 {
                            var errorMessage = ""
                            for key in responseDictionary.keys {
                                if let messageArray = responseDictionary[key] as? [String] {
                                    for message in messageArray {
                                        errorMessage += message + "\n"
                                    }
                                } else if let messageDic = responseDictionary[key] as? [String:Any] {
                                    for key in messageDic.keys {
                                        if let messageArray = messageDic[key] as? [String] {
                                            for message in messageArray {
                                                errorMessage += message + "\n"
                                            }
                                        }
                                    }
                                }
                            }
                            if errorMessage != "" {
                                failure(nil,errorMessage)
                            } else {
                                completion(nil, nil)
                            }
                        } else {
                            completion(nil, nil)
                        }
                    }
                } else {
                    completion(nil, nil)
                }
            }, onError: { (error) in
                failure(error,nil)
            })
        }
    }
    
    static func verifyAccount(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        RequestManager.manager.request(AppConstants.Api.Auth.verifyAccount, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                completion(true)
                break
            case .failure(let error):
                failure(error,response.response?.statusCode,getErrorMessage(response: response))
            }
        }
    }
    
    static func getUserDetails(id: Int,completion: @escaping(_ object: UserDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Auth.userDetails, id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<UserDetails>().map(JSON: objectDictionary) {
                            if object.id != nil {
                                DataManager.shared.loginDetails?.user = [object]
                                if let code = objectDictionary["qr_code"] as? String {
                                    DataManager.shared.loginDetails?.customer?.qrCode = code
                                }
                                AuthManager.saveLoginDetails()
                            }
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: User related methods
extension RequestManager {
    
    static func forgotPassword(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        RequestManager.manager.request(AppConstants.Api.User.forgotPassword, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                completion(true)
                break
            case .failure(let error):
                failure(error,response.response?.statusCode,getErrorMessage(response: response))
            }
        }
    }
    
    static func forgotPasswordPhoneVerify(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        RequestManager.manager.request(AppConstants.Api.User.forgotPasswordVerifyAccount, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                completion(true)
                break
            case .failure(let error):
                failure(error,response.response?.statusCode,getErrorMessage(response: response))
            }
        }
    }
    
    static func resendVerification(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        RequestManager.manager.request(AppConstants.Api.User.resendVerificationCode, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                completion(true)
                break
            case .failure(let error):
                failure(error,response.response?.statusCode,getErrorMessage(response: response))
            }
        }
    }
    
    static func resetPassword(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        RequestManager.manager.request(AppConstants.Api.User.resetPassword, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                completion(true)
                break
            case .failure(let error):
                failure(error,response.response?.statusCode,getErrorMessage(response: response))
            }
        }
    }
    
    static func changePassword(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.User.changePassword, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func changePhone(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.User.changePhone, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func changePhoneVerify(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.User.changePhoneVerify, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: Tickets related methods
extension RequestManager {
    
    static func getTicketList(completion: @escaping(_ object: TicketListDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Ticket.list, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<TicketListDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func raiseTicket(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Ticket.raise, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: Restaurant methods
extension RequestManager {
    
    static func getRestaurantListByLocation(lat: Double, long: Double, completion: @escaping(_ object: RestaurantListDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Restaurant.listByLocation, lat, long), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<RestaurantListDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getRestaurantListByKeyword(keyword: String, completion: @escaping(_ object: RestaurantListDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Restaurant.listByKeyword, keyword), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<RestaurantListDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getRestaurantDetails(id: Int,completion: @escaping(_ object: RestaurantDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Restaurant.details, id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<RestaurantDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getRestaurantFoodItems(id: Int, extraArgument: String = "", completion: @escaping(_ object: FoodItemListDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            var urlString = String(format: AppConstants.Api.Restaurant.foodItems, id)
            if extraArgument != "" {
                if AppUtility.isValidUrl(urlString: extraArgument) {
                    urlString = extraArgument
                } else {
                    urlString += extraArgument
                }
            }
            RequestManager.manager.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<FoodItemListDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getRestaurantFoodItemDetails(id: Int, completion: @escaping(_ object: FoodItemDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Restaurant.foodItemDetails, id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<FoodItemDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getRestaurantFoodCategories(id: Int, completion: @escaping(_ object: FoodCategoryListDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Restaurant.foodCategories, id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<FoodCategoryListDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: Order related methods
extension RequestManager {
    
    static func createOrder(params: [String:Any], completion: @escaping(_ object: OrderDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Order.create, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<OrderDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getOrderParticipants(params: [String:Any], completion: @escaping(_ object: [ParticipantDetails]) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion([])
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Order.participants, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDic = json.dictionaryObject, let object = Mapper<ParticipantListDetails>().map(JSON: objectDic)?.users {
                            completion(object)
                        } else {
                            completion([])
                        }
                    } else {
                        completion([])
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getOrderDetailsBy(id: Int, completion: @escaping(_ object: OrderDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Order.details, id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<OrderDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getActiveOrder(completion: @escaping(_ object: ActiveOrderDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Order.activeOrder, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDic = (json.dictionaryObject?["results"] as? [Any])?.first as? [String:Any], let object = Mapper<ActiveOrderDetails>().map(JSON: objectDic) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func inviteOrder(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
        
        RequestManager.manager.request(AppConstants.Api.Order.invite, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                completion(true)
                break
            case .failure(let error):
                failure(error,response.response?.statusCode,getErrorMessage(response: response))
            }
        }
    }
    
    static func leaveOrder(id: Int, completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            let params: [String:Any] = ["sure" : true]
            RequestManager.manager.request(String(format: AppConstants.Api.Order.leave, id), method: .delete, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func acceptInvitation(id: String,params: [String:Any], completion: @escaping(_ object: AcceptOrderResponseDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Order.acceptInvitation, id), method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<AcceptOrderResponseDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func addProductToCart(params: [String:Any], completion: @escaping(_ object: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Order.addToCart, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        completion(true)
                    } else {
                        completion(false)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func updateCartItem(id: Int, params: [String:Any], completion: @escaping(_ object: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Order.updateCartItem, id), method: .patch, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        completion(true)
                    } else {
                        completion(false)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getOrderItems(id: Int, completion: @escaping(_ object: [OrderItemDetails]) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion([])
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Order.items, id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let array = json.dictionaryObject?["results"] as? [Any], let object = Mapper<OrderItemDetails>().mapArray(JSONObject: array) {
                            completion(object)
                        } else {
                            completion([])
                        }
                    } else {
                        completion([])
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func confirmOrder(id: Int, params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Order.confirmOrder, id), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func orderItemInvite(params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Order.orderItemInvite, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func acceptOrderItem(id: String, params: [String:Any], completion: @escaping(_ object: AcceptOrderResponseDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Order.acceptOrderItem, id), method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        print(json)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<AcceptOrderResponseDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func cancelOrderItem(id: Int, completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Order.cancelOrderItem, id), method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func orderRating(params: [String:Any], completion: @escaping(_ object: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Order.rating, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: Invoice related methods
extension RequestManager {
    
    static func getInvoiceDetails(id: Int, completion: @escaping(_ object: InvoiceDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Invoice.details, id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<InvoiceDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getTransactionDetails(params: [String:Any], completion: @escaping(_ object: TransactionResponseDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Invoice.transactionDetails, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<TransactionResponseDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func verifyTransaction(id: String, completion: @escaping(_ status: Int?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let params: [String:Any] = ["transaction_id" : id]
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Invoice.transactionVerification, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let value = json.dictionaryObject?["transaction_status"] as? Int {
                            completion(value)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getInvoiceListDetails(argument: String, completion: @escaping(_ object: InvoiceListDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            var urlString = AppConstants.Api.Invoice.list
            if argument != "" {
                urlString += argument
            }
            RequestManager.manager.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<InvoiceListDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func reportInvoice(params: [String:Any], completion: @escaping(_ object: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Invoice.reportIssue, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func createInvoice(params: [String:Any], completion: @escaping(_ object: InvoiceDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Invoice.create, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<InvoiceDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: Table related methods
extension RequestManager {
    
    static func getTableInfo(id: Int, completion: @escaping(_ object: TableInfoDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Restaurant.tableInfo, id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<TableInfoDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: Contact related methods
extension RequestManager {
    
    static func syncContactList(params: [String: Any], completion: @escaping(_ object: [GroupContactDetails]) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion([])
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Contact.sync, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let array = json.arrayObject, let object = Mapper<GroupContactDetails>().mapArray(JSONObject: array) {
                            completion(object)
                        } else {
                            completion([])
                        }
                    } else {
                        completion([])
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getKOLGroupList(completion: @escaping(_ object: ContactGroupListDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Contact.groupList, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let objectDictionary = json.dictionaryObject, let object = Mapper<ContactGroupListDetails>().map(JSON: objectDictionary) {
                            completion(object)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func createContactGroup(params: [String:Any], completion: @escaping(_ object: Int?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.Contact.createGroup, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let id = json.dictionaryObject?["id"] as? Int {
                            completion(id)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func updateContactGroupName(id: Int, params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Contact.deleteGroup, id), method: .patch, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func updateContactGroupContacts(id: Int, params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Contact.updateGroupContacts, id), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func addContactsIntoGroup(id: Int, params: [String:Any], completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Contact.addContacts, id), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func deleteContactGroup(id: Int, completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.Contact.deleteGroup, id), method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: Notification methods
extension RequestManager {
    
    static func subscribeNotification(params: [String:Any], completion: @escaping(_ fcm: FCMDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(AppConstants.Api.General.registerNotification, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let resultDictionary = json.dictionaryObject, let fcmDetails = Mapper<FCMDetails>().map(JSON: resultDictionary) {
                            completion(fcmDetails)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func updateNotificationStatus(id: String, bodyObject: [String:Any], completion: @escaping(_ fcm: FCMDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.General.updateNotification, id), method: .put, parameters: bodyObject, encoding: URLEncoding(), headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let resultDictionary = json.dictionaryObject, let fcmDetails = Mapper<FCMDetails>().map(JSON: resultDictionary) {
                            completion(fcmDetails)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func unsubscribeNotification(registerId: String, completion: @escaping(_ success: Bool) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        let requestUrl = AppConstants.Api.General.deleteNotification + "\(registerId)/"
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(false)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(requestUrl, method: .delete, parameters: nil, encoding: URLEncoding(), headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(true)
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
    
    static func getFCMDetails(id: String, completion: @escaping(_ fcm: FCMDetails?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
            RequestManager.manager.request(String(format: AppConstants.Api.General.updateNotification, id), method: .get, parameters: nil, encoding: URLEncoding(), headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let resultDictionary = json.dictionaryObject, let fcmDetails = Mapper<FCMDetails>().map(JSON: resultDictionary) {
                            completion(fcmDetails)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}

//MARK: Generic methods
extension RequestManager {
    
    static func getErrorMessage(response: DataResponse<Any>?,_ shouldRedirect: Bool = true) -> String? {
        var errorMessage: String?
        if response?.response?.statusCode == 401 && shouldRedirect {
            AuthManager.logout()
            (UIApplication.shared.delegate as? AppDelegate)?.instantiateAppropriateViewController()
        } else if let data = response?.data {
            let responsJSON = JSON(data)
            print(responsJSON)
            if let message = responsJSON["detail"].string {
                errorMessage = message.localized
            } else if let message = responsJSON["error_description"].string {
                errorMessage = message.localized
            } else {
                var error = ""
                if let dictionary = responsJSON.dictionaryObject {
                    for key in dictionary.keys {
                        if let messageArray = dictionary[key] as? [String], messageArray.count > 0 {
                            error += messageArray[0].localized+"\n"
                        } else if let message = dictionary[key] as? String {
                            error += message.localized+"\n"
                        } else if let innerDic = dictionary[key] as? [String:Any] {
                            for key in innerDic.keys {
                                if let messageArray = innerDic[key] as? [String], messageArray.count > 0 {
                                    error += messageArray[0].localized+"\n"
                                } else if let message = innerDic[key] as? String {
                                    error += message.localized+"\n"
                                }
                            }
                        }
                    }
                }
                if error != "" {
                    errorMessage = error.localized
                }
            }
        }
        
        return errorMessage?.localized
    }
    
    static func requestWith(modify: Bool = false, sendHeader: Bool = true, reqUrl: String, filesDic: [[String:FileDetails]], parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        if modify {
            TaskManager.updateRefreshTokenIfNeeded { (success) in
                if !success {
                    onCompletion?(nil)
                    return
                }
                let headers: HTTPHeaders = ["Authorization" : DataManager.shared.fullToken]
                RequestManager.manager.upload(multipartFormData: { (multipartFormData) in
                    for (key, value) in parameters {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                    for fileDetails in filesDic {
                        for (key,value) in fileDetails {
                            if let fileData = value.fileData {
                                multipartFormData.append(fileData, withName: key, fileName: value.fileName, mimeType: value.mimeType)
                            }
                        }
                    }
                    
                }, usingThreshold: UInt64.init(), to: reqUrl, method: modify ? .patch : .post, headers: modify ? headers : sendHeader ? headers : nil) { (result) in
                    switch result{
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print("Succesfully uploaded")
                            if let err = response.error {
                                onError?(err)
                                return
                            } else {
                                if let value = response.result.value {
                                    onCompletion?(JSON(value))
                                } else {
                                    onCompletion?(nil)
                                }
                            }
                        }
                    case .failure(let error):
                        print("Error in upload: \(error.localizedDescription)")
                        onError?(error)
                    }
                }
            }
        } else {
            RequestManager.manager.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                for fileDetails in filesDic {
                    for (key,value) in fileDetails {
                        if let fileData = value.fileData {
                            multipartFormData.append(fileData, withName: key, fileName: value.fileName, mimeType: value.mimeType)
                        }
                    }
                }
                
            }, usingThreshold: UInt64.init(), to: reqUrl, method: modify ? .patch : .post, headers: nil) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        if let err = response.error {
                            onError?(err)
                            return
                        } else {
                            if let value = response.result.value {
                                onCompletion?(JSON(value))
                            } else {
                                onCompletion?(nil)
                            }
                        }
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    onError?(error)
                }
            }
        }
    }
    
    static func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    static func getSelectionList(urlString: String, completion: @escaping(_ selection: [IDName]?) -> Void, failure: @escaping (_ error: Error?,_ errorCode: Int?, _ message: String?) -> Void) {
        
        TaskManager.updateRefreshTokenIfNeeded { (success) in
            if !success {
                completion(nil)
                return
            }
            RequestManager.manager.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        if let resultArray = json.arrayObject, let selectionList = Mapper<IDName>().mapArray(JSONObject: resultArray) {
                            completion(selectionList)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                    break
                case .failure(let error):
                    failure(error,response.response?.statusCode,getErrorMessage(response: response))
                }
            }
        }
    }
}
