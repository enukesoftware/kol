//
//  DataManager.swift
//  KOL
//
//  Created by Rupak Biswas on 20/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class DataManager: NSObject {

    static let shared = DataManager()
    var cartCount = 0
    var isFromPickup = false
    var orderConfirmed = false
    var loginDetails: LoginDetails?
    var fullToken: String {
        return "JWT " + (loginDetails?.access ?? "")
    }
    let currency = "SAR"
    let profileImages = ["profile1","profile2","profile3","profile5","profile6","profile7","profile8","profile9"]
    let names = ["Beatrice Meyer", "Nick Wade", "Doris Mendoza", "Jerry Baker", "Jerry Baker","Israr Frost","Oran Bowman","Christina Finley","John Cornish","Aoife Cochran","Jimi Edmonds","Olly Carpenter","Tania Peck","Tomos Bloom","Ashlea Webster"]
    let addOns = ["Beef Pepperoni","Chicken Sausage","Mushroom","Bacon","Avocado","Cheese","Double-Meat & Cheese"]
    var currentTableId = -1
    var fcmDetails: FCMDetails?
    var haveToShowNotificationDetails: PushNotificationDetails?
    var runningOrderDetails: OrderDetails?
    var pendingRating = false
    var savedCardDetails = SavedCardDetails()
    var shouldUpdatePaymentSelection = false
}
