//
//  AuthManager.swift
//  KOL
//
//  Created by Rupak on 17/08/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class AuthManager {
    
    class func saveLoginDetails() {
        if let loginDetails = DataManager.shared.loginDetails {
            UserDefaults.standard.setValue(loginDetails.toJSON(), forKey: "loginDetails")
        }
    }
    
    class func getLastSavedLoginDetails() -> LoginDetails? {
        if let loginDic = UserDefaults.standard.value(forKey: "loginDetails") as? [String:Any],let loginDetails = Mapper<LoginDetails>().map(JSON: loginDic) {
            DataManager.shared.loginDetails = loginDetails
            return loginDetails
        }
        return nil
    }
    
    class func logout() {
        DataManager.shared.loginDetails = nil
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "loginDetails")
        defaults.synchronize()
    }
    
    class func saveCardDetails() {
        UserDefaults.standard.setValue(DataManager.shared.savedCardDetails.toJSON(), forKey: "savedCardDetails")
    }
    
    class func getLastSavedCardDetails() -> SavedCardDetails? {
        if let cardDic = UserDefaults.standard.value(forKey: "savedCardDetails") as? [String:Any],let savedCardDetails = Mapper<SavedCardDetails>().map(JSON: cardDic) {
            DataManager.shared.savedCardDetails = savedCardDetails
            return savedCardDetails
        }
        return nil
    }
}
