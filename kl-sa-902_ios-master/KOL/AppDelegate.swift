//
//  AppDelegate.swift
//  KOL
//
//  Created by Rupak Biswas on 2/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import Firebase
import UserNotifications
import ObjectMapper
import SwiftyJSON
import FirebaseMessaging

#if DEBUG
    let DEBUG: Int = 1
#else
    let DEBUG: Int = 0
#endif

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        let currentLanguage: Languages = UserDefaults.standard.value(forKey: "currentLanguage") as? String == "ar" ? .ar : .en
        LanguageManger.shared.currentLanguage = currentLanguage
        UIView.appearance().semanticContentAttribute = currentLanguage == .ar ? .forceRightToLeft : .forceLeftToRight
        GMSServices.provideAPIKey("AIzaSyAKBFc4cGe5-UPAMziz5Woi2u-bxG1XvdE")
        instantiateAppropriateViewController()
        
        //Notification setup
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        if let notificationDic = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String:Any], let notificationDetails = Mapper<PushNotificationDetails>().map(JSON: notificationDic) {
            DataManager.shared.haveToShowNotificationDetails = notificationDetails
        }
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

//MARK: Push notification related
extension AppDelegate: MessagingDelegate {
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if let refreshedToken = Messaging.messaging().fcmToken {
            print("\nInstanceID token: \(refreshedToken)\n")
        }
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if application.applicationState == .inactive || application.applicationState == .background
        {
            print("inactive")
        }else if application.applicationState == .active {
            print("active")
        }
//        DataManager.manager.haveNewNotification = true
//        NotificationCenter.default.post(name: Notification.Name("NewNotification"), object: nil)
        print(JSON(userInfo))
        if let dataDic = userInfo as? [String:Any], let notificationDetails = Mapper<PushNotificationDetails>().map(JSON: dataDic) {
            DataManager.shared.haveToShowNotificationDetails = notificationDetails
            if DataManager.shared.loginDetails?.user?.first?.id != nil {
                TaskManager.openTappedNotificationScreen()
            }
//            if notificationDetails.notificationAction == "NEW_ITEM" {
//                DataManager.shared.otherNotificationDetails = notificationDetails
//                if DataManager.shared.loginDetails?.user?.first?.id != nil {
//                    TaskManager.openTappedNotificationScreen()
//                }
//            } else {
//                DataManager.shared.haveToShowNotificationDetails = notificationDetails
//                if DataManager.shared.loginDetails?.user?.first?.id != nil {
//                    TaskManager.openTappedNotificationScreen()
//                }
//            }
        }
    }
}

//MARK: Custom methods
extension AppDelegate {
    
    func instantiateAppropriateViewController() {
        if AuthManager.getLastSavedLoginDetails() != nil {
            if let navController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? UINavigationController {
                if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController {
                    navController.pushViewController(controller, animated: false)
                    window?.rootViewController = navController
                }
            }
        } else {
            if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() {
                window?.rootViewController = controller
            }
        }
    }
}
