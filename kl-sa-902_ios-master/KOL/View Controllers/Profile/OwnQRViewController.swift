//
//  OwnQRViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class OwnQRViewController: UIViewController {

    @IBOutlet weak var qrImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateWithUser()
    }
}

//MARK: Other methods
extension OwnQRViewController {
    
    func updateWithUser() {
        qrImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        if let urlString = DataManager.shared.loginDetails?.customer?.qrCode, let url = URL(string: urlString) {
            qrImageView.sd_setImage(with: url, placeholderImage: nil, options: .highPriority, completed: nil)
        }
    }
}
