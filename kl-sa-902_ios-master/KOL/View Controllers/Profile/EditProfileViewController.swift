//
//  EditProfileViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class EditProfileViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var profileImageFile: FileDetails? {
        didSet {
            profileImageView.image = profileImageFile?.image
            profileImageView.makeCircular = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateWithUser()
    }
}

//MARK: Action methods
extension EditProfileViewController {
    
    @IBAction func clickedOnAddPhoto(_ sender: Any) {
        TaskManager.shared.pickImage(source: sender as? UIView) { (selectedImage) in
            if let image = selectedImage {
                image.resizeImage(targetSize: CGSize.init(width: 500.0, height: 500.0*(image.size.width/image.size.height))) { (resizedImage) in
                    
                    let fileData = resizedImage.jpegData(compressionQuality: 0.75)
                    var fileDetails = FileDetails(fileName: "profile_image.jpg", mimeType: "image/jpeg", fileData: fileData)
                    fileDetails.image =  resizedImage
                    self.profileImageFile = fileDetails
                }
            }
        }
    }
    
    @IBAction func clickedOnSave(_ sender: Any) {
        let nameString = self.nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.nameTextField.text = nameString
        self.view.endEditing(true)
        if checkValidation() {
            updateUser { (success) in
                if success {
                    self.navigationController?.popViewController(animated: true)
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Your profile updated successfully.".localized)
                }
            }
        }
    }
}

//MARK: Request methods
extension EditProfileViewController {
    
    func updateUser(_ completion: @escaping(_ success: Bool) -> Void) {
        var fileDic:[[String: FileDetails]] = []
        if let file = profileImageFile {
            fileDic.append(["user.profile_picture" : file])
        }
        
        AppUtility.showProgress(nil, title: "Updating...".localized)
        RequestManager.registerWith(modify: true, params: getParameters(), fileDic: fileDic, completion: { (loginDetails, userDetails) in
            AppUtility.hideProgress(nil)
            if let user = userDetails, user.id != nil {
                DataManager.shared.loginDetails?.user = [user]
                AuthManager.saveLoginDetails()
            }
            completion(userDetails != nil)
        }) { (error, message) in
            AppUtility.hideProgress(nil)
            if let msg = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: msg)
            }
            completion(false)
        }
    }
    
    func verifyAccount(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.verifyAccount(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension EditProfileViewController {
    
    func updateWithUser() {
        if let user = DataManager.shared.loginDetails?.user?.first {
            profileImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            if let urlString = user.profilePicture, let url = URL(string: urlString) {
                profileImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
                profileImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority) { (image, error, cache, url) in
                    if let image = image, let data = image.jpegData(compressionQuality: 1.0) {
                        var fileDetails = FileDetails(fileName: "profile_image.jpg", mimeType: "image/jpeg", fileData: data)
                        fileDetails.image = image
                        self.profileImageFile = fileDetails
                    }
                }
            } else {
                profileImageView.image = UIImage.defaultProfile()
            }
            nameTextField.text = user.name
            emailTextField.text = user.email
        }
    }
    
    func checkValidation() -> Bool {
        
        if nameTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your name.".localized)
            return false
        } else if emailTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your email address.".localized)
            return false
        } else if let email = emailTextField.text, !AppUtility.isValidEmail(email: email) {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter a valid email address.".localized)
            return false
        } else if let name = nameTextField.text, !AppUtility.hasNoSpecialCharacter(text: name) {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Special characters not allowed".localized)
            return false
        }
        
        return true
    }
    
    func getParameters() -> [String: Any] {
        let params: [String: Any] = ["user.name" : nameTextField.text ?? "",
                                     "user.email" : emailTextField.text ?? ""]
        
        return params
    }
}
