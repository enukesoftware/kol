//
//  NearbyShopsViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 13/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class NearbyShopsViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var shopListView: UIView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var shopListTableView: UITableView!
    @IBOutlet weak var searchListView: UIView!
    
    @IBOutlet weak var shopListViewHeightConstraint: NSLayoutConstraint!
    
    var listViewOpen = false {
        didSet {
            if listViewOpen {
                let topHeight = searchView.frame.origin.y + 52
                shopListViewHeightConstraint.constant = self.view.frame.height-(UIApplication.shared.statusBarFrame.height+topHeight)
            } else {
                shopListViewHeightConstraint.constant = self.view.frame.height/3
            }
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation? {
        didSet{
            if let lat = currentLocation?.coordinate.latitude, let long = currentLocation?.coordinate.longitude {
                mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14)
            }
        }
    }
    private var infoWindow = MapMarkerWindow()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
    var restaurantListDetails: RestaurantListDetails? {
        didSet {
            if let restaurants = restaurantListDetails?.restaurants {
                restaurantList.append(contentsOf: restaurants)
            }
        }
    }
    var restaurantList = [RestaurantDetails]() {
        didSet {
            shopListTableView.reloadData()
            loadMarkers()
        }
    }
    var searchedRestaurantListDetails: RestaurantListDetails? {
        didSet {
            searchedRestaurantList = searchedRestaurantListDetails?.restaurants ?? []
        }
    }
    var searchedRestaurantList = [RestaurantDetails]() {
        didSet {
            searchTableView.reloadData()
        }
    }
    var isLoadingData = false
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listViewOpen = false
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        shopListView.addGestureRecognizer(gesture)
        gesture.delegate = self
        
        shopListTableView.estimatedRowHeight = 115
        shopListTableView.rowHeight = UITableView.automaticDimension
        
        self.locationManager.delegate = self
        
        self.locationManager.startUpdatingLocation()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        currentLocation = CLLocation.init(latitude: 23.777044, longitude: 90.420701)
        
        self.infoWindow = loadMarkerNiB()
        loadMarkers()
//        loadDummyData()
        
        TaskManager.shared.requestForAccessingLocation { (currentLocation) in
            if let lat = currentLocation?.coordinate.latitude, let long = currentLocation?.coordinate.longitude {
                self.getRestaurantListByLocation(lat: lat, long: long) { (success) in}
            } else {
                self.getRestaurantListByLocation(lat: 23.777044, long: 90.420701) { (success) in}
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if LanguageManger.shared.currentLanguage == .en {
            searchTextField.textAlignment = .left
        } else {
            searchTextField.textAlignment = .right
        }
    }
}

//MARK: Action methods
extension NearbyShopsViewController {
    
    @IBAction func clickedOnClear(_ sender: UIButton) {
        searchTextField.text = ""
        searchTextField.resignFirstResponder()
        searchListView.isHidden = true
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension NearbyShopsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchTableView {
            return searchedRestaurantList.count
        }
        return restaurantList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == searchTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell") as? SearchTableViewCell else {
                return UITableViewCell.init()
            }
            
            cell.selectionStyle = .none
            cell.nameLabel?.text = searchedRestaurantList[indexPath.row].user?.localizedName()
            
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShopListTableViewCell") as? ShopListTableViewCell else {
            return UITableViewCell.init()
        }
        
        cell.selectionStyle = .none
        cell.updateWith(object: restaurantList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let controller = Storyboard.controllerWith(name: "ShopDetailsViewController") as? ShopDetailsViewController {
            if tableView == searchTableView {
                controller.restaurantDetails = searchedRestaurantList[indexPath.row]
            } else {
                controller.restaurantDetails = restaurantList[indexPath.row]
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == searchTableView {
            return 48
        }
        return UITableView.automaticDimension
    }
}

//MARK: UITextFieldDelegate methods
extension NearbyShopsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchButton.isUserInteractionEnabled = ((textField.text?.count ?? 0) > 0)
        if let text = textField.text, text.count > 0  {
            getRestaurantListByKeyword(keyword: text) { (success) in}
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let textFieldString = textField.text, let swtRange = Range(range, in: textFieldString) {
            let fullString = textFieldString.replacingCharacters(in: swtRange, with: string)
            
            searchButton.setImage(UIImage(named: fullString.count > 0 ? "clear" : "search"), for: .normal)
            searchButton.isUserInteractionEnabled = (fullString.count > 0)
            searchListView.isHidden = !(fullString.count > 0)
        }
        
        if let text = textField.text,
           let textRange = Range(range, in: text) {
           let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
           getRestaurantListByKeyword(keyword: updatedText) { (success) in}
        }
        
        return true
    }
}

//MARK: Location Manager delegates related
extension NearbyShopsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        self.mapView?.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
    }
}

//MARK: GMSMapViewDelegate and map related methods
extension NearbyShopsViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        locationMarker = marker
        infoWindow.removeFromSuperview()
        infoWindow = loadMarkerNiB(index: marker.iconView?.tag ?? 0)
        guard let location = locationMarker?.position else {
            print("locationMarker is nil")
            return false
        }
        infoWindow.delegate = self
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - 90
        
        self.view.insertSubview(infoWindow, at: 1)
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 90
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
    }
    
    func loadMarkers() {
        for (index, restaurant) in restaurantList.enumerated() {
            if let lat = restaurant.lat, let long = restaurant.lng {
                let marker = GMSMarker()
                let markerImage = UIImage.init(named: "marker")
                let markerView = UIImageView(image: markerImage)
                marker.iconView = markerView
                marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                marker.map = self.mapView
                marker.iconView?.tag = index
            }
            
//            firstMarker = marker
        }
    }
    
    func loadMarkerNiB(index: Int = 0) -> MapMarkerWindow {
        let infoWindow = MapMarkerWindow.instanceFromNib() as! MapMarkerWindow
        if restaurantList.count > index {
            infoWindow.nameLabel.text = restaurantList[index].user?.localizedName() ?? "N/A"
            infoWindow.addressLabel.text = restaurantList[index].fullAddress ?? "N/A"
            infoWindow.restaurentTypeLabel.text = restaurantList[index].restaurantType?.finalName() ?? "N/A"
        }
        return infoWindow
    }
}

//MARK: MapMarkerWindowDelegate methods
extension NearbyShopsViewController: MapMarkerWindowDelegate {
    
    func clickedInside(with view: MapMarkerWindow, data: NSDictionary?) {
        if let controller = Storyboard.controllerWith(name: "ShopDetailsViewController") as? ShopDetailsViewController {
            if view.tag-1 < restaurantList.count {
                controller.restaurantDetails = restaurantList[view.tag-1]
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

//MARK: UIGestureRecognizerDelegate methods
extension NearbyShopsViewController: UIGestureRecognizerDelegate {
    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizer.State.ended || gestureRecognizer.state == UIGestureRecognizer.State.changed {
            let translation = gestureRecognizer.translation(in: self.view)
//            let velocity = gestureRecognizer.velocity(in: self.view)
            
            if shopListViewHeightConstraint.constant-translation.y > self.view.frame.height/3 {
                shopListViewHeightConstraint.constant = shopListViewHeightConstraint.constant-translation.y
            }
            if gestureRecognizer.state == .ended {
                listViewOpen = (shopListViewHeightConstraint.constant > self.view.center.y)
            } else {
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
        }
    }
}

//MARK: Request methods
extension NearbyShopsViewController {
    
    func getRestaurantListByLocation(isNew: Bool = false, lat: Double, long: Double, completion: @escaping(_ success: Bool) -> Void) {
        isLoadingData = true
        AppUtility.showProgress(self.view, title: nil)
        if isNew {
            restaurantList = []
        }
        var nextPageArguments = "?listing=True&limit=15&offset=\(restaurantList.count)"
        if let nextPage = restaurantListDetails?.next, nextPageArguments.contains(AppConstants.BaseURL.Current), !isNew {
            let argument = nextPage.replacingOccurrences(of: AppConstants.BaseURL.Current, with: "")
            if argument != "" {
                nextPageArguments = argument
            }
        }
        
        RequestManager.getRestaurantListByLocation(lat: lat, long: long, completion: { (object) in
            AppUtility.hideProgress(self.view)
            self.isLoadingData = false
            self.restaurantListDetails = object
            completion(self.restaurantListDetails?.restaurants != nil)
        }) { (error, code, message) in
            AppUtility.hideProgress(self.view)
            self.isLoadingData = false
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            print("Error occured")
            completion(false)
        }
    }
    
    func getRestaurantListByKeyword(keyword: String, completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(self.view, title: nil)
        RequestManager.getRestaurantListByKeyword(keyword: keyword, completion: { (object) in
            AppUtility.hideProgress(self.view)
            self.searchedRestaurantListDetails = object
            completion(self.restaurantListDetails?.restaurants != nil)
        }) { (error, code, message) in
            AppUtility.hideProgress(self.view)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            print("Error occured")
            completion(false)
        }
    }
}

extension NearbyShopsViewController {
    
//    func loadDummyData() {
//        shopList = []
//        shopList.append(["logo" : "first_crack",
//                         "name" : "First Crack",
//                         "category" : "Cafe",
//                         "address" : "Corniche Rd, Ash Shati, Beach Tower, Jeddah 23613, Saudi Arabia"])
//        shopList.append(["logo" : "firefly",
//                         "name" : "Firefly Burger",
//                         "category" : "Burger",
//                         "address" : "New Town Center، Jeddah 23432, Saudi Arabia"])
//        searchTableView.reloadData()
//        shopListTableView.reloadData()
//    }
}
