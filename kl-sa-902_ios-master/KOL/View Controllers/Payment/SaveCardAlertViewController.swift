//
//  SaveCardAlertViewController.swift
//  KOL
//
//  Created by Rupak on 26/03/2020.
//  Copyright © 2020 Rupak. All rights reserved.
//

import UIKit

protocol SaveCardAlertViewControllerDelegate {
    func clickedOnAction(isYes: Bool)
}

class SaveCardAlertViewController: UIViewController {

    @IBOutlet weak var checkImageView: UIImageView!
    
    var delegate: SaveCardAlertViewControllerDelegate?
    var askAgain = true {
        didSet {
            checkImageView.image = UIImage(named: askAgain ? "ask_uncheck" : "ask_check")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

//MARK: Action methods
extension SaveCardAlertViewController {
    
    @IBAction func clickedOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedOnYes(_ sender: Any) {
        self.dismiss(animated: true) {
            DataManager.shared.savedCardDetails.showAlert = self.askAgain
            AuthManager.saveCardDetails()
            self.delegate?.clickedOnAction(isYes: true)
        }
    }
    
    @IBAction func clickedOnNo(_ sender: Any) {
        self.dismiss(animated: true) {
            DataManager.shared.savedCardDetails.showAlert = self.askAgain
            AuthManager.saveCardDetails()
            self.delegate?.clickedOnAction(isYes: false)
        }
    }
    
    @IBAction func clickedOnAskAgain(_ sender: Any) {
        self.askAgain = !self.askAgain
    }
}
