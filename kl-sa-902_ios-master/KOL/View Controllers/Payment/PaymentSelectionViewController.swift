//
//  PaymentSelectionViewController.swift
//  KOL
//
//  Created by Rupak on 6/8/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

struct MemberDetails {
    var name: String?
    var amount: Double = 0.0
    var profileImage: String?
    
    init(name: String?, amount: Double, profileImage: String?) {
        self.name = name
        self.amount = amount
        self.profileImage = profileImage
    }
}

class SavedCardDetails: NSObject, Mappable {
    
    var showAlert: Bool = true
    var showPaymentAlert: Bool = true
    var savedToken: String = ""
    
    override init() {}
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        showAlert <- map["showAlert"]
        showPaymentAlert <- map["showPaymentAlert"]
        savedToken <- map["savedToken"]
    }
}

class PaymentSelectionViewController: UIViewController {

    @IBOutlet weak var paymentsTableView: UITableView!
    
    var selectedPersonIndexList = [Int]()
    var selectedPaymentTypeIndex = 0
    
    var paymentMethodList: [[String:Any]] = []
    var memberList = [MemberDetails]()
    var initialSetupViewController: PTFWInitialSetupViewController!
    var orderDetails: OrderDetails?
    var invoiceItemList = [InvoiceItemDetails]()
    var transactionDetails: TransactionResponseDetails?
    var shouldSaveToken = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateWithContents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if DataManager.shared.shouldUpdatePaymentSelection {
            DispatchQueue.main.async {
                self.updateAndRefreshPaymentList()
            }
            DataManager.shared.shouldUpdatePaymentSelection = false
        }
    }
}

//MARK: Action methods
extension PaymentSelectionViewController {
    
    @IBAction func clickedOnContactKOL(_ sender: Any) {
        guard let number = URL(string: "tel://" + "+966565562501") else { return }
        if UIApplication.shared.canOpenURL(number) {
            UIApplication.shared.openURL(number)
        } else {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Sorry, can't able to call. Please check your network status.".localized)
        }
    }
    
    @IBAction func clickedOnReportIssue(_ sender: Any) {
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReportOrderViewController") as? ReportOrderViewController {
            controller.orderDetails = orderDetails
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func clickedOnPaymentProcess(_ sender: Any) {
        if !checkValidation() {return}
        self.proceedPayment()
        /*if DataManager.shared.savedCardDetails.showAlert && DataManager.shared.savedCardDetails.savedToken == "" {
            if let controller = Storyboard.controllerWith(name: "SaveCardAlertViewController") as? SaveCardAlertViewController {
                controller.modalPresentationStyle = .overCurrentContext
                controller.modalTransitionStyle = .crossDissolve

                controller.delegate = self
                self.present(controller, animated: true, completion: nil)
            }
        } else if DataManager.shared.savedCardDetails.savedToken != "" && DataManager.shared.savedCardDetails.showPaymentAlert {
            if let controller = Storyboard.controllerWith(name: "ProcessPaymentAlertViewController") as? ProcessPaymentAlertViewController {
                controller.modalPresentationStyle = .overCurrentContext
                controller.modalTransitionStyle = .crossDissolve
                
                controller.delegate = self
                self.present(controller, animated: true, completion: nil)
            }
        } else {
            self.proceedPayment()
        }*/
        
//        if let controller = Storyboard.controllerWith(name: "PaymentSuccessViewController") as? PaymentSuccessViewController {
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
    }
}

//MARK: SaveCardAlertViewControllerDelegate methods
extension PaymentSelectionViewController: SaveCardAlertViewControllerDelegate {
    
    func clickedOnAction(isYes: Bool) {
        if isYes {
            self.shouldSaveToken = true
            self.proceedPayment()
        } else {
            self.shouldSaveToken = false
            self.proceedPayment()
        }
    }
}

//MARK: ProcessPaymentAlertViewControllerDelegate methods
extension PaymentSelectionViewController: ProcessPaymentAlertViewControllerDelegate {
    
    func clickedOnContinue(option: ProcessOption) {
        if option.type == .token {
            
        } else {
            self.proceedPayment()
        }
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate  methods
extension PaymentSelectionViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return memberList.count}
        return paymentMethodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell") as? PaymentTableViewCell else {
            return UITableViewCell.init()
        }
        cell.selectionStyle = .none
        cell.payForKolView.isHidden = !(indexPath.section == 0 && indexPath.row == 0)
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.contentsView.round(corners: [.topLeft, .topRight], radius: 10)
            } else if indexPath.row == (invoiceItemList[indexPath.section].foodItems?.count ?? 0) - 1 {
                cell.contentsView.round(corners: [.bottomLeft, .bottomRight], radius: 10)
            }
            cell.amountLabel.isHidden = false
            cell.updateWith(object: memberList[indexPath.row], index: indexPath.row)
            cell.isSelected = selectedPersonIndexList.contains(indexPath.row)
        } else {
            if indexPath.row == 0 {
                cell.contentsView.round(corners: [.topLeft, .topRight], radius: 10)
            } else if indexPath.row == paymentMethodList.count - 1 {
                cell.contentsView.round(corners: [.bottomLeft, .bottomRight], radius: 10)
            }
            cell.amountLabel.isHidden = true
            cell.titleLabel.text = paymentMethodList[indexPath.row]["title"] as? String
            cell.isSelected = (selectedPaymentTypeIndex == indexPath.row)
            if let icon = paymentMethodList[indexPath.row]["icon"] as? String {
                cell.paymentImageView.image = UIImage(named: icon)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if !selectedPersonIndexList.contains(indexPath.row) {
                    selectedPersonIndexList = []
                    for index in 1..<memberList.count {
                        selectedPersonIndexList.append(index)
                    }
                }
            } else {
                selectedPersonIndexList.remove(object: 0)
            }
            if selectedPersonIndexList.contains(indexPath.row) {
                selectedPersonIndexList.remove(object: indexPath.row)
            } else {
                selectedPersonIndexList.append(indexPath.row)
            }
        } else {
            selectedPaymentTypeIndex = indexPath.row
        }
        paymentsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Choose Person".localized
        }
        return "Payment Type".localized
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.backgroundColor = .clear
            headerView.tintColor = .clear
            headerView.textLabel?.textColor = UIColor.hexStringToUIColor("78849E")
            headerView.textLabel?.font = UIFont.init(name: "Poppins-Medium", size: 13)
        }
    }
}

//MARK: Paytabs methods
extension PaymentSelectionViewController {
    
    private func initiateSDK() {
        let bundle = Bundle(url: Bundle.main.url(forResource: AppConstants.ApplicationResources.kFrameworkResourcesBundle, withExtension: "bundle")!)
        
        guard let userDetails = DataManager.shared.loginDetails?.user?.first else {return}
        
        self.initialSetupViewController = PTFWInitialSetupViewController.init(
            bundle: bundle,
            andWithViewFrame: self.view.frame,
            andWithAmount: Float(Double(self.transactionDetails?.amount ?? "0") ?? 0.0),
            andWithCustomerTitle: "KOL Payment",
            andWithCurrencyCode: "SAR",
            andWithTaxAmount: 0.0,
            andWithSDKLanguage: (LanguageManger.shared.currentLanguage == .ar) ? "ar" : "en",
            andWithShippingAddress: "address",
            andWithShippingCity: "city_name",
            andWithShippingCountry: "BHR",
            andWithShippingState: "city_name",
            andWithShippingZIPCode: "1212",
            andWithBillingAddress: "address",
            andWithBillingCity: "city_name",
            andWithBillingCountry: "SAU",
            andWithBillingState: "city_name",
            andWithBillingZIPCode: "1212",
            andWithOrderID: self.transactionDetails?.ptOrderId ?? "",
            andWithPhoneNumber: "+966"+AppUtility.getPhoneWithCountryCode(value: userDetails.phoneNumber ?? "01913243746"),
            andWithCustomerEmail: userDetails.email ?? "abc@accept.com",
            andIsTokenization: false,
            andIsPreAuth: false,
            andWithMerchantEmail: "bahaajamal@hotmail.com",
            andWithMerchantSecretKey: "UJmcIv85wlWT3fsc3A9rTIiJC5FF8En3oJf7fndLJ9FMp7ZsH9TBRclui0aqFxt3hJbw6oiPyXCX4CqMLjoDwJ7OKhfpOJt6sECz",
            andWithAssigneeCode: "SDK",
            andWithThemeColor: UIColor(red:  CGFloat(43.0/255), green: CGFloat(55.0/255), blue: CGFloat(78.0/255), alpha: 1.0),
            andIsThemeColorLight: false)
        
        self.initialSetupViewController.didReceiveBackButtonCallback = {
            self.initialSetupViewController.dismiss(animated: true, completion: nil)
        }
        
        self.initialSetupViewController.didStartPreparePaymentPage = {
            AppUtility.showProgress(title: nil)
        }
        self.initialSetupViewController.didFinishPreparePaymentPage = {
            AppUtility.hideProgress()
        }
        
        self.initialSetupViewController.didReceiveFinishTransactionCallback = {(responseCode, result, transactionID, tokenizedCustomerEmail, tokenizedCustomerPassword, token, transactionState) in
            
            self.initialSetupViewController.dismiss(animated: true, completion: {
                self.verifyTransaction(id: "\(transactionID)", { (status) in
                    if let transactionStatus = status {
//                        DataManager.manager.shouldUpdateParentData = true
                        if ((!DataManager.shared.isFromPickup && transactionStatus == 1) || (DataManager.shared.isFromPickup && transactionStatus == 5)), let controller = Storyboard.controllerWith(name: "PaymentSuccessViewController") as? PaymentSuccessViewController {
                            if token != "" {
                                DataManager.shared.savedCardDetails.savedToken = token
                                AuthManager.saveCardDetails()
                            }
                            controller.orderDetails = self.orderDetails
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                })
            })
        }
    }
    
    func proceedPayment() {
        if let orderId = orderDetails?.id {
            let params: [String:Any] = ["order" : orderId,
                                        "invoice_items" : selectedInvoiceItems()]
            getTransactionDetails(params: params) { (success) in
                if success {
                    self.initiateSDK()
                    let bundle = Bundle(url: Bundle.main.url(forResource: AppConstants.ApplicationResources.kFrameworkResourcesBundle, withExtension: "bundle")!)
                    
                    if bundle?.path(forResource: AppConstants.ApplicationXIBs.kPTFWInitialSetupView, ofType: "nib") != nil {
                        print("exists")
                    } else {
                        print("not exist")
                    }
                    DispatchQueue.main.async {
                        UIApplication.topViewController()?.present(self.initialSetupViewController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}

//MARK: Request methods
extension PaymentSelectionViewController {
    
    func getTransactionDetails(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getTransactionDetails(params: params, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            self.transactionDetails = object
            completion(object?.ptOrderId != nil)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func verifyTransaction(_ showLoading: Bool = true, id: String, _ completion: @escaping(_ status: Int?) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.verifyTransaction(id: id, completion: { (status) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(status)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
    
    func getInvoiceDetails(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ object: InvoiceDetails?) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getInvoiceDetails(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(nil)
        }
    }
}

//MARK: Custom methods
extension PaymentSelectionViewController {
    
    func getPriceWith(items: [OrderItemDetails]) -> Double {
        var amount = 0.0
        for item in items {
            amount += item.sharedPriceWithTax ?? 0
        }
        return amount
    }
    
    func updateWithContents() {
        for (index, item) in invoiceItemList.enumerated() {
            if item.user?.id == DataManager.shared.loginDetails?.user?.first?.id {
                selectedPersonIndexList = [index + 1]
                break
            }
        }
        memberList = []
        var amount = 0.0
        for item in invoiceItemList {
            if item.paid == false {
                amount += getPriceWith(items: item.foodItems ?? [])
            }
        }
        memberList.append(MemberDetails(name: "Pay for".localized, amount: amount, profileImage: nil))
        for item in invoiceItemList {
            if item.paid == false {
                memberList.append(MemberDetails(name: item.user?.localizedName(), amount: getPriceWith(items: item.foodItems ?? []), profileImage: item.user?.profilePicture))
            }
        }
        
        paymentMethodList = []
        paymentMethodList.append(["title" : "Card".localized,
                                  "icon" : "debit_card"])
        paymentMethodList.append(["title" : "Mada".localized,
                                  "icon" : "mada"])
        paymentsTableView.reloadData()
    }
    
    func updateAndRefreshPaymentList() {
        if let id = orderDetails?.id {
            getInvoiceDetails(true, id: id) { (object) in
                self.invoiceItemList = object?.invoiceItems ?? []
                self.updateWithContents()
            }
        }
    }
    
    func getAmountOfSelectedMembers() -> Double {
        var amount = 0.0
        for index in selectedPersonIndexList {
            amount += memberList[index].amount
        }
        return amount
    }
    
    func selectedInvoiceItems() -> [Int] {
        var items = [Int]()
        for index in selectedPersonIndexList {
            if index == 0 {
                items = []
                for item in invoiceItemList {
                    if let id = item.id {
                        items.append(id)
                    }
                }
            } else {
                if let id = invoiceItemList[index-1].id {
                    items.append(id)
                }
            }
        }
        return items
    }
    
    func checkValidation() -> Bool {
        if selectedPersonIndexList.count == 0 {
            AlertManager.showAlertWith(title: "Alert".localized, message: "Please select person first.".localized, nil)
            return false
        }
        return true
    }
}
