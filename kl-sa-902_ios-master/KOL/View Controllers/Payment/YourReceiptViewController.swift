//
//  YourReceiptViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 11/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class YourReceiptViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ordersTableView: UITableView!
    
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLeftConstraint: NSLayoutConstraint!
    
    let headerViewMaxHeight: CGFloat = 108
    let headerViewMinHeight: CGFloat = 56
    var maxLeftPadding: CGFloat = 1.0
    var invoice: InvoiceDetails?
    var invoiceDetails: InvoiceDetails? {
        didSet {
            if let items = invoiceDetails?.invoiceItems {
                invoiceItemList = items
                ordersTableView.reloadData()
            }
        }
    }
    var invoiceItemList = [InvoiceItemDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let font = UIFont.init(name: "Poppins-SemiBold", size: 24), let titleLabelWidth = titleLabel.text?.widthOfString(usingFont: font) {
            maxLeftPadding = (self.view.frame.width/2.0)-(titleLabelWidth/2.0)
        }
        self.invoiceDetails = invoice
        ordersTableView.estimatedRowHeight = 135
        ordersTableView.rowHeight = UITableView.automaticDimension
    }
}

//MARK: Action methods
extension YourReceiptViewController {
    
    @IBAction func clickedOnReport(_ sender: Any) {
        self.performSegue(withIdentifier: "ReportOrderViewControllerSegue", sender: nil)
    }
}

//MARK: Segue methods
extension YourReceiptViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ReportOrderViewControllerSegue", let controller = segue.destination as? ReportOrderViewController {
            controller.invoiceDetails = invoiceDetails
        }
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate  methods
extension YourReceiptViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return invoiceItemList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoiceItemList[section].foodItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptTableViewCell") as? ReceiptTableViewCell else {
            return UITableViewCell.init()
        }
        cell.selectionStyle = .none
        cell.contentsView.cornerRadius = 0
//        if indexPath.row == 0 {
//            cell.contentsView.round(corners: [.bottomLeft, .bottomRight], radius: 0)
//            cell.contentsView.round(corners: [.topLeft, .topRight], radius: 10)
//        }
        if let item = invoiceItemList[indexPath.section].foodItems?[indexPath.row] {
            cell.updateWith(item: item, participants: [], userList: invoiceDetails?.getUserList() ?? [])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return invoiceItemList[section].user?.localizedName() ?? "N/A"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let footerView = tableView.dequeueReusableCell(withIdentifier: "CheckoutGroupFooterTableViewCell") as? CheckoutGroupFooterTableViewCell {
            footerView.subtotalLabel.text = String(format: "%.2f \(DataManager.shared.currency.localized)", getPriceWith(items: invoiceItemList[section].foodItems ?? []))
            footerView.subtotalCalorieLabel.text = "\(getCalorieWith(items: invoiceItemList[section].foodItems ?? []))"
            
//            footerView.contentsView.round(corners: [.bottomLeft, .bottomRight], radius: 10)
            
            return footerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.backgroundColor = .clear
            headerView.tintColor = .clear
            headerView.textLabel?.textColor = UIColor.hexStringToUIColor("78849E")
            headerView.textLabel?.font = UIFont.init(name: "Poppins-Medium", size: 13)
        }
    }
}

// MARK: - UIScrollViewDelegate
extension YourReceiptViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
        
        if newHeaderViewHeight > headerViewMaxHeight {
            headerViewHeightConstraint.constant = headerViewMaxHeight
            adjustTitleLabel(newHeight: headerViewMaxHeight)
        } else if newHeaderViewHeight < headerViewMinHeight {
            headerViewHeightConstraint.constant = headerViewMinHeight
            adjustTitleLabel(newHeight: headerViewMinHeight)
        } else {
            headerViewHeightConstraint.constant = newHeaderViewHeight
            adjustTitleLabel(newHeight: newHeaderViewHeight)
            scrollView.contentOffset.y = 0
        }
    }
}


//MARK: Request methods
extension YourReceiptViewController {
    
    func getInvoiceDetails(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getInvoiceDetails(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            self.invoiceDetails = object
            completion(object != nil)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

// MARK: - Custom
extension YourReceiptViewController {
    
    func adjustTitleLabel(newHeight: CGFloat) {
        let fontSize = 40-((headerViewMaxHeight-newHeight)*0.25)
        titleLabel.font = UIFont.init(name: "Poppins-SemiBold", size: fontSize)
        let paddingUnit = (maxLeftPadding-8)/64.0
        let leftPadding = 24+((headerViewMaxHeight-newHeight)*paddingUnit)
        titleLeftConstraint.constant = leftPadding
    }
    
    func getPriceWith(items: [OrderItemDetails]) -> Double {
        var amount = 0.0
        for item in items {
            amount += item.sharedPriceWithTax ?? 0
        }
        return amount
    }
    
    func getCalorieWith(items: [OrderItemDetails]) -> Int {
        var amount = 0
        for item in items {
            amount += item.foodItemCalorie ?? 0
        }
        return amount
    }
}
