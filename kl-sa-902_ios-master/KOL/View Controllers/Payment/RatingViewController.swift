//
//  RatingViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 9/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController {

    @IBOutlet weak var foodRatingView: FloatRatingView!
    @IBOutlet weak var customerServiceRatingView: FloatRatingView!
    @IBOutlet weak var restaurantRatingView: FloatRatingView!
    @IBOutlet weak var applicationRatingView: FloatRatingView!
    @IBOutlet weak var shareButton: UIButton!
    
    var orderDetails: OrderDetails?
    var orderId: String?
    var ratingSubmitted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if orderDetails == nil {
            if let idString = orderId, let id = Int(idString) {
                TaskManager.getOrderDetails(id: id) { (order) in
                    self.orderDetails = order
                }
            }
        }
        UserDefaults.standard.setValue(true, forKey: "pendingRating")
        
        shareButton.imageEdgeInsets = LanguageManger.shared.currentLanguage == .en ? UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8) : UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
    }
}

//MARK: Action methods
extension RatingViewController {
    
    @IBAction func clickedOnHome(_ sender: Any) {
        if !checkValidation() {return}
        if let id = orderDetails?.id, let restaurant = orderDetails?.restaurant, let userId = DataManager.shared.loginDetails?.user?.first?.id {
            let params: [String:Any] = ["order" : id,
                                        "restaurant" : restaurant,
                                        "user" : userId,
                                        "food_item_rating" : Int(foodRatingView.rating),
                                        "restaurant_rating" : Int(restaurantRatingView.rating),
                                        "customer_service_rating" : Int(customerServiceRatingView.rating),
                                        "application_rating" : Int(applicationRatingView.rating)]
            orderRating(params: params) { (success) in
                if success {
                    self.ratingSubmitted = true
                    UserDefaults.standard.setValue(false, forKey: "pendingRating")
                    AlertManager.showAlertWith(title: "Success".localized, message: "Rating submitted successfully.".localized, actions: [CustomAction.init(index: 0, title: "Ok".localized.localized, style: .default)]) { (action) in
                        TaskManager.moveToViewController(with: DashboardViewController.classForCoder())
                    }
                }
            }
        }
    }
    
    @IBAction func clickedOnShare(_ sender: Any) {
        if !checkValidation() {return}
        if ratingSubmitted {
            let shareItems = ["Enhance your Digital Ordering by KOL App, Download & Check it out Yourself".localized, "\nhttps://apps.apple.com/sa/app/kol-app/id1495841798 \nhttps://play.google.com/store/apps/details?id=com.dtmweb.kol"]
            let activityViewController = UIActivityViewController(activityItems: shareItems , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        } else if let id = orderDetails?.id, let restaurant = orderDetails?.restaurant, let userId = DataManager.shared.loginDetails?.user?.first?.id {
            let params: [String:Any] = ["order" : id,
                                        "restaurant" : restaurant,
                                        "user" : userId,
                                        "food_item_rating" : Int(foodRatingView.rating),
                                        "restaurant_rating" : Int(restaurantRatingView.rating),
                                        "customer_service_rating" : Int(customerServiceRatingView.rating),
                                        "application_rating" : Int(applicationRatingView.rating)]
            orderRating(params: params) { (success) in
                if success {
                    self.ratingSubmitted = true
                    let shareItems = ["Enhance your Digital Ordering by KOL App, Download & Check it out Yourself".localized, "\nhttps://apps.apple.com/sa/app/kol-app/id1495841798 \nhttps://play.google.com/store/apps/details?id=com.dtmweb.kol"]
                    let activityViewController = UIActivityViewController(activityItems: shareItems , applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
//                    AlertManager.showAlertWith(title: "Success", message: "Rating submitted successfully.") { (action) in
//                        TaskManager.moveToViewController(with: DashboardViewController.classForCoder())
//                    }
                }
            }
        }
    }
}

//MARK: Request methods
extension RatingViewController {
    
    func orderRating(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.orderRating(params: params, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage.localized)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension RatingViewController {
    
    func checkValidation() -> Bool {
        if foodRatingView.rating == 0 {
            AlertManager.showAlertWith(title: "Alert".localized, message: "Please update rating for food.".localized, nil)
            return false
        } else if customerServiceRatingView.rating == 0 {
            AlertManager.showAlertWith(title: "Alert".localized, message: "Please update rating for customer service.".localized, nil)
            return false
        } else if restaurantRatingView.rating == 0 {
            AlertManager.showAlertWith(title: "Alert".localized, message: "Please update rating for restaurant.".localized, nil)
            return false
        } else if applicationRatingView.rating == 0 {
            AlertManager.showAlertWith(title: "Alert".localized, message: "Please update rating for application.".localized, nil)
            return false
        }
        return true
    }
}
