//
//  ProcessPaymentAlertViewController.swift
//  KOL
//
//  Created by Rupak on 26/03/2020.
//  Copyright © 2020 Rupak. All rights reserved.
//

import UIKit

enum ProcessType {
    case token, saveAndTry, notSaveAndTry
}

struct ProcessOption {
    var title = ""
    var token = ""
    var type: ProcessType = .notSaveAndTry
}

protocol ProcessPaymentAlertViewControllerDelegate {
    func clickedOnContinue(option: ProcessOption)
}

class ProcessPaymentAlertViewController: UIViewController {

    @IBOutlet weak var processOptionTableView: UITableView!
    @IBOutlet weak var checkImageView: UIImageView!
    
    @IBOutlet weak var processOptionTableViewHeightConstraint: NSLayoutConstraint!
    
    var optionList: [ProcessOption] = []
    var selectedIndex = 0
    var askAgain = true {
        didSet {
            checkImageView.image = UIImage(named: askAgain ? "ask_uncheck" : "ask_check")
        }
    }
    var delegate: ProcessPaymentAlertViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        processOptionTableView.backgroundColor = .clear
        loadOptions()
        processOptionTableViewHeightConstraint.constant = CGFloat(optionList.count * 40 + 10)
    }
}

//MARK: Action methods
extension ProcessPaymentAlertViewController {
    
    @IBAction func clickedOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedOnContinue(_ sender: Any) {
        let selectedOption = self.optionList[self.selectedIndex]
        self.dismiss(animated: true) {
            DataManager.shared.savedCardDetails.showPaymentAlert = self.askAgain
            AuthManager.saveCardDetails()
            self.delegate?.clickedOnContinue(option: selectedOption)
        }
    }
    
    @IBAction func clickedOnAskAgain(_ sender: Any) {
        self.askAgain = !self.askAgain
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate  methods
extension ProcessPaymentAlertViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProcessOptionTableViewCell") as? ProcessOptionTableViewCell else {
            return UITableViewCell.init()
        }
        cell.selectionStyle = .none
        cell.titleLabel.text = optionList[indexPath.row].title
        cell.makeSelected = indexPath.row == selectedIndex
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}

extension ProcessPaymentAlertViewController {
    
    func loadOptions() {
        optionList = []
        optionList.append(ProcessOption(title: "Pay with saved card", token: "", type: .token))
        optionList.append(ProcessOption(title: "Save & try with new card", token: "", type: .saveAndTry))
        optionList.append(ProcessOption(title: "Don't save & try with new card".localized, token: "", type: .notSaveAndTry))
        
        self.processOptionTableView.reloadData()
    }
}
