//
//  PaymentSuccessViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 9/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class PaymentSuccessViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var rateButton: UIButton!
    
    var orderDetails: OrderDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusLabel.text = statusLabel.text?.replacingOccurrences(of: "\\n", with: "\n").localized
        rateButton.isEnabled = false
        //enable or disable rate button by paid status
        if let id = orderDetails?.id {
            TaskManager.getInvoiceDetails(id: id) { (object) in
                if let invoice = object, invoice.id != nil {
                    var isPaid = true
                    for item in invoice.invoiceItems ?? [] {
                        if item.paid == false {
                            isPaid = false
                        }
                    }
                    DispatchQueue.main.async {
                        self.rateButton.isEnabled = isPaid
                    }
                }
            }
        }
    }
}

//MARK: Action methods
extension PaymentSuccessViewController {
    
    @IBAction func clickedOnRate(_ sender: Any) {
        if let controller = Storyboard.controllerWith(name: "RatingViewController") as? RatingViewController {
            controller.orderDetails = orderDetails
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
