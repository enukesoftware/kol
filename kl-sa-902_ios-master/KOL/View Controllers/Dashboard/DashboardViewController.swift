//
//  DashboardViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import AVFoundation
import SDWebImage
import ObjectMapper

class DashboardViewController: ScannerViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var scanQRLabel: UILabel!
    
    var previousCode = ""
    var activeOrderDetails: OrderDetails?
    var isLoadingData = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scannerDelegate = self
        loadQRScannerView()
        updateWithUser()
        loadDataContents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        TaskManager.checkCameraPermission()
        updateWithUser()
        DataManager.shared.cartCount = 0
        if AuthManager.getLastSavedCardDetails() == nil {
            DataManager.shared.savedCardDetails = SavedCardDetails()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        previousCode = ""
    }
}

//MARK: Action methods
extension DashboardViewController {
    
    @IBAction func clickedOnOwnQR(_ sender: Any) {
        self.performSegue(withIdentifier: "OwnQRViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnSettings(_ sender: Any) {
        self.performSegue(withIdentifier: "SettingsViewControllerSegue", sender: nil)
//        if let controller = Storyboard.controllerWith(name: "ForgotPasswordViewController") as? ForgotPasswordViewController {
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
    }
    
    @IBAction func clickedOnAddGroup(_ sender: Any) {
        if let controller = Storyboard.controllerWith(name: "GroupDashboardViewController") as? GroupDashboardViewController {
            controller.isFromInviteFriend = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
//        self.performSegue(withIdentifier: "ShopDetailsViewControllerSegue", sender: nil)
//        DataManager.shared.isFromPickup = false
//        DataManager.shared.orderConfirmed = false
    }
    
    @IBAction func clickedOnPickup(_ sender: Any) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "NearbyShopsViewControllerSegue", sender: nil)
            DataManager.shared.isFromPickup = true
            DataManager.shared.orderConfirmed = true
        }
    }
}

//MARK: ScannerViewControllerDelegate methods
extension DashboardViewController: ScannerViewControllerDelegate {
    
    func foundQRValue(with controller: ScannerViewController, code: String) {
        if isLoadingData {return}
        let finalCode = code.replacingOccurrences(of: "\'", with: "\"")
        if previousCode != finalCode {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            if let dic = AppUtility.convertToDictionary(text: finalCode), let object = Mapper<DashboardTableScanDetails>().map(JSON: dic) {
                
                if let id = object.tableId {
                    AppUtility.showProgress(nil, title: nil)
                    getTableInfo(id: id, false) { (tableObject) in
                        if tableObject?.publicField == true {
                            if let restaurant = object.restaurantId, let table = object.tableId {
                                let params: [String:Any] = ["order_type" : 1,
                                                            "restaurant" : restaurant,
                                                            "table": table]
                                self.createOrder(false, params: params) { (success) in
                                    AppUtility.hideProgress(nil)
                                    if success {
                                        if let controller = Storyboard.controllerWith(name: "ShopDetailsViewController") as? ShopDetailsViewController {
                                            DataManager.shared.isFromPickup = false
                                            DataManager.shared.orderConfirmed = false
                                            controller.tableScanDetails = object
                                            self.navigationController?.pushViewController(controller, animated: true)
                                        }
                                    }
                                }
                            } else {
                                AppUtility.hideProgress(nil)
                            }
                        } else {
                            AppUtility.hideProgress(nil)
                            AppUtility.showAlertWithProperty("Alert".localized, messageString: "This table is currently unavailable, thanks.".localized)
                        }
                    }
                }
            }
        }
        previousCode = finalCode
//        scanQRLabel.text = code
    }
}

//MARK: Request methods
extension DashboardViewController {
    
    func getTableInfo(id: Int,_ showLoading: Bool = true, _ completion: @escaping(_ object: TableInfoDetails?) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getTableInfo(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
    
    func createOrder(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.createOrder(params: params, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object?.orderParticipants != nil)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension DashboardViewController {
    
    func updateWithUser() {
        if let user = DataManager.shared.loginDetails?.user?.first {
            if let urlString = user.profilePicture, let url = URL(string: urlString) {
                profileImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                profileImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
            } else {
                profileImageView.image = UIImage.defaultProfile()
            }
            profileImageView.makeCircular = true
        }
    }
    
    func loadDataContents() {
        isLoadingData = true
        AppUtility.showProgress(title: nil)
        let group = DispatchGroup()
        group.enter()
        TaskManager.loadUserDetails(false) { (success) in
            group.leave()
        }
        group.enter()
        TaskManager.getActiveOrderDetails(false) { (object) in
            if let id = object?.lastOrder {
                TaskManager.getOrderDetails(false, id: id) { (orderDetails) in
                    self.activeOrderDetails = orderDetails
                    group.leave()
                }
            } else {
                group.leave()
            }
        }
        group.notify(queue: .main) {
            AppUtility.hideProgress()
            self.updateWithUser()
            if DataManager.shared.loginDetails?.user?.first?.id != nil {
                TaskManager.openTappedNotificationScreen()
            }
            if self.activeOrderDetails?.orderType == 1, let restaurantId = self.activeOrderDetails?.restaurant, let tableId = self.activeOrderDetails?.table {
                self.isLoadingData = false
                if let controller = Storyboard.controllerWith(name: "ShopDetailsViewController") as? ShopDetailsViewController {
                    DataManager.shared.isFromPickup = false
                    DataManager.shared.orderConfirmed = false
                    controller.tableScanDetails = DashboardTableScanDetails.init(restaurantId: restaurantId, tableId: tableId)
                    if !AppUtility.navigationContains(controllerClass: ShopDetailsViewController.classForCoder()) {
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            } else if self.activeOrderDetails?.orderType == 0, let restaurantId = self.activeOrderDetails?.restaurant {
                TaskManager.getRestaurantDetails(true, id: restaurantId) { (object) in
                    self.isLoadingData = false
                    if let controller = Storyboard.controllerWith(name: "ShopDetailsViewController") as? ShopDetailsViewController {
                        DataManager.shared.isFromPickup = true
                        DataManager.shared.orderConfirmed = true
                        controller.restaurantDetails = object
                        if !AppUtility.navigationContains(controllerClass: ShopDetailsViewController.classForCoder()) {
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                }
            } else {
                self.isLoadingData = false
            }
        }
    }
}
