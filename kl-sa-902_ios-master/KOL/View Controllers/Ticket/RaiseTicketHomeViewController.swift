//
//  RaiseTicketHomeViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 8/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class RaiseTicketHomeViewController: UIViewController {

    @IBOutlet weak var ticketCategoriesTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
//    @IBOutlet weak var tableViewParentHeightConstraint: NSLayoutConstraint!
    
    var ticketListDetails: TicketListDetails? {
        didSet {
            if let tickets = ticketListDetails?.tickets {
                ticketList.append(contentsOf: tickets)
            }
            emptyView.isHidden = !(ticketListDetails?.tickets?.count == 0)
        }
    }
    var ticketList = [TicketDetails]() {
        didSet {
            ticketCategoriesTableView.reloadData()
        }
    }
    var isLoadingData = false
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ticketCategoriesTableView.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        ticketCategoriesTableView.addSubview(refreshControl)
        
        getTicketList { (success) in}
    }
}

//MARK: Action methods
extension RaiseTicketHomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ticketList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TicketCategoryTableViewCell") as? TicketCategoryTableViewCell else {
            return UITableViewCell.init()
        }
        
        cell.selectionStyle = .none
        cell.titleLabel.text = ticketList[indexPath.row].localizedText()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "TicketSubmitViewControllerSegue", sender: ticketList[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

//MARK: Segue methods
extension RaiseTicketHomeViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TicketSubmitViewControllerSegue", let ticket = sender as? TicketDetails, let controller = segue.destination as? TicketSubmitViewController {
            controller.ticketDetails = ticket
        }
    }
}

//MARK: Refresh control methods
extension RaiseTicketHomeViewController {
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getTicketList(isNew: true) { (success) in}
        refreshControl.endRefreshing()
    }
}

//MARK: Request methods
extension RaiseTicketHomeViewController {
    
    func getTicketList(isNew: Bool = false, completion: @escaping(_ success: Bool) -> Void) {
        isLoadingData = true
        AppUtility.showProgress(self.view, title: nil)
        if isNew {
            ticketList = []
        }
        var nextPageArguments = "?listing=True&limit=15&offset=\(ticketList.count)"
        if let nextPage = ticketListDetails?.next, nextPageArguments.contains(AppConstants.BaseURL.Current), !isNew {
            let argument = nextPage.replacingOccurrences(of: AppConstants.BaseURL.Current, with: "")
            if argument != "" {
                nextPageArguments = argument
            }
        }
        
        RequestManager.getTicketList(completion: { (ticketListDetails) in
            AppUtility.hideProgress(self.view)
            self.isLoadingData = false
            self.ticketListDetails = ticketListDetails
            completion(ticketListDetails != nil)
        }) { (error, code, message) in
            AppUtility.hideProgress(self.view)
            self.isLoadingData = false
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            print("Error occured")
            completion(false)
        }
    }
}


//MARK: Other methods
extension RaiseTicketHomeViewController {
    
}
