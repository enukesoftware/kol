//
//  TicketSubmitViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 8/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class TicketSubmitViewController: UIViewController {

    @IBOutlet weak var topicTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var ticketDetails: TicketDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        topicTextField.text = ticketDetails?.topic
//        if let text = ticketDetails?.descriptionField, !text.isEmpty {
//            descriptionTextView.text = text
//            descriptionTextView.textColor = .black
//        }
        descriptionTextView.text = "Details".localized
        descriptionTextView.textColor = UIColor.hexStringToUIColor("B4BBC9").withAlphaComponent(0.8)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if LanguageManger.shared.currentLanguage == .en {
            topicTextField.textAlignment = .left
            descriptionTextView.textAlignment = .left
        } else {
            topicTextField.textAlignment = .right
            descriptionTextView.textAlignment = .right
        }
    }
}

//MARK: UITextViewDelegate methods
extension TicketSubmitViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Details".localized {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Details".localized
            textView.textColor = UIColor.hexStringToUIColor("B4BBC9").withAlphaComponent(0.8)
        }
    }
}

//MARK: Action methods
extension TicketSubmitViewController {
    
    @IBAction func clickedOnContinue(_ sender: Any) {
        if !checkValidation() {return}
        if let subTopic  = topicTextField.text, let description = descriptionTextView.text, let id = ticketDetails?.id {
            let params: [String:Any] = ["topic" : id,
                                        "sub_topic" : subTopic,
                                        "description" : description]
            raiseTicketWith(params: params) { (success) in
                if success {
                    TaskManager.moveToViewController(with: SettingsViewController.classForCoder())
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Your ticket sent successfully, thanks.".localized)
                }
            }
        }
    }
}

//MARK: Request methods
extension TicketSubmitViewController {
    
    func raiseTicketWith(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.raiseTicket(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension TicketSubmitViewController {
    
    func checkValidation() -> Bool {
        
        if topicTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter ticket title.".localized)
            return false
        } else if descriptionTextView.text == "Details".localized {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter ticket description.".localized)
            return false
        }
        return true
    }
}
