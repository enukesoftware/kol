//
//  SignUpViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 4/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class SignUpViewController: AbstructViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var addPhotoIconImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var profileImageFile: FileDetails? {
        didSet {
            profileImageView.image = profileImageFile?.image
            profileImageView.makeCircular = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        loadDummyData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if LanguageManger.shared.currentLanguage == .en {
            nameTextField.textAlignment = .left
            phoneTextField.textAlignment = .left
            emailTextField.textAlignment = .left
            passwordTextField.textAlignment = .left
            confirmPasswordTextField.textAlignment = .left
        } else {
            nameTextField.textAlignment = .right
            phoneTextField.textAlignment = .right
            emailTextField.textAlignment = .right
            passwordTextField.textAlignment = .right
            confirmPasswordTextField.textAlignment = .right
        }
    }
}

//MARK: Action methods
extension SignUpViewController {
    
    @IBAction func clickedOnProfilePhoto(_ sender: Any) {
        TaskManager.shared.pickImage(source: sender as? UIView) { (selectedImage) in
            if let image = selectedImage {
                image.resizeImage(targetSize: CGSize.init(width: 500.0, height: 500.0*(image.size.width/image.size.height))) { (resizedImage) in
                    
                    let fileData = resizedImage.jpegData(compressionQuality: 0.75)
                    var fileDetails = FileDetails(fileName: "profile_image.jpg", mimeType: "image/jpeg", fileData: fileData)
                    fileDetails.image =  resizedImage
                    self.profileImageFile = fileDetails
                }
            }
            self.addPhotoIconImageView.isHidden = selectedImage != nil
        }
    }
    
    @IBAction func clickedOnContinue(_ sender: Any) {
        let nameString = self.nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.nameTextField.text = nameString
        self.view.endEditing(true)
        if checkValidation() {
            createUser { (success) in
                if success {
                    self.showVerificationCodeAlert()
                }
            }
        }
    }
    
    @IBAction func clickedOnSignIn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: Request methods
extension SignUpViewController {
    
    func createUser(_ completion: @escaping(_ success: Bool) -> Void) {
        var fileDic:[[String: FileDetails]] = []
        if let file = profileImageFile {
            fileDic.append(["user.profile_picture" : file])
        }
        
        AppUtility.showProgress(nil, title: "Creating...".localized)
        RequestManager.registerWith(modify: false, params: getParameters(), fileDic: fileDic, completion: { (loginDetails, userDetails) in
            AppUtility.hideProgress(nil)
            completion(loginDetails != nil)
        }) { (error, message) in
            AppUtility.hideProgress(nil)
            if let msg = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: msg)
            }
            completion(false)
        }
    }
    
    func verifyAccount(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.verifyAccount(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func resendVerification(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.resendVerification(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
//            if let errorMessage = message {
//                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
//            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension SignUpViewController {
    
    func loadDummyData() {
        nameTextField.text = "Test Name"
        phoneTextField.text = "+8801913243746"
        emailTextField.text = "test@yopmail.com"
        passwordTextField.text = "12345678"
        confirmPasswordTextField.text = "12345678"
    }
    
    func checkValidation() -> Bool {
        
        if nameTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your name.".localized)
            return false
        } else if phoneTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your phone number.".localized)
            return false
        } else if emailTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your email address.".localized)
            return false
        } else if let email = emailTextField.text, !AppUtility.isValidEmail(email: email) {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter a valid email address.".localized)
            return false
        } else if passwordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter password.".localized)
            return false
        } else if confirmPasswordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter confirm password.".localized)
            return false
        } else if let name = nameTextField.text, !AppUtility.hasNoSpecialCharacter(text: name) {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Special characters not allowed".localized)
            return false
        } else if let password = confirmPasswordTextField.text, !AppUtility.isStrongPassword(text: password) {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Weak password. For security purposes kindly provide a password with Capital character, small character, numbers & special character".localized)
            return false
        }
        return true
    }
    
    func getParameters() -> [String: Any] {
        let params: [String: Any] = ["user.name" : nameTextField.text ?? "",
                                     "user.email" : emailTextField.text ?? "",
                                     "user.phone_number" : AppUtility.getPhoneWithCountryCode(value: phoneTextField.text ?? ""),
                                     "user.password" : passwordTextField.text ?? ""]
        
        let params2: [String: Any] = ["user" :["name" : nameTextField.text ?? "",
            "email" : emailTextField.text ?? "",
            "phone_number" : AppUtility.getPhoneWithCountryCode(value: phoneTextField.text ?? ""),
            "password" : passwordTextField.text ?? ""] ]
        
        
        
        
        print("paramsValue2",params2)

        print("paramsValue",params)
        return params
    }
    
    func showVerificationCodeAlert() {
        let alertController = UIAlertController.init(title: "Alert".localized, message: "Please enter verification code sent to your mobile to verify your account.".localized, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter verification code".localized
            textField.keyboardType = .numberPad
        }
        alertController.addAction(UIAlertAction(title: "Send".localized, style: .default, handler: { (alertAction) in
            
            if let code = alertController.textFields?.first?.text {
                let params:[String:Any] = ["phone_number": AppUtility.getPhoneWithCountryCode(value: self.phoneTextField.text ?? ""),
                                                "code": code]
                AppUtility.showProgress(title: nil)
                self.verifyAccount(params: params, { (success) in
                    AppUtility.hideProgress()
                    if success {
                        AlertManager.showAlertWith(title: "Success".localized, message: "You account verified successfully, now you can login.".localized, actions: [CustomAction(index: 0, title: "Ok".localized, style: .default)]) { (alertAction) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                })
            }
        }))
        alertController.addAction(UIAlertAction.init(title: "Resend".localized, style: .default, handler: { (alertAction) in
            let params:[String:Any] = ["phone_number": AppUtility.getPhoneWithCountryCode(value: self.phoneTextField.text ?? "")]
            self.resendVerification(params: params) { (success) in
//                AlertManager.showAlertWith(title: "Success", message: "Resend verification code successfully.", nil)
                self.showVerificationCodeAlert()
            }
        }))
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
}
