//
//  ResetPasswordViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var phoneNumber = ""
    var verificationCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

//MARK: Action methods
extension ResetPasswordViewController {
    
    @IBAction func clickedOnReset(_ sender: Any) {
        self.view.endEditing(true)
        if checkValidation() {
            let params: [String:Any] = ["phone_number" : phoneNumber,
                                        "code" : verificationCode,
                                        "new_password" : confirmPasswordTextField.text ?? ""]
            resetPassword(params: params) { (success) in
                if success {
                    self.navigationController?.popToRootViewController(animated: true)
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Your password changed successfully.".localized)
                }
            }
        }
    }
}

//MARK: Request methods
extension ResetPasswordViewController {
    
    func resetPassword(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.resetPassword(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension ResetPasswordViewController {
    
    func checkValidation() -> Bool {
        
        if newPasswordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter new password.".localized)
            return false
        } else if confirmPasswordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter confirm password.".localized)
            return false
        } else if newPasswordTextField.text != confirmPasswordTextField.text {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "New and confirm password are not matched, please verify.".localized)
            return false
        } else if let password = newPasswordTextField.text, !AppUtility.isStrongPassword(text: password) {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Weak password. For security purposes kindly provide a password with Capital character, small character, numbers & special character".localized)
            return false
        }
        return true
    }
}
