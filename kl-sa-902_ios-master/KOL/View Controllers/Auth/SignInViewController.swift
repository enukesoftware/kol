//
//  SignInViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 3/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var languageChangeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        loadDummyData()
        languageChangeButton.setTitle(LanguageManger.shared.currentLanguage == .ar ?  "English" : "اللغة العربية", for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if LanguageManger.shared.currentLanguage == .en {
            phoneTextField.textAlignment = .left
            passwordTextField.textAlignment = .left
        } else {
            phoneTextField.textAlignment = .right
            passwordTextField.textAlignment = .right
        }
    }
}

//MARK: Action methods
extension SignInViewController {
    
    @IBAction func clickedOnForgotPassword(_ sender: Any) {
        if let controller = Storyboard.controllerWith(name: "ForgotPasswordViewController") as? ForgotPasswordViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func clickedOnContinue(_ sender: Any) {
        self.view.endEditing(true)
        if checkValidation(), let phone = phoneTextField.text, let password = passwordTextField.text {
            let loginObject:[String:Any] = ["phone_number": AppUtility.getPhoneWithCountryCode(value: phone),
                                            "password": password]
            loginWith(params: loginObject) { (success) in
                if success {
                    if !AppUtility.isSimulator() {AppUtility.subscribeNotification()}
                    self.performSegue(withIdentifier: "DashboardViewControllerSegue", sender: nil)
                }
            }
        }
    }
    
    @IBAction func clickedOnSignUp(_ sender: Any) {
        self.performSegue(withIdentifier: "SignUpViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnLanguage(_ sender: Any) {
        LanguageManger.shared.currentLanguage = (LanguageManger.shared.currentLanguage == .ar) ? .en : .ar
        AppUtility.changeLanguage()
    }
}

//MARK: Request methods
extension SignInViewController {
    
    func loginWith(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: "Checking...".localized)
        RequestManager.loginWith(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension SignInViewController {
    
    func loadDummyData() {
        phoneTextField.text = "01673166782"
        passwordTextField.text = "12345678"
    }
    
    func checkValidation() -> Bool {
        
        if phoneTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your phone number.".localized)
            return false
        } else if passwordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter password.".localized)
            return false
        }
        return true
    }
}
