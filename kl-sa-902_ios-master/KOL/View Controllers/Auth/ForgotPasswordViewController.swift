//
//  ForgotPasswordViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 4/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

//MARK: Action methods
extension ForgotPasswordViewController {
    
    @IBAction func clickedOnSendCode(_ sender: Any) {
        self.view.endEditing(true)
        if checkValidation(), let phone = phoneTextField.text {
            let params:[String:Any] = ["phone_number": AppUtility.getPhoneWithCountryCode(value: phone)]
            forgotPasswordRequest(params: params) { (success) in
                if success {
                    self.performSegue(withIdentifier: "VerifyCodeViewControllerSegue", sender: nil)
                }
            }
        }
    }
}

//MARK: Request methods
extension ForgotPasswordViewController {
    
    func forgotPasswordRequest(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.forgotPassword(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Segue related methods
extension ForgotPasswordViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VerifyCodeViewControllerSegue", let controller = segue.destination as? VerifyCodeViewController {
            controller.phoneNumber = AppUtility.getPhoneWithCountryCode(value: phoneTextField.text ?? "")
        }
    }
}

//MARK: Other methods
extension ForgotPasswordViewController {
    
    func checkValidation() -> Bool {
        
        if phoneTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your phone number.".localized)
            return false
        }
        return true
    }
}
