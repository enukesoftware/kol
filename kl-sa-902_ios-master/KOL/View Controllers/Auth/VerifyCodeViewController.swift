//
//  VerifyCodeViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class VerifyCodeViewController: UIViewController {

    @IBOutlet weak var codeNumberInputView: CodeNumberInputView!
    
    var fromChangePhone = false
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

//MARK: Action methods
extension VerifyCodeViewController {
    
    @IBAction func clickedOnContinue(_ sender: Any) {
        self.view.endEditing(true)
        if fromChangePhone {
            if checkValidation() {
                let params: [String:Any] = ["new_phone_number" : phoneNumber,
                                            "code" : codeNumberInputView.getPinNumber()]
                changePhoneVerify(params: params) { (success) in
                    if success {
                        DataManager.shared.loginDetails?.user?.first?.phoneNumber = self.phoneNumber
                        AuthManager.saveLoginDetails()
                        TaskManager.moveToViewController(with: SettingsViewController.classForCoder())
                        AppUtility.showAlertWithProperty("Success".localized, messageString: "Your phone number changed successfully.".localized)
                    }
                }
            }
        } else {
            if checkValidation() {
//                let params: [String:Any] = ["phone_number" : phoneNumber,
//                                            "code" : codeNumberInputView.getPinNumber()]
//                forgotPasswordVerifyAccount(params: params) { (success) in
//                    if success {
//                        
//                    }
//                }
                self.performSegue(withIdentifier: "ResetPasswordViewControllerSegue", sender: nil)
            }
        }
    }
    
    @IBAction func clickedOnResend(_ sender: Any) {
        self.view.endEditing(true)
        let params: [String:Any] = ["phone_number" : phoneNumber]
        resendVerification(params: params) { (success) in
            if success {
                AppUtility.showAlertWithProperty("Success".localized, messageString: "Verification code sent successfully.".localized)
            }
        }
    }
}

//MARK: Segue related methods
extension VerifyCodeViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ResetPasswordViewControllerSegue", let controller = segue.destination as? ResetPasswordViewController {
            controller.phoneNumber = phoneNumber
            controller.verificationCode = codeNumberInputView.getPinNumber()
        }
    }
}

//MARK: Request methods
extension VerifyCodeViewController {
    
    func changePhoneVerify(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.changePhoneVerify(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func forgotPasswordVerifyAccount(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.forgotPasswordPhoneVerify(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func resendVerification(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.resendVerification(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension VerifyCodeViewController {
    
    func checkValidation() -> Bool {
        
        if codeNumberInputView.getPinNumber().count != 4 {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter otp code.".localized)
            return false
        }
        return true
    }
}
