//
//  SearchFoodViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 21/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol SearchFoodViewControllerDelegate {
    func clickedOnFood(with controller: SearchFoodViewController, item: FoodItemDetails)
}

class SearchFoodViewController: UIViewController {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var foodListTableView: UITableView!
    
    @IBOutlet weak var foodListTableViewHeightConstraint: NSLayoutConstraint!
    
    var delegate: SearchFoodViewControllerDelegate?
    var restaurantDetails: RestaurantDetails?
    var foodItemListDetails: FoodItemListDetails? {
        didSet {
            foodItemList = foodItemListDetails?.items ?? []
        }
    }
    var foodItemList = [FoodItemDetails]() {
        didSet {
            foodListTableView.reloadData()
        }
    }
    var isLoadingData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if LanguageManger.shared.currentLanguage == .en {
            searchTextField.textAlignment = .left
        } else {
            searchTextField.textAlignment = .right
        }
    }
}

//MARK: Action methods
extension SearchFoodViewController {
    
    @IBAction func clickedOnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension SearchFoodViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodItemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell") as? SearchTableViewCell else {
            return UITableViewCell.init()
        }
        cell.selectionStyle = .none
        cell.nameLabel.text = foodItemList[indexPath.row].localizedName()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.delegate?.clickedOnFood(with: self, item: self.foodItemList[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
}

//MARK: UITextFieldDelegate methods
extension SearchFoodViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let textFieldString = textField.text, let swtRange = Range(range, in: textFieldString) {
            let fullString = textFieldString.replacingCharacters(in: swtRange, with: string)
            
            if fullString.count > 0 {
                foodListTableView.isHidden = false
                searchView.backgroundColor = .white
                if !isLoadingData {
                    getRestaurantFoodItems(query: fullString) { (success) in}
                }
            } else {
                foodListTableView.isHidden = true
                searchView.backgroundColor = .clear
            }
        }
        
        return true
    }
}

//MARK: Request methods
extension SearchFoodViewController {
    
    func getRestaurantFoodItems(_ showLoading: Bool = true, query: String, completion: @escaping(_ success: Bool) -> Void) {
        guard let id = restaurantDetails?.user?.id else {
            completion(false)
            return
        }
        isLoadingData = true
        if showLoading {AppUtility.showProgress(title: nil)}
        let arguments = "&limit=50&offset=0&name__icontains=\(query)"
        RequestManager.getRestaurantFoodItems(id: id, extraArgument: arguments, completion: { (object) in
            self.isLoadingData = false
            if showLoading {AppUtility.hideProgress()}
            self.foodItemListDetails = object
            completion(self.foodItemListDetails?.items != nil)
        }) { (error, code, message) in
            self.isLoadingData = false
            if showLoading {AppUtility.hideProgress()}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}
