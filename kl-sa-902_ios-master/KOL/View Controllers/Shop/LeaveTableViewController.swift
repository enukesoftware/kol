//
//  LeaveTableViewController.swift
//  KOL
//
//  Created by Rupak on 26/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol LeaveTableViewControllerDelegate {
    func clickedOnLeave(with controller: LeaveTableViewController)
}

class LeaveTableViewController: UIViewController {

    @IBOutlet weak var detailsLabel: UILabel!
    
    var delegate: LeaveTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailsLabel.text = DataManager.shared.isFromPickup ? "Do you want to leave this order?".localized : "Do you want to leave this table and cancel your order?".localized
    }
}

//MARK: Action methods
extension LeaveTableViewController {
    
    @IBAction func clickedOnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedOnYes(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.clickedOnLeave(with: self)
        }
    }
}
