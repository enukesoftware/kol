//
//  SearchFoodResultViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 21/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class SearchFoodResultViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var foodListCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchTextField.text = "Chicken".localized
    }
}

//MARK: Action methods
extension SearchFoodResultViewController {
    
    @IBAction func clickedOnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: UICollectionViewDataSource and UICollectionViewDelegate methods
extension SearchFoodResultViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
            return UICollectionViewCell.init()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width-47)/2
        var height = width*1.3
        if AppUtility.isiPad() {
            height = width
        }
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let controller = Storyboard.controllerWith(name: "ProductDetailsViewController") as? ProductDetailsViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
