//
//  ShopDetailsViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 12/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class ShopDetailsViewController: UIViewController {

    @IBOutlet weak var productsCollectionView: UICollectionView!
    @IBOutlet weak var topVisualEffectView: UIVisualEffectView!
    @IBOutlet weak var bottomActionView: UIView!
    @IBOutlet weak var topCategoryView: UIView!
    @IBOutlet weak var topCategoryCollectionView: UICollectionView!
    @IBOutlet weak var imageStackView: ImageStackView!
    @IBOutlet weak var cartControl: UIControl!
    @IBOutlet weak var cartItemCountLabel: UILabel!
    @IBOutlet weak var orderListView: ShoppingCartListView!
    
    @IBOutlet weak var listHeaderTopHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderListViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cartBottomHeightConstraint: NSLayoutConstraint!
    
    var categoryView: UIView!
    var headerView: ShopHeaderCollectionReusableView!
    var shopHeaderHeight: CGFloat = 0
    var selectedTabIndex = 0
    var movedTopCategoryView = false
    var listViewOpen = false {
        didSet {
            if listViewOpen {
                orderListViewHeightConstraint.constant = self.view.frame.height+10
                orderListView.cornerRadius = 0
                orderListView.listTopIndicatorView.isHidden = true
            } else {
                orderListViewHeightConstraint.constant = self.view.frame.height/1.5
                orderListView.cornerRadius = 10
                orderListView.listTopIndicatorView.isHidden = false
            }
            listHeaderTopHeightConstraint.constant = listViewOpen ? 48 : 8
            orderListView.listBackButton.isHidden = !orderListView.listTopIndicatorView.isHidden
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    var restaurantDetails: RestaurantDetails?
    var tableScanDetails: DashboardTableScanDetails?
    var isLoadingData = false
    var foodItemListDetails: FoodItemListDetails? {
        didSet {
            if let items = foodItemListDetails?.items {
                foodItemList.append(contentsOf: items)
            }
        }
    }
    var foodItemList = [FoodItemDetails]() {
        didSet {
            productsCollectionView.reloadData()
        }
    }
    var foodCategoryListDetails: FoodCategoryListDetails? {
        didSet {
            if let categories = foodCategoryListDetails?.categories {
                foodCategoryList = categories
                headerView.categoryList = categories
                topCategoryCollectionView.reloadData()
            }
        }
    }
    var foodCategoryList = [FoodCategoryDetails]()
    var orderId = -1
    var orderDetails: OrderDetails?
    var selectedCategoryId = -1
    var orderItemList = [OrderItemDetails]() {
        didSet {
            if orderItemList.count > 0 {
                cartControl.isHidden = false
                cartItemCountLabel.text = "\(orderItemList.count)"
                cartBottomHeightConstraint.constant = bottomActionView.isHidden ? 48 : 100
            } else {
                DataManager.shared.orderConfirmed = false
                cartControl.isHidden = true
                self.listViewOpen = false
                self.orderListView.isHidden = true
            }
            DataManager.shared.cartCount = orderItemList.count
            orderListView.orderItemCountLabel.text = String(format: "%02d " + "items".localized, orderItemList.count)
            orderListView.participantList = orderDetails?.orderParticipants ?? []
            orderListView.orderItemList = orderItemList
            //Update with checkout
            var isConfirmed = true
            for item in orderItemList {
                if item.status != 1 {
                    isConfirmed = false
                    break
                }
            }
            DataManager.shared.orderConfirmed = isConfirmed
            orderListView.confirmButton.setTitle(DataManager.shared.orderConfirmed ? "CHECKOUT".localized : "CONFIRM ORDER".localized, for: .normal)
        }
    }
    var activeOrderDetails: ActiveOrderDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        orderListView.orderListTableView.estimatedRowHeight = 120
        orderListView.orderListTableView.rowHeight = UITableView.automaticDimension
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        orderListView.addGestureRecognizer(gesture)
        gesture.delegate = self
        listViewOpen = false
        orderListView.confirmButton.setTitle(DataManager.shared.orderConfirmed ? "CHECKOUT".localized : "CONFIRM ORDER".localized, for: .normal)
        orderListView.delegate = self
        
        //Load api contents
        loadInitialApiContentsAndUpdateUI()
        DataManager.shared.pendingRating = UserDefaults.standard.value(forKey: "pendingRating") as? Bool ?? false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if DataManager.shared.cartCount != 0 {
            cartControl.isHidden = false
            cartItemCountLabel.text = "\(DataManager.shared.cartCount)"
            cartBottomHeightConstraint.constant = bottomActionView.isHidden ? 48 : 100
        } else {
            DataManager.shared.orderConfirmed = false
            cartControl.isHidden = true
            self.listViewOpen = false
            self.orderListView.isHidden = true
        }
    }
}

//MARK: Action methods
extension ShopDetailsViewController {
    
    @IBAction func clickedOnLeave(_ sender: Any) {
        self.performSegue(withIdentifier: "LeaveTableViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnSearch(_ sender: Any) {
        
        if let controller = Storyboard.controllerWith(name: "SearchFoodViewController") as? SearchFoodViewController {
            controller.modalPresentationStyle = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            controller.delegate = self
            controller.restaurantDetails = restaurantDetails
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func clickedOnScanUser(_ sender: UIButton) {
        if imageStackView.imageViewList.count < 40 {
            self.performSegue(withIdentifier: "ScanUserViewControllerSegue", sender: nil)
        } else {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Too many users in your table.".localized)
        }
    }
    
    @IBAction func clickedOnCart(_ sender: Any) {
        if orderItemList.count == 0 {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Cart is empty, please add product first.".localized)
            return
        } else {
            orderListView.isHidden = false
            orderListView.orderListTableView.reloadData()
        }
    }
    
    @IBAction func clickedOnListClose(_ sender: Any) {
        self.listViewOpen = false
        self.orderListView.isHidden = true
    }
    
    @IBAction func clickedOnAddFriend(_ sender: Any) {
        self.performSegue(withIdentifier: "GroupDashboardViewControllerSegue", sender: false)
    }
    
    @IBAction func clickedOnInviteFriend(_ sender: Any) {
        self.performSegue(withIdentifier: "GroupDashboardViewControllerSegue", sender: true)
    }
    
    @IBAction func clickedOnConfirmOrder(_ sender: Any) {
        let params: [String:Any] = ["order_type" : DataManager.shared.isFromPickup ? 0 : 1]
        if DataManager.shared.orderConfirmed, let id = orderDetails?.id {
            if activeOrderDetails?.lastOrderInCheckout == true {
                if let controller = Storyboard.controllerWith(name: "CheckoutViewController") as? CheckoutViewController {
                    controller.orderDetails = self.orderDetails
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            } else {
                let params: [String:Any] = ["order" : id]
                self.createInvoice(params: params) { (success) in
                    if success {
                        if let controller = Storyboard.controllerWith(name: "CheckoutViewController") as? CheckoutViewController {
                            controller.orderDetails = self.orderDetails
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                }
            }
        } else {
            TaskManager.confirmOrder(id: orderId, params: params) { (success) in
                if success {
                    self.refreshOrderItems { (success) in
                        if let controller = Storyboard.controllerWith(name: "OrderSuccessViewController") as? OrderSuccessViewController {
                            controller.modalTransitionStyle = .crossDissolve
                            controller.modalPresentationStyle = .overFullScreen
                            self.present(controller, animated: true, completion: nil)
                        }
                        DataManager.shared.orderConfirmed = true
                        self.orderListView.confirmButton.setTitle(DataManager.shared.orderConfirmed ? "CHECKOUT".localized : "CONFIRM ORDER".localized, for: .normal)
                    }
                }
            }
        }
    }
}

//MARK: UICollectionViewDataSource and UICollectionViewDelegate methods
extension ShopDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == topCategoryCollectionView {
            return foodCategoryList.count
        }
        return foodItemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == topCategoryCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as? CategoryCollectionViewCell else {
                return UICollectionViewCell.init()
            }
            cell.titleLabel.text = foodCategoryList[indexPath.row].localizedName()
            if selectedTabIndex == indexPath.row {
                cell.isSelected = true
            } else {
                cell.isSelected = false
            }
            
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
            return UICollectionViewCell.init()
        }
        cell.updateWith(item: foodItemList[indexPath.row])
        if foodItemList.count > 0, indexPath.row > foodItemList.count - 6, foodItemListDetails?.next != nil, !isLoadingData, let id = restaurantDetails?.user?.id {
            getRestaurantFoodItems(isNew: false, true, id: id) { (success) in}
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == topCategoryCollectionView {
            let size = ((foodCategoryList[indexPath.row].localizedName())! as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .medium)])
            let itemWidth = size.width + 60
            return CGSize(width: itemWidth, height: collectionView.frame.size.height)
        }
        let width = (collectionView.frame.width-47)/2
        var height = width*1.3
        if AppUtility.isiPad() {
            height = width
        }
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == topCategoryCollectionView {
            selectedTabIndex = indexPath.row
            self.topCategoryCollectionView?.reloadData()
            self.topCategoryCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            headerView.selectedTabIndex = indexPath.row
            headerView.categoryCollectionView.reloadData()
            headerView.categoryCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            if let id = foodCategoryList[indexPath.row].id, let restaurantId = restaurantDetails?.user?.id {
                selectedCategoryId = id
                getRestaurantFoodItems(isNew: true, true, id: restaurantId) { (success) in}
            }
            return
        }
        if let controller = Storyboard.controllerWith(name: "ProductDetailsViewController") as? ProductDetailsViewController {
            controller.productDelegate = self
            controller.foodItemDetails = foodItemList[indexPath.row]
            controller.orderDetails = orderDetails
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView == topCategoryCollectionView {
            return CGSize.zero
        }
        shopHeaderHeight = (view.frame.width-32)*0.7142857143 + 228
        return CGSize(width: collectionView.frame.width, height: shopHeaderHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ShopHeaderCollectionReusableView", for: indexPath) as? ShopHeaderCollectionReusableView else {
                return UICollectionReusableView.init()
            }
            headerView.delegate = self
            categoryView = headerView.categoryView
            categoryView.round(corners: [.topLeft, .topRight], radius: 10)
            headerView.backgroundColor = UIColor.hexStringToUIColor("F7F7FA")
            self.headerView = headerView
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
        return UICollectionReusableView.init()
    }
}

//MARK: - ShopHeaderCollectionReusableViewDelegate methods
extension ShopDetailsViewController: ShopHeaderCollectionReusableViewDelegate {
    
    func clickedOnItem(with view: ShopHeaderCollectionReusableView, indexPath: IndexPath) {
        
        headerView.selectedTabIndex = indexPath.row
        headerView.categoryCollectionView.reloadData()
        headerView.categoryCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        selectedTabIndex = indexPath.row
        self.topCategoryCollectionView?.reloadData()
        self.topCategoryCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        if let id = foodCategoryList[indexPath.row].id, let restaurantId = restaurantDetails?.user?.id {
            selectedCategoryId = id
            getRestaurantFoodItems(isNew: true, true, id: restaurantId) { (success) in}
        }
    }
    
    func updatedScrollPosition(with view: ShopHeaderCollectionReusableView) {
        topCategoryCollectionView.contentOffset.x = headerView.categoryCollectionView.contentOffset.x
    }
}

//MARK: - SearchFoodViewControllerDelegate methods
extension ShopDetailsViewController: SearchFoodViewControllerDelegate {
    
    func clickedOnFood(with controller: SearchFoodViewController, item: FoodItemDetails) {
        if let controller = Storyboard.controllerWith(name: "ProductDetailsViewController") as? ProductDetailsViewController {
            controller.productDelegate = self
            controller.foodItemDetails = item
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

// MARK: - ShoppingCartListViewDelegate methods
extension ShopDetailsViewController: ShoppingCartListViewDelegate {
    
    func clickedOnEdit(with view: ShoppingCartListView, indexPath: IndexPath) {
        if let controller = Storyboard.controllerWith(name: "ProductDetailsViewController") as? ProductDetailsViewController {
            controller.productDelegate = self
            controller.orderDetails = orderDetails
            controller.orderItemDetails = orderItemList[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func clickedOnCancel(with view: ShoppingCartListView, indexPath: IndexPath) {
        if let id = orderItemList[indexPath.row].id {
            TaskManager.cancelOrderItem(id: id) { (success) in
                if success, self.orderId != -1 {
                    TaskManager.getOrderItems(true, id: self.orderId) { (objects) in
                        self.orderItemList = objects
                    }
                }
            }
        }
    }
}

// MARK: - ProductDetailsViewControllerDelegate methods
extension ShopDetailsViewController: ProductDetailsViewControllerDelegate {
    
    func shouldRefreshOrder(controller: ProductDetailsViewController) {
        refreshOrderItems { (success) in}
    }
}

//MARK: - segue related methods
extension ShopDetailsViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ScanUserViewControllerSegue", let controller = segue.destination as? ScanUserViewController {
            controller.delegate = self
        } else if segue.identifier == "LeaveTableViewControllerSegue", let controller = segue.destination as? LeaveTableViewController {
            controller.delegate = self
        } else if segue.identifier == "GroupDashboardViewControllerSegue", let controller = segue.destination as? GroupDashboardViewController, let isFromInviteFriend = sender as? Bool {
            controller.isFromInviteFriend = isFromInviteFriend
            DataManager.shared.runningOrderDetails = orderDetails
        }
    }
}

// MARK: - ScanUserViewControllerDelegate methods
extension ShopDetailsViewController: ScanUserViewControllerDelegate {
    
    func scannedUser(with controller: ScanUserViewController, details: String) {
        if details.count > 0, orderId != -1 {
            let params: [String:Any] = ["invited_users": [details],
                                        "order" : orderId]
            TaskManager.inviteOrder(params: params) { (success) in
                if success {
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Invitation sent successfully.".localized)
                }
            }
        }
        /*if let controller = Storyboard.controllerWith(name: "InvitationAlertViewController") as? InvitationAlertViewController {
            controller.modalPresentationStyle = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: true, completion: nil)
            controller.takenAction = { (accepted) in
                if accepted {
                    self.imageStackView.addNewImage(image: UIImage(named: "own_profile"))
                }
            }
        }*/
    }
}

// MARK: - LeaveTableViewControllerDelegate methods
extension ShopDetailsViewController: LeaveTableViewControllerDelegate {
    
    func clickedOnLeave(with controller: LeaveTableViewController) {
        if orderId != -1 {
            TaskManager.leaveOrder(id: orderId) { (success) in
                if success {
                    DataManager.shared.runningOrderDetails = nil
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

//MARK: UIGestureRecognizerDelegate methods
extension ShopDetailsViewController: UIGestureRecognizerDelegate {
    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizer.State.ended || gestureRecognizer.state == UIGestureRecognizer.State.changed {
            let translation = gestureRecognizer.translation(in: self.view)
            
            if orderListViewHeightConstraint.constant-translation.y > self.view.frame.height/1.5, !listViewOpen {
                orderListViewHeightConstraint.constant = orderListViewHeightConstraint.constant+translation.y
            }
            if gestureRecognizer.state == .ended {
                listViewOpen = (orderListViewHeightConstraint.constant > self.view.center.y)
            } else {
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
        }
    }
}

// MARK: - UIScrollViewDelegate
extension ShopDetailsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == orderListView.orderListTableView {
            return
        }
        if scrollView == topCategoryCollectionView {
            return
        }
        let categoryBottomHeight = shopHeaderHeight-146
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        if translation.y > 0 {
            bottomActionView.isHidden = false
            cartBottomHeightConstraint.constant = 100
            if scrollView.contentOffset.y < categoryBottomHeight {
                self.topCategoryView.isHidden = true
            }
        } else {
            bottomActionView.isHidden = true
            cartBottomHeightConstraint.constant = 48
            if scrollView.contentOffset.y > categoryBottomHeight {
                self.topCategoryView.isHidden = false
            }
        }
        UIView.animate(withDuration: 0.05) {
            self.view.layoutIfNeeded()
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (!decelerate) {
            if scrollView == topCategoryCollectionView {
                headerView.categoryCollectionView.contentOffset.x = topCategoryCollectionView.contentOffset.x
                movedTopCategoryView = false
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == topCategoryCollectionView {
            headerView.categoryCollectionView.contentOffset.x = topCategoryCollectionView.contentOffset.x
            movedTopCategoryView = false
        }
    }
}

//MARK: Request methods
extension ShopDetailsViewController {
    
    func getRestaurantFoodItems(isNew: Bool = false, _ showLoading: Bool = true, id: Int, completion: @escaping(_ success: Bool) -> Void) {
        isLoadingData = true
        if showLoading {AppUtility.showProgress(self.view, title: nil)}
        if isNew {
            foodItemList = []
        }
        var nextPageArguments = "&limit=15&offset=0"
        if selectedCategoryId != -1 {
            nextPageArguments += "&category=\(selectedCategoryId)"
        }
        if let nextPage = foodItemListDetails?.next, !nextPage.isEmpty, !isNew {
            nextPageArguments = nextPage
        }
        
        RequestManager.getRestaurantFoodItems(id: id, extraArgument: nextPageArguments, completion: { (object) in
            if showLoading {AppUtility.hideProgress(self.view)}
            self.isLoadingData = false
            self.foodItemListDetails = object
            completion(self.foodItemListDetails?.items != nil)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(self.view)}
            self.isLoadingData = false
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            print("Error occured")
            completion(false)
        }
    }
    
    func getFoodCategoryListDetails(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.getRestaurantFoodCategories(id: id, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            self.foodCategoryListDetails = object
            completion(object != nil)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func createOrder(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.createOrder(params: params, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let participants = object?.orderParticipants {
                self.imageStackView.participantList = participants
            }
            self.orderId = object?.id ?? -1
            self.orderDetails = object
            completion(object?.orderParticipants != nil)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func acceptOrderItem(id: String, params: [String:Any], _ completion: @escaping(_ object: AcceptOrderResponseDetails?) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.acceptOrderItem(id: id, params: params, completion: { (object) in
            AppUtility.hideProgress(nil)
            completion(object)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
    
    func createInvoice(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.createInvoice(params: params, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object?.id != nil)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension ShopDetailsViewController {
    
    func updateWithRestaurantDetails() {
        if let urlString = restaurantDetails?.coverPicture, let url = URL(string: urlString) {
            headerView?.bannerImageView?.sd_imageIndicator = SDWebImageActivityIndicator.gray
            headerView?.bannerImageView?.sd_setImage(with: url, placeholderImage: UIImage.noImage(), options: .highPriority, completed: nil)
        } else {
            headerView?.bannerImageView?.image = UIImage.noImage()
        }
        if let urlString = restaurantDetails?.user?.profilePicture, let url = URL(string: urlString) {
            headerView?.shopLogoImageView?.sd_imageIndicator = SDWebImageActivityIndicator.gray
            headerView?.shopLogoImageView?.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
            headerView?.shopLogoImageView.makeCircular = true
        } else {
            headerView?.shopLogoImageView?.image = UIImage.defaultProfile()
            headerView?.shopLogoImageView?.makeCircular = true
        }
        headerView?.shopNameLabel.text = restaurantDetails?.user?.localizedName() ?? "N/A"
        headerView?.shopCategoryLabel.text = restaurantDetails?.restaurantType?.finalName() ?? "N/A"
        headerView?.ratingLabel.text = String(format: "(%.1f)", restaurantDetails?.rating ?? 0)
        headerView?.ratingView.rating = restaurantDetails?.rating ?? 0
    }
    
    func loadInitialApiContentsAndUpdateUI() {
        updateWithRestaurantDetails()
        if let id = tableScanDetails?.restaurantId {
            AppUtility.showProgress(nil, title: nil)
            let group = DispatchGroup()
            group.enter()
            TaskManager.getRestaurantDetails(false, id: id) { (object) in
                if object?.user?.id != nil {
                    self.restaurantDetails = object
                    self.updateWithRestaurantDetails()
                }
                group.leave()
            }
            group.enter()
            getFoodCategoryListDetails(false, id: id) { (success) in
                if self.foodCategoryList.count > 0, let categoryId = self.foodCategoryList.first?.id {
                    self.selectedCategoryId = categoryId
                    self.getRestaurantFoodItems(isNew: true, false, id: id) { (success) in
                        group.leave()
                    }
                } else {
                    self.getRestaurantFoodItems(isNew: true, false, id: id) { (success) in
                        group.leave()
                    }
                }
            }
            group.enter()
            TaskManager.getActiveOrderDetails(false) { (object) in
                self.activeOrderDetails = object
                DataManager.shared.pendingRating = object?.lastOrderInRating ?? false
                if let orderId = object?.lastOrder {
                    TaskManager.getOrderDetails(false, id: orderId) { (object) in
                        self.orderDetails = object
                        if object?.id != nil {
                            self.orderId = orderId
                            self.imageStackView.participantList = object?.orderParticipants ?? []
                            TaskManager.getOrderItems(false, id: orderId) { (objects) in
                                self.orderItemList = objects
                                group.leave()
                            }
                        } else {
                            group.leave()
                        }
//                        else {
//                            //Create order
//                            if let restaurant = self.tableScanDetails?.restaurantId, let table = self.tableScanDetails?.tableId {
//                                let params: [String:Any] = ["order_type" : 1,
//                                                            "restaurant" : restaurant,
//                                                            "table": table]
//                                self.createOrder(false, params: params) { (success) in
//                                    group.leave()
//                                }
//                            } else {
//                                group.leave()
//                            }
//                        }
                    }
                } else {
                    group.leave()
                }
//                else {
//                    //Create order
//                    if let restaurant = self.tableScanDetails?.restaurantId, let table = self.tableScanDetails?.tableId {
//                        let params: [String:Any] = ["order_type" : 1,
//                                                    "restaurant" : restaurant,
//                                                    "table": table]
//                        self.createOrder(false, params: params) { (success) in
//                            group.leave()
//                        }
//                    } else {
//                        group.leave()
//                    }
//                }
            }
            group.notify(queue: .main) {
                AppUtility.hideProgress()
                //if failed to create order
                if self.orderDetails?.id == nil {
                    self.navigationController?.popViewController(animated: true)
                    AppUtility.showAlertWithProperty("Alert".localized, messageString: "Failed to create order, please try again later.".localized)
                    return
                }
                DataManager.shared.runningOrderDetails = self.orderDetails
                //Load checkout active order
                if self.activeOrderDetails?.lastOrderInCheckout == true {
                    if let controller = Storyboard.controllerWith(name: "CheckoutViewController") as? CheckoutViewController {
                        controller.orderDetails = self.orderDetails
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                } else if DataManager.shared.pendingRating == true {
                    if let controller = Storyboard.controllerWith(name: "RatingViewController") as? RatingViewController {
                        controller.orderDetails = self.orderDetails
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }
        } else {
            if let id = restaurantDetails?.user?.id {
                AppUtility.showProgress(nil, title: nil)
                let group = DispatchGroup()
                group.enter()
                getFoodCategoryListDetails(false, id: id) { (success) in
                    if self.foodCategoryList.count > 0, let categoryId = self.foodCategoryList.first?.id {
                        self.selectedCategoryId = categoryId
                        self.getRestaurantFoodItems(isNew: true, false, id: id) { (success) in
                            group.leave()
                        }
                    } else {
                        self.getRestaurantFoodItems(isNew: true, false, id: id) { (success) in
                            group.leave()
                        }
                    }
                }
                group.enter()
                TaskManager.getActiveOrderDetails(false) { (object) in
                    self.activeOrderDetails = object
                    DataManager.shared.pendingRating = object?.lastOrderInRating ?? false
                    if let orderId = object?.lastOrder {
                        TaskManager.getOrderDetails(false, id: orderId) { (object) in
                            self.orderDetails = object
                            if object?.id != nil {
                                self.orderId = orderId
                                self.imageStackView.participantList = object?.orderParticipants ?? []
                                TaskManager.getOrderItems(false, id: orderId) { (objects) in
                                    self.orderItemList = objects
                                    group.leave()
                                }
                            } else {
                                //Create order
                                let params: [String:Any] = ["order_type" : 0,
                                                            "restaurant" : id]
                                self.createOrder(false, params: params) { (success) in
                                    group.leave()
                                }
                            }
                        }
                    } else {
                        //Create order
                        let params: [String:Any] = ["order_type" : 0,
                                                    "restaurant" : id]
                        self.createOrder(false, params: params) { (success) in
                            group.leave()
                        }
                    }
                }
                group.notify(queue: .main) {
                    AppUtility.hideProgress()
                    //if failed to create order
                    if self.orderDetails?.id == nil {
                        self.navigationController?.popViewController(animated: true)
                        AppUtility.showAlertWithProperty("Alert".localized, messageString: "Failed to create order, please try again later.".localized)
                        return
                    }
                    DataManager.shared.runningOrderDetails = self.orderDetails
                    //Load checkout active order
                    if self.activeOrderDetails?.lastOrderInCheckout == true {
                        if let controller = Storyboard.controllerWith(name: "CheckoutViewController") as? CheckoutViewController {
                            controller.orderDetails = self.orderDetails
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    } else if DataManager.shared.pendingRating == true {
                        if let controller = Storyboard.controllerWith(name: "RatingViewController") as? RatingViewController {
                            controller.orderDetails = self.orderDetails
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func refreshOrderItems(_ completion: @escaping(_ success: Bool) -> Void) {
        TaskManager.getOrderItems(false, id: orderId) { (objects) in
            self.orderItemList = objects
            completion(true)
        }
    }
}
