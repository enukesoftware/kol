//
//  ScanUserViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 14/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import AVKit

protocol ScanUserViewControllerDelegate {
    func scannedUser(with controller: ScanUserViewController, details: String)
}

class ScanUserViewController: ScannerViewController {

    @IBOutlet weak var scannerView: UIView!
    
    var delegate: ScanUserViewControllerDelegate?
    var previousCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scannerDelegate = self
        loadQRScannerView(containerView: scannerView)
    }
}

//MARK: ScannerViewControllerDelegate methods
extension ScanUserViewController: ScannerViewControllerDelegate {
    
    func foundQRValue(with controller: ScannerViewController, code: String) {
        if previousCode != code {
            previousCode = code
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            self.navigationController?.popViewController(animated: true)
            delegate?.scannedUser(with: self, details: code)
        }
    }
}
