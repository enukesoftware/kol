//
//  ShoppingCartItemsHeaderView.swift
//  KOL
//
//  Created by Rupak on 18/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class ShoppingCartItemsHeaderView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var totalAmountView: UIView!
    @IBOutlet weak var amountTitleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
}
