//
//  InvitationAlertViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 15/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class InvitationAlertViewController: UIViewController {

    @IBOutlet weak var senderProfileImageView: UIImageView!
    @IBOutlet weak var senderNameLabel: UILabel!
    
    var takenAction: ((Bool, AcceptOrderResponseDetails?) -> ())?
    var notificationDetails: PushNotificationDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateWithNotification()
    }
}

//MARK: Action methods
extension InvitationAlertViewController {
    
    @IBAction func clickedOnClose(_ sender: UIButton) {
        if let id = notificationDetails?.inviteId, let userId = notificationDetails?.toUser {
            let params: [String:Any] = ["invited_user" : userId,
                                        "status" : 0]
            acceptInvitation(id: id, params: params) { (object) in
                if object?.id != nil {
                    self.dismiss(animated: true) {
                        self.takenAction?(false, nil)
                    }
                }
            }
        }
    }
    
    @IBAction func clickedOnDone(_ sender: UIButton) {
        if let id = notificationDetails?.inviteId, let userId = notificationDetails?.toUser {
            let params: [String:Any] = ["invited_user" : userId,
                                        "status" : 1]
            acceptInvitation(id: id, params: params) { (object) in
                if object?.id != nil {
                    self.dismiss(animated: true) {
                        self.takenAction?(true, object)
                    }
                }
            }
        }
    }
}

//MARK: Request methods
extension InvitationAlertViewController {
    
    func acceptInvitation(id: String, params: [String:Any], _ completion: @escaping(_ object: AcceptOrderResponseDetails?) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.acceptInvitation(id: id, params: params, completion: { (object) in
            AppUtility.hideProgress(nil)
            completion(object)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
}

//MARK: Other methods
extension InvitationAlertViewController {
    
    func updateWithNotification() {
        if let notification = notificationDetails {
            senderProfileImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            if let urlString = notification.fromUserProfilePicture, let url = URL(string: urlString) {
                senderProfileImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
            } else {
                senderProfileImageView.image = UIImage.defaultProfile()
            }
            senderProfileImageView.makeCircular = true
            
            if let name = notification.fromUserName {
                let attrString = NSMutableAttributedString(string: "\(name) Sent you a Invitation!".localized, attributes: [.font:UIFont.systemFont(ofSize: 18.0),.foregroundColor:UIColor.hexStringToUIColor("78849E")])
                attrString.addAttributes([.foregroundColor:UIColor.hexStringToUIColor("454F63"),.font:UIFont.systemFont(ofSize: 18, weight: .medium)], range: NSRange(location:0,length:name.count))
                senderNameLabel.attributedText = attrString
            }
        }
    }
}
