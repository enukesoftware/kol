//
//  ReportOrderViewController.swift
//  KOL
//
//  Created by Rupak on 20/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class ReportOrderViewController: UIViewController {

    @IBOutlet weak var topicTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var invoiceDetails: InvoiceDetails?
    var orderDetails: OrderDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionTextView.text = "Details".localized
        descriptionTextView.textColor = UIColor.hexStringToUIColor("B4BBC9").withAlphaComponent(0.8)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if LanguageManger.shared.currentLanguage == .en {
            topicTextField.textAlignment = .left
            descriptionTextView.textAlignment = .left
        } else {
            topicTextField.textAlignment = .right
            descriptionTextView.textAlignment = .right
        }
    }
}

//MARK: UITextViewDelegate methods
extension ReportOrderViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Details".localized {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Details".localized
            textView.textColor = UIColor.hexStringToUIColor("B4BBC9").withAlphaComponent(0.8)
        }
    }
}

//MARK: Action methods
extension ReportOrderViewController {
    
    @IBAction func clickedOnSubmit(_ sender: Any) {
        if !checkValidation() {return}
        let orderId = invoiceDetails?.order ?? orderDetails?.id
        if let id = orderId, let description = descriptionTextView.text, let topic = topicTextField.text {
            let params: [String:Any] = ["order" : id,
                                        "topic" : topic,
                                        "description" : description]
            reportInvoice(params: params) { (success) in
                if success {
                    self.navigationController?.popViewController(animated: true)
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Your report sent successfully, thanks.".localized)
                }
            }
        }
    }
}

//MARK: Request methods
extension ReportOrderViewController {
    
    func reportInvoice(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.reportInvoice(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension ReportOrderViewController {
    
    func checkValidation() -> Bool {
        
        if topicTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter report topic.".localized)
            return false
        } else if descriptionTextView.text == "Details".localized {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter report description.".localized)
            return false
        }
        return true
    }
}
