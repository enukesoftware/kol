//
//  OrderSuccessViewController.swift
//  KOL
//
//  Created by Rupak on 6/8/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class OrderSuccessViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        statusLabel.text = statusLabel.text?.replacingOccurrences(of: "\\n", with: "\n").localized
        if DataManager.shared.isFromPickup {
            statusLabel.text = "Kindly proceed with the payment & Wait for restaurant/cafe approval".localized
        }
    }
}

//MARK: Action methods
extension OrderSuccessViewController {
    
    @IBAction func clickedOnOutside(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
