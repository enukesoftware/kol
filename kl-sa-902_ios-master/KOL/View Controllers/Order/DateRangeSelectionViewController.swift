//
//  DateRangeSelectionViewController.swift
//  KOL
//
//  Created by Rupak on 26/04/2020.
//  Copyright © 2020 Rupak. All rights reserved.
//

import UIKit
import FSCalendar

class DateRangeSelectionViewController: UIViewController {

    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var doneButton: UIButton!
    
    private var firstDate: Date?
    private var lastDate: Date?
    private var datesRange: [Date]?
    
    var selectionCompletion: ((_ startDate: Date, _ endDate: Date) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        calendarView.allowsMultipleSelection = true
        doneButton.isEnabled = false
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIView.appearance().semanticContentAttribute = LanguageManger.shared.currentLanguage == .en ? .forceLeftToRight : .forceRightToLeft
    }
    
    @IBAction func clickedOnDone(_ sender: Any) {
        self.dismiss(animated: true) {
            if let start = self.firstDate, let end = self.lastDate {
                self.selectionCompletion?(start, end)
            }
        }
    }
    
    @IBAction func clickedOnOutside(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension DateRangeSelectionViewController: FSCalendarDelegate {
    
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        if firstDate == nil {
            firstDate = date
            datesRange = [firstDate!]

            print("datesRange contains: \(datesRange!)")

            return
        }

        // only first date is selected:
        if firstDate != nil && lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= firstDate! {
                calendar.deselect(firstDate!)
                firstDate = date
                datesRange = [firstDate!]

                print("datesRange contains: \(datesRange!)")

                return
            }

            let range = datesRange(from: firstDate!, to: date)

            lastDate = range.last

            for d in range {
                calendar.select(d)
            }

            datesRange = range

            print("datesRange contains: \(datesRange!)")
            doneButton.isEnabled = true

            return
        }

        // both are selected:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }

            lastDate = nil
            firstDate = nil

            datesRange = []

            print("datesRange contains: \(datesRange!)")
            doneButton.isEnabled = false
        }
    }

    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:

        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }

            lastDate = nil
            firstDate = nil

            datesRange = []
            print("datesRange contains: \(datesRange!)")
            
            doneButton.isEnabled = false
        }
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }

        var tempDate = from
        var array = [tempDate]

        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }

        return array
    }
}
