//
//  CheckoutViewController.swift
//  KOL
//
//  Created by Rupak on 6/8/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class CheckoutViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ordersTableView: UITableView!
    
//    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
//    @IBOutlet weak var titleLeftConstraint: NSLayoutConstraint!
    
    let headerViewMaxHeight: CGFloat = 108
    let headerViewMinHeight: CGFloat = 56
    var maxLeftPadding: CGFloat = 1.0
    var orderDetails: OrderDetails?
    var invoiceDetails: InvoiceDetails? {
        didSet {
            if let items = invoiceDetails?.invoiceItems {
                invoiceItemList = items
                DispatchQueue.main.async {
                    self.ordersTableView.reloadData()
                }
            }
        }
    }
    var invoiceItemList = [InvoiceItemDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ordersTableView.estimatedRowHeight = 110
        ordersTableView.rowHeight = UITableView.automaticDimension
        if let font = UIFont.init(name: "Poppins-SemiBold", size: 24), let titleLabelWidth = titleLabel.text?.widthOfString(usingFont: font) {
            maxLeftPadding = (self.view.frame.width/2.0)-(titleLabelWidth/2.0)
        }
        if let id = orderDetails?.id {
            TaskManager.getInvoiceDetails(id: id) { (object) in
                self.invoiceDetails = object
            }
        }
    }
}

//MARK: Action methods
extension CheckoutViewController {
    
    @IBAction func clickedOnContinue(_ sender: Any) {
        if let controller = Storyboard.controllerWith(name: "PaymentSelectionViewController") as? PaymentSelectionViewController {
            controller.orderDetails = orderDetails
            controller.invoiceItemList = invoiceItemList
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func clickedOnBackCheckout(_ sender: Any) {
        AlertManager.showAlertWith(title: "Alert".localized, message: "Please complete the order first.".localized, nil)
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate  methods
extension CheckoutViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return invoiceItemList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoiceItemList[section].foodItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptTableViewCell") as? ReceiptTableViewCell else {
            return UITableViewCell.init()
        }
        cell.selectionStyle = .none
        cell.contentsView.cornerRadius = 0
        if indexPath.row == 0 {
            cell.contentsView.round(corners: [.bottomLeft, .bottomRight], radius: 0)
            cell.contentsView.round(corners: [.topLeft, .topRight], radius: 10)
        }
        if let item = invoiceItemList[indexPath.section].foodItems?[indexPath.row] {
            cell.updateWith(item: item, participants: orderDetails?.orderParticipants ?? [])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return invoiceItemList[section].user?.localizedName() ?? "N/A"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let footerView = tableView.dequeueReusableCell(withIdentifier: "CheckoutGroupFooterTableViewCell") as? CheckoutGroupFooterTableViewCell {
            footerView.subtotalLabel.text = String(format: "%.2f \(DataManager.shared.currency.localized)", getPriceWith(items: invoiceItemList[section].foodItems ?? []))
            footerView.subtotalCalorieLabel.text = "\(getCalorieWith(items: invoiceItemList[section].foodItems ?? []))"
            
            footerView.contentsView.round(corners: [.bottomLeft, .bottomRight], radius: 10)
            
            return footerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.backgroundColor = .clear
            headerView.tintColor = .clear
            headerView.textLabel?.textColor = UIColor.hexStringToUIColor("78849E")
            headerView.textLabel?.font = UIFont.init(name: "Poppins-Medium", size: 13)
        }
    }
}

// MARK: - UIScrollViewDelegate
extension CheckoutViewController: UIScrollViewDelegate {
    
    /*func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
        
        if newHeaderViewHeight > headerViewMaxHeight {
            headerViewHeightConstraint.constant = headerViewMaxHeight
            adjustTitleLabel(newHeight: headerViewMaxHeight)
        } else if newHeaderViewHeight < headerViewMinHeight {
            headerViewHeightConstraint.constant = headerViewMinHeight
            adjustTitleLabel(newHeight: headerViewMinHeight)
        } else {
            headerViewHeightConstraint.constant = newHeaderViewHeight
            adjustTitleLabel(newHeight: newHeaderViewHeight)
            scrollView.contentOffset.y = 0
        }
    }*/
}

// MARK: - Custom
extension CheckoutViewController {
    
    /*func adjustTitleLabel(newHeight: CGFloat) {
        let fontSize = 40-((headerViewMaxHeight-newHeight)*0.25)
        titleLabel.font = UIFont.init(name: "Poppins-SemiBold", size: fontSize)
        let paddingUnit = (maxLeftPadding-8)/64.0
        let leftPadding = 24+((headerViewMaxHeight-newHeight)*paddingUnit)
        titleLeftConstraint.constant = leftPadding
    }*/
    
    func getPriceWith(items: [OrderItemDetails]) -> Double {
        var amount = 0.0
        for item in items {
            amount += item.sharedPriceWithTax ?? 0
        }
        return amount
    }
    
    func getCalorieWith(items: [OrderItemDetails]) -> Int {
        var amount = 0
        for item in items {
            amount += item.foodItemCalorie ?? 0
        }
        return amount
    }
}
