//
//  OrderHistoryViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 9/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class OrderHistoryViewController: UIViewController {

    @IBOutlet weak var orderHistoryTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var calendarButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerBackgroundImageView: UIImageView!
    @IBOutlet weak var navigationBackgroundView: UIView!
    @IBOutlet weak var statusBarBackgroundView: UIView!
    
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLeftConstraint: NSLayoutConstraint!
    
    let headerViewMaxHeight: CGFloat = 120
    let headerViewMinHeight: CGFloat = 56
    var maxLeftPadding: CGFloat = 1.0
    var invoiceListDetails: InvoiceListDetails? {
        didSet {
            generateInvoiceGroupList()
        }
    }
    var invoiceGroupList = [InvoiceGroupDetails]() {
        didSet {
            orderHistoryTableView.reloadData()
        }
    }
    var filterFromDate: Date?
    var filterToDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let font = UIFont.init(name: "Poppins-SemiBold", size: 24), let titleLabelWidth = titleLabel.text?.widthOfString(usingFont: font) {
            maxLeftPadding = (self.view.frame.width/2.0)-(titleLabelWidth/2.0)
        }
        
        orderHistoryTableView.backgroundColor = .clear
        orderHistoryTableView.rowHeight = UITableView.automaticDimension
        orderHistoryTableView.estimatedRowHeight = 170
        orderHistoryTableView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 120))
        orderHistoryTableView.tableFooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 40))
        
        getInvoiceListDetails { (success) in}
    }
}

//MARK: Action methods
extension OrderHistoryViewController {
    
    @IBAction func clickedOnCalendar(_ sender: Any) {
        if let controller = Storyboard.controllerWith(name: "DateRangeSelectionViewController") as? DateRangeSelectionViewController {
            controller.modalPresentationStyle = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            
            controller.selectionCompletion = {(start, end) in
                self.filterFromDate = start
                self.filterToDate = end
                self.getInvoiceListDetails { (success) in}
            }
            self.present(controller, animated: true, completion: nil)
        }
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension OrderHistoryViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return invoiceGroupList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoiceGroupList[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryTableViewCell") as? OrderHistoryTableViewCell else {
            return UITableViewCell.init()
        }
        cell.selectionStyle = .none
        cell.delegate = self
        cell.updateWith(item: invoiceGroupList[indexPath.section].items[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return invoiceGroupList[section].date.dateStringWithFormat("dd MMM, yyyy", local: false)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.backgroundColor = .clear
            headerView.tintColor = .clear
            if section == 0 {
                headerView.textLabel?.textColor = UIColor.white.withAlphaComponent(0.6)
            } else {
                headerView.textLabel?.textColor = UIColor.hexStringToUIColor("78849E")
            }
            headerView.textLabel?.font = UIFont.init(name: "Poppins-Medium", size: 13)
        }
    }
}

// MARK: - OrderHistoryTableViewCellDelegate methods
extension OrderHistoryViewController: OrderHistoryTableViewCellDelegate {
    
    func clickedOnView(with cell: OrderHistoryTableViewCell) {
        guard let indexPath = orderHistoryTableView.indexPath(for: cell) else {return}
        self.performSegue(withIdentifier: "YourReceiptViewControllerSegue", sender: indexPath)
    }
}

// MARK: - Segue methods
extension OrderHistoryViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "YourReceiptViewControllerSegue", let controller = segue.destination as? YourReceiptViewController, let indexPath = sender as? IndexPath {
            controller.invoice = invoiceGroupList[indexPath.section].items[indexPath.row]
        }
    }
}

// MARK: - UIScrollViewDelegate
extension OrderHistoryViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
        if y >= 0 {
            headerBackgroundImageView.alpha = 1-(y*0.8333333333)/100.0
            navigationBackgroundView.backgroundColor = UIColor.white.withAlphaComponent(1-headerBackgroundImageView.alpha)
            statusBarBackgroundView.alpha = (1-headerBackgroundImageView.alpha)
            navigationBackgroundView.shadowOpacity = Float((1-headerBackgroundImageView.alpha))
        } else {
            headerBackgroundImageView.alpha = 1
            navigationBackgroundView.backgroundColor = .clear
            statusBarBackgroundView.alpha = 0
            navigationBackgroundView.shadowOpacity = 0
        }
        if newHeaderViewHeight > headerViewMaxHeight {
            headerViewHeightConstraint.constant = headerViewMaxHeight
            adjustTitleLabel(newHeight: headerViewMaxHeight)
            
            titleLabel.textColor = .white
            backButton.imageColor = .white
            calendarButton.imageColor = .white
        } else if newHeaderViewHeight < headerViewMinHeight {
            headerViewHeightConstraint.constant = headerViewMinHeight
            adjustTitleLabel(newHeight: headerViewMinHeight)
            
            titleLabel.textColor = UIColor.hexStringToUIColor("454F63")
            backButton.imageColor = UIColor.hexStringToUIColor("454F63")
            calendarButton.imageColor = UIColor.hexStringToUIColor("454F63")
        } else {
            headerViewHeightConstraint.constant = newHeaderViewHeight
            adjustTitleLabel(newHeight: newHeaderViewHeight)
            
            titleLabel.textColor = .white
            backButton.imageColor = .white
            calendarButton.imageColor = .white
//            scrollView.contentOffset.y = 0
        }
    }
}

//MARK: Request methods
extension OrderHistoryViewController {
    
    func getInvoiceListDetails(_ showLoading: Bool = true, _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        var extraArgument = ""
        if let fromDateString = filterFromDate?.dateStringWithFormat("yyyy-MM-dd", local: true), let toDateString = filterToDate?.dateStringWithFormat("yyyy-MM-dd", local: true) {
            extraArgument = "&created_at_after=\(fromDateString)&created_at_before=\(toDateString)"
        }
        RequestManager.getInvoiceListDetails(argument: extraArgument, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            self.invoiceListDetails = object
            completion(object?.count != nil)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

// MARK: - Custom
extension OrderHistoryViewController {
    
    func adjustTitleLabel(newHeight: CGFloat) {
        let fontSize = 40-((headerViewMaxHeight-newHeight)*0.25)
        titleLabel.font = UIFont.init(name: "Poppins-SemiBold", size: fontSize)
        let paddingUnit = (maxLeftPadding-8)/70.0
        let leftPadding = 24+((headerViewMaxHeight-newHeight)*paddingUnit)
        titleLeftConstraint.constant = leftPadding
    }
    
    func generateInvoiceGroupList() {
        var groupList = [InvoiceGroupDetails]()
        for item in invoiceListDetails?.items ?? [] {
            if let date = item.createdAt?.convertedDate().dateStringWithFormat("dd-MM-yyyy", local: true) {
                if let index = groupList.firstIndex(where: {$0.dateString == date}) {
                    groupList[index].items.append(item)
                } else {
                    var object = InvoiceGroupDetails()
                    object.dateString = date
                    object.items = [item]
                    groupList.append(object)
                }
            }
        }
        self.invoiceGroupList = groupList.sorted(by: {$0.date > $1.date})
    }
}
