//
//  PrivacyPolicyViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentTextViewHeightConstraint: NSLayoutConstraint!
    
    let headerViewMaxHeight: CGFloat = 108
    let headerViewMinHeight: CGFloat = 56
    var maxLeftPadding: CGFloat = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let font = UIFont.init(name: "Poppins-SemiBold", size: 24), let titleLabelWidth = titleLabel.text?.widthOfString(usingFont: font) {
            maxLeftPadding = (self.view.frame.width/2.0)-(titleLabelWidth/2.0)
        }
        
        //Load contents from url
        let urlString = "https://kol.com.sa/privacy_policy.html"
        guard let privacyUrl = URL(string: urlString) else {
            print("Error: \(urlString) doesn't seem to be a valid URL")
            return
        }

        do {
            let htmlString = try String(contentsOf: privacyUrl, encoding: .ascii)
            contentTextView.attributedText = htmlString.htmlToAttributedString
        } catch let error {
            print("Error: \(error)")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        contentTextViewHeightConstraint.constant = contentTextView.contentSize.height + 20
    }
}

// MARK: - UIScrollViewDelegate
extension PrivacyPolicyViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
        
        if newHeaderViewHeight > headerViewMaxHeight {
            headerViewHeightConstraint.constant = headerViewMaxHeight
            adjustTitleLabel(newHeight: headerViewMaxHeight)
        } else if newHeaderViewHeight < headerViewMinHeight {
            headerViewHeightConstraint.constant = headerViewMinHeight
            adjustTitleLabel(newHeight: headerViewMinHeight)
        } else {
            headerViewHeightConstraint.constant = newHeaderViewHeight
            adjustTitleLabel(newHeight: newHeaderViewHeight)
            scrollView.contentOffset.y = 0
        }
    }
}

// MARK: - Custom
extension PrivacyPolicyViewController {
    
    func adjustTitleLabel(newHeight: CGFloat) {
        let fontSize = 40-((headerViewMaxHeight-newHeight)*0.25)
        titleLabel.font = UIFont.init(name: "Poppins-SemiBold", size: fontSize)
        let paddingUnit = (maxLeftPadding-8)/64.0
        let leftPadding = 24+((headerViewMaxHeight-newHeight)*paddingUnit)
        titleLeftConstraint.constant = leftPadding
    }
}
