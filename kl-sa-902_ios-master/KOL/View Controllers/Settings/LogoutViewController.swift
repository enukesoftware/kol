//
//  LogoutViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol LogoutViewControllerDelegate {
    func clickedOnLogout(with controller: LogoutViewController)
}

class LogoutViewController: UIViewController {

    var delegate: LogoutViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

//MARK: Action methods
extension LogoutViewController {
    
    @IBAction func clickedOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedOnLogout(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.clickedOnLogout(with: self)
        }
    }
}
