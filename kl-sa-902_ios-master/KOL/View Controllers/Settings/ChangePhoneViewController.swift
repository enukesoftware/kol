//
//  ChangePhoneViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class ChangePhoneViewController: UIViewController {

    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        loadDummyData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if LanguageManger.shared.currentLanguage == .en {
            currentPasswordTextField.textAlignment = .left
            phoneTextField.textAlignment = .left
        } else {
            currentPasswordTextField.textAlignment = .right
            phoneTextField.textAlignment = .right
        }
    }
}

//MARK: Action methods
extension ChangePhoneViewController {
    
    @IBAction func clickedOnContinue(_ sender: Any) {
        if checkValidation() {
            let params: [String:Any] = ["password" : currentPasswordTextField.text ?? "",
                                        "new_phone_number" : phoneTextField.text ?? ""]
            changePhone(params: params) { (success) in
                if success {
                    if let controller = Storyboard.controllerWith(name: "VerifyCodeViewController") as? VerifyCodeViewController {
                        controller.fromChangePhone = true
                        controller.phoneNumber = self.phoneTextField.text ?? ""
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }
        }
    }
}

//MARK: Request methods
extension ChangePhoneViewController {
    
    func changePhone(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.changePhone(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension ChangePhoneViewController {
    
    func loadDummyData() {
        currentPasswordTextField.text = "12345678"
        phoneTextField.text = "+8801913243746"
    }
    
    func checkValidation() -> Bool {
        
        if currentPasswordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your current password.".localized)
            return false
        } else if phoneTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter new phone number.".localized)
            return false
        }
        return true
    }
}
