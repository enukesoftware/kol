//
//  SettingsViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class SettingsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var currentLanguageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateWithUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateWithUser()
    }
}

//MARK: Action methods
extension SettingsViewController {
    
    @IBAction func clickedOnEditProfile(_ sender: Any) {
        self.performSegue(withIdentifier: "EditProfileViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnOrderHistory(_ sender: Any) {
        self.performSegue(withIdentifier: "OrderHistoryViewControllerSegue", sender: nil)
//        if let controller = Storyboard.controllerWith(name: "RatingViewController") as? RatingViewController {
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
    }
    
    @IBAction func clickedOnContact(_ sender: Any) {
        guard let number = URL(string: "tel://" + "+966565562501") else { return }
        if UIApplication.shared.canOpenURL(number) {
            UIApplication.shared.openURL(number)
        } else {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Sorry, can't able to call. Please check your network status.".localized)
        }
    }
    
    @IBAction func clickedOnLanguage(_ sender: Any) {
        self.performSegue(withIdentifier: "ChangeLanguageViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnPrivacyPolicy(_ sender: Any) {
        self.performSegue(withIdentifier: "PrivacyPolicyViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnRaiseTicket(_ sender: Any) {
        self.performSegue(withIdentifier: "RaiseTicketHomeViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnChangePassword(_ sender: Any) {
        self.performSegue(withIdentifier: "ChangePasswordViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnPhoneNumber(_ sender: Any) {
        self.performSegue(withIdentifier: "ChangePhoneViewControllerSegue", sender: nil)
    }
    
    @IBAction func clickedOnLogout(_ sender: Any) {
        self.performSegue(withIdentifier: "LogoutViewControllerSegue", sender: nil)
//        let alertController = UIAlertController.init(title: "Alert".localized, message: "Are you sure to logout?".localized, preferredStyle: .alert)
//        alertController.addAction(UIAlertAction.init(title: "Yes".localized, style: .default, handler: { (alertAction) in
//            self.navigationController?.popToRootViewController(animated: true)
//        }))
//        alertController.addAction(UIAlertAction.init(title: "No".localized, style: .cancel, handler: nil))
//        self.present(alertController, animated: true, completion: nil)
    }
}

//MARK: Action methods
extension SettingsViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LogoutViewControllerSegue", let controller = segue.destination as? LogoutViewController {
            controller.delegate = self
        } else if segue.identifier == "ChangeLanguageViewControllerSegue", let controller = segue.destination as? ChangeLanguageViewController {
            controller.delegate = self
            controller.userLanguage = (LanguageManger.shared.currentLanguage == .en) ? "en" : "ar"
        }
    }
}

//MARK: LogoutViewControllerDelegate methods
extension SettingsViewController: LogoutViewControllerDelegate {
    
    func clickedOnLogout(with controller: LogoutViewController) {
        if !AppUtility.isSimulator() {
            AppUtility.unsubscribeNotification()
        }
        AuthManager.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
}

//MARK: ChangeLanguageViewControllerDelegate methods
extension SettingsViewController: ChangeLanguageViewControllerDelegate {
    
    func updatedLanguage(with controller: ChangeLanguageViewController, isEnglish: Bool) {
        updateUser(language: isEnglish ? "en" : "ar") { (success) in
            if success {
                LanguageManger.shared.currentLanguage = isEnglish ? .en : .ar
                AppUtility.changeLanguage()
                self.currentLanguageLabel.text = (DataManager.shared.loginDetails?.user?.first?.locale == "en") ? "English".localized : "Arabic".localized
                AppUtility.showAlertWithProperty("Success".localized, messageString: "Language changed successfully.".localized)
            }
        }
    }
}

//MARK: Request methods
extension SettingsViewController {
    
    func updateUser(language: String,_ completion: @escaping(_ success: Bool) -> Void) {
        
        let params: [String:Any] = ["user.locale" : language]
        AppUtility.showProgress(nil, title: nil)
        RequestManager.registerWith(modify: true, params: params, fileDic: [], completion: { (loginDetails, userDetails) in
            AppUtility.hideProgress(nil)
            if let user = userDetails, user.id != nil {
                DataManager.shared.loginDetails?.user = [user]
                AuthManager.saveLoginDetails()
            }
            completion(userDetails != nil)
        }) { (error, message) in
            AppUtility.hideProgress(nil)
            if let msg = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: msg)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension SettingsViewController {
    
    func updateWithUser() {
        if let user = DataManager.shared.loginDetails?.user?.first {
            profileImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            if let urlString = user.profilePicture, let url = URL(string: urlString) {
                profileImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
            } else {
                profileImageView.image = UIImage.defaultProfile()
            }
            nameLabel.text = user.localizedName()
            numberLabel.text = user.phoneNumber
            currentLanguageLabel.text = (LanguageManger.shared.currentLanguage == .en) ? "English".localized : "Arabic".localized
        }
    }
}
