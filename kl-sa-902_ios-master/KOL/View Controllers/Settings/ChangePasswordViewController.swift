//
//  ChangePasswordViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        loadDummyData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if LanguageManger.shared.currentLanguage == .en {
            oldPasswordTextField.textAlignment = .left
            newPasswordTextField.textAlignment = .left
            confirmPasswordTextField.textAlignment = .left
        } else {
            oldPasswordTextField.textAlignment = .right
            newPasswordTextField.textAlignment = .right
            confirmPasswordTextField.textAlignment = .right
        }
    }
}

//MARK: Action methods
extension ChangePasswordViewController {
    
    @IBAction func clickedOnReset(_ sender: Any) {
        if checkValidation() {
            let params: [String:Any] = ["current_password" : oldPasswordTextField.text ?? "",
                                        "new_password" : confirmPasswordTextField.text ?? ""]
            changePassword(params: params) { (success) in
                if success {
                    TaskManager.moveToViewController(with: SettingsViewController.classForCoder())
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Your password changed successfully.".localized)
                }
            }
        }
    }
}

//MARK: Request methods
extension ChangePasswordViewController {
    
    func changePassword(params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.changePassword(params: params, completion: { (success) in
            AppUtility.hideProgress(nil)
            completion(success)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension ChangePasswordViewController {
    
    func loadDummyData() {
        oldPasswordTextField.text = "12345678"
        newPasswordTextField.text = "12345678"
        confirmPasswordTextField.text = "12345678"
    }
    
    func checkValidation() -> Bool {
        
        if oldPasswordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter your phone current password.".localized)
            return false
        } else if newPasswordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter new password.".localized)
            return false
        } else if confirmPasswordTextField.text == "" {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Please enter confirm password.".localized)
            return false
        } else if newPasswordTextField.text != confirmPasswordTextField.text {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "New and confirm password are not matched, please verify.".localized)
            return false
        } else if oldPasswordTextField.text == confirmPasswordTextField.text {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Old and confirm password can't be same.".localized)
            return false
        } else if let password = newPasswordTextField.text, !AppUtility.isStrongPassword(text: password) {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Weak password. For security purposes kindly provide a password with Capital character, small character, numbers & special character".localized)
            return false
        }
        return true
    }
}
