//
//  ChangeLanguageViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol ChangeLanguageViewControllerDelegate {
    func updatedLanguage(with controller: ChangeLanguageViewController, isEnglish: Bool)
}

class ChangeLanguageViewController: UIViewController {

    @IBOutlet weak var arabicCheckImageView: UIImageView!
    @IBOutlet weak var englishCheckImageView: UIImageView!
    
    var delegate: ChangeLanguageViewControllerDelegate?
    var englishIsSelected = true {
        didSet {
            if englishIsSelected {
                arabicCheckImageView.image = UIImage(named: "uncheck")
                englishCheckImageView.image = UIImage(named: "check")
            } else {
                arabicCheckImageView.image = UIImage(named: "check")
                englishCheckImageView.image = UIImage(named: "uncheck")
            }
        }
    }
    var userLanguage = "en"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        englishIsSelected = userLanguage == "en" ? true : false
    }
}

//MARK: Action methods
extension ChangeLanguageViewController {
    
    @IBAction func clickedOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedOnArabic(_ sender: Any) {
        if englishIsSelected {
            englishIsSelected = false
            self.dismiss(animated: true) {
                self.delegate?.updatedLanguage(with: self, isEnglish: false)
            }
        }
    }
    
    @IBAction func clickedOnEnglish(_ sender: Any) {
        if !englishIsSelected {
            englishIsSelected = true
            self.dismiss(animated: true) {
                self.delegate?.updatedLanguage(with: self, isEnglish: true)
            }
        }
    }
}
