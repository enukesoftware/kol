//
//  ShareItemViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 20/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol ShareItemViewControllerDelegate {
    func clickedOnDone(with controller: ShareItemViewController)
}

class ShareItemViewController: UIViewController {

    @IBOutlet weak var groupMemberTableView: UITableView!
    @IBOutlet weak var memberCountLabel: UILabel!
    
    var selectedIdList = [Int]()
    var delegate: ShareItemViewControllerDelegate?
    var orderDetails: OrderDetails?
    var participantList = [ParticipantDetails]() {
        didSet {
            groupMemberTableView.reloadData()
        }
    }
    var foodItemDetails: FoodItemDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        participantList = orderDetails?.orderParticipants?.filter({$0.user?.id ?? 0 != DataManager.shared.loginDetails?.user?.first?.id ?? 0}) ?? []
        participantList = participantList.filter({self.selectedIdList.contains($0.user?.id ?? 0) == false})
        selectedIdList = []
        memberCountLabel.text = "\(participantList.count) "+"Joined".localized
    }
}

//MARK: Action methods
extension ShareItemViewController {
    
    @IBAction func clickedOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedOnDone(_ sender: Any) {
        if self.selectedIdList.count > 0 {
            self.selectedIdList = selectedIdList.filter({$0 != (DataManager.shared.loginDetails?.user?.first?.id ?? 0)})
            if let itemId = foodItemDetails?.id {
                let params: [String:Any] = ["invited_users": selectedIdList,
                                            "order_item" : itemId]
                orderItemInvite(params: params) { (success) in
                    self.dismiss(animated: true) {
                        self.delegate?.clickedOnDone(with: self)
                    }
                }
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension ShareItemViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return participantList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupMemberTableViewCell") as? GroupMemberTableViewCell else {
            return UITableViewCell.init()
        }
        cell.selectionStyle = .none
        cell.isSelected = selectedIdList.contains(participantList[indexPath.row].user?.id ?? -1)
        cell.updateWith(object: participantList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let id = participantList[indexPath.row].user?.id {
            if selectedIdList.contains(id) {
                selectedIdList.remove(object: id)
            } else {
                selectedIdList.append(id)
            }
            groupMemberTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
}

//MARK: Request methods
extension ShareItemViewController {
    
    func orderItemInvite(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading{AppUtility.showProgress(nil, title: nil)}
        RequestManager.orderItemInvite(params: params, completion: { (success) in
            if showLoading{AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading{AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension ShareItemViewController {
    
}
