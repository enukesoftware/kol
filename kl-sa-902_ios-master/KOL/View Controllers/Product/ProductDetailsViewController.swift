//
//  ProductDetailsViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 20/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

protocol ProductDetailsViewControllerDelegate {
    func shouldRefreshOrder(controller: ProductDetailsViewController)
}

class ProductDetailsViewController: AbstructViewController {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productTypeLabel: UILabel!
    @IBOutlet weak var productCaloryLabel: UILabel!
    @IBOutlet weak var addOnTableView: UITableView!
    @IBOutlet weak var attributesTableView: UITableView!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var orderListView: ShoppingCartListView!
    @IBOutlet weak var cartControl: UIControl!
    @IBOutlet weak var cartItemCountLabel: UILabel!
    @IBOutlet weak var noAddonView: UIView!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var shareItemButton: UIButton!
    
    @IBOutlet weak var listHeaderTopHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderListViewHeightConstraint: NSLayoutConstraint!
    
    var productDelegate: ProductDetailsViewControllerDelegate?
    var selectedIdList = [Int]()
    var productQuantity = 1 {
        didSet {
            productQuantityLabel.text = "\(productQuantity)"
        }
    }
    var addOnList = [FoodAddonDetails]() {
        didSet {
            noAddonView.isHidden = (addOnList.count > 0)
        }
    }
    var listViewOpen = false {
        didSet {
            if listViewOpen {
                orderListViewHeightConstraint.constant = self.view.frame.height+10
                orderListView.cornerRadius = 0
                orderListView.listTopIndicatorView.isHidden = true
            } else {
                orderListViewHeightConstraint.constant = self.view.frame.height/1.5
                orderListView.cornerRadius = 10
                orderListView.listTopIndicatorView.isHidden = false
            }
            listHeaderTopHeightConstraint.constant = listViewOpen ? 48 : 8
            orderListView.listBackButton.isHidden = !orderListView.listTopIndicatorView.isHidden
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    var foodItemDetails: FoodItemDetails?
    var selectedAttributes: [Int : FoodAttributeMatrixDetails] = [:]
    var orderDetails: OrderDetails?
    var orderItemDetails: OrderItemDetails?
    var sharedWithUserIDList = [Int]()
    var orderItemList = [OrderItemDetails]() {
        didSet {
            if orderItemList.count > 0 {
                cartControl.isHidden = false
                cartItemCountLabel.text = "\(orderItemList.count)"
            } else {
                DataManager.shared.orderConfirmed = false
                self.listViewOpen = false
                self.orderListView.isHidden = true
                cartItemCountLabel.text = "0"
            }
            DataManager.shared.cartCount = orderItemList.count
            orderListView.orderItemCountLabel.text = String(format: "%02d " + "items".localized, orderItemList.count)
            orderListView.participantList = orderDetails?.orderParticipants ?? []
            orderListView.orderItemList = orderItemList
            //Update with checkout
            var isConfirmed = true
            for item in orderItemList {
                if item.status != 1 {
                    isConfirmed = false
                    break
                }
            }
            DataManager.shared.orderConfirmed = isConfirmed
            orderListView.confirmButton.setTitle(DataManager.shared.orderConfirmed ? "CHECKOUT".localized : "CONFIRM ORDER".localized, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addOnTableView.backgroundColor = .clear
        orderListView.orderListTableView.estimatedRowHeight = 110
        orderListView.orderListTableView.rowHeight = UITableView.automaticDimension
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        orderListView.addGestureRecognizer(gesture)
        gesture.delegate = self
        listViewOpen = false
        orderListView.confirmButton.setTitle(DataManager.shared.orderConfirmed ? "CHECKOUT".localized : "CONFIRM ORDER".localized, for: .normal)
        orderListView.delegate = self
        
        updateProductWithItemDetails()
        refreshOrderItems()
        addToCartButton.setTitle(orderItemDetails?.id != nil ? "UPDATE".localized : "ADD TO CART".localized, for: .normal)
        shareItemButton.imageEdgeInsets = LanguageManger.shared.currentLanguage == .en ? UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8) : UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if DataManager.shared.cartCount != 0 {
//            cartControl.isHidden = false
            cartItemCountLabel.text = "\(DataManager.shared.cartCount)"
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Dynamic sizing for the footer view
        DispatchQueue.main.async {
            if let footerView = self.addOnTableView.tableFooterView {
                var footerFrame = footerView.frame
                footerFrame.size.height = (self.attributesTableView.contentSize.height + 20)
                footerView.frame = footerFrame
                
                self.addOnTableView.tableFooterView = footerView
            }
        }
    }
}

//MARK: Action methods
extension ProductDetailsViewController {
    
    @IBAction func clickedOnShareItem(_ sender: Any) {
        if orderDetails?.orderParticipants?.count ?? 0 > 1 {
            self.performSegue(withIdentifier: "ShareItemViewControllerSegue", sender: nil)
        } else {
            AlertManager.showAlertWith(title: "Alert".localized, message: "Please add participants to share item, thanks.".localized, nil)
        }
    }
    
    @IBAction func clickedOnPlus(_ sender: Any) {
        productQuantity += 1
    }
    
    @IBAction func clickedOnMinus(_ sender: Any) {
        if productQuantity > 1 {
            productQuantity -= 1
        }
    }
    
    @IBAction func clickedOnAddToCart(_ sender: Any) {
        if let id = orderItemDetails?.id {
            updateCartItem(id: id, params: getAddCartRequestDetails()) { (success) in
                if success {
                    self.productDelegate?.shouldRefreshOrder(controller: self)
                    self.navigationController?.popViewController(animated: true)
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Product updated successfully.".localized)
                }
            }
        } else {
            addProductToCart(params: getAddCartRequestDetails()) { (success) in
                if success {
                    DataManager.shared.cartCount = DataManager.shared.cartCount+1
                    self.cartCountLabel.text = "\(DataManager.shared.cartCount)"
                    self.productDelegate?.shouldRefreshOrder(controller: self)
                    self.navigationController?.popViewController(animated: true)
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Product added successfully.".localized)
                }
            }
        }
    }
    
    @IBAction func clickedOnCart(_ sender: Any) {
        if orderItemList.count == 0 {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "Cart is empty, please add product first.".localized)
            return
        } else {
            orderListView.isHidden = false
            orderListView.orderListTableView.reloadData()
        }
    }
    
    @IBAction func clickedOnListClose(_ sender: Any) {
        self.listViewOpen = false
        self.orderListView.isHidden = true
    }
    
    @IBAction func clickedOnConfirmOrder(_ sender: Any) {
        if DataManager.shared.orderConfirmed {
            if let controller = Storyboard.controllerWith(name: "CheckoutViewController") as? CheckoutViewController {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        } else {
            if let controller = Storyboard.controllerWith(name: "OrderSuccessViewController") as? OrderSuccessViewController {
                controller.modalTransitionStyle = .crossDissolve
                controller.modalPresentationStyle = .overFullScreen
                self.present(controller, animated: true, completion: nil)
            }
            DataManager.shared.orderConfirmed = true
            orderListView.confirmButton.setTitle(DataManager.shared.orderConfirmed ? "CHECKOUT".localized : "CONFIRM ORDER".localized, for: .normal)
        }
    }
}

//MARK: Segue methods
extension ProductDetailsViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShareItemViewControllerSegue", let controller = segue.destination as? ShareItemViewController {
            controller.delegate = self
            controller.orderDetails = self.orderDetails
            controller.foodItemDetails = self.foodItemDetails
            controller.selectedIdList = self.sharedWithUserIDList
        }
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension ProductDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == attributesTableView {
            return foodItemDetails?.attributes?.count ?? 0
        }
        return addOnList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == attributesTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttributeTableViewCell") as? AttributeTableViewCell else {
                return UITableViewCell.init()
            }
            cell.selectionStyle = .none
            cell.delegate = self
            let attribute = foodItemDetails?.attributes?[indexPath.row]
            cell.titleLabel.text = attribute?.localizedName()
            if let id = attribute?.id, selectedAttributes.keys.contains(id) {
                cell.titleLabel.text = selectedAttributes[id]?.localizedName()
            }
            
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddOnTableViewCell") as? AddOnTableViewCell else {
            return UITableViewCell.init()
        }
        cell.selectionStyle = .none
        cell.isSelected = selectedIdList.contains(addOnList[indexPath.row].id ?? -1)
        cell.titleLabel.text = addOnList[indexPath.row].localizedName()
        cell.priceLabel.text = "\(addOnList[indexPath.row].price ?? "N/A") " + "SAR".localized
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == addOnTableView {
            if let id = addOnList[indexPath.row].id {
                if selectedIdList.contains(id) {
                    selectedIdList.remove(object: id)
                } else {
                    selectedIdList.append(id)
                }
                addOnTableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == attributesTableView {
            return 45
        }
        return 35
    }
}

//MARK: AttributeTableViewCellDelegate methods
extension ProductDetailsViewController: AttributeTableViewCellDelegate {
    
    func clickedOnAttribute(with cell: AttributeTableViewCell) {
        guard let indexPath = attributesTableView.indexPath(for: cell) else {return}
        let items = getFoodAttributeMatrix(index: indexPath.row)
        if items.count > 0, let attribute = foodItemDetails?.attributes?[indexPath.row] {
            showSelectionList(title: attribute.localizedName() ?? "N/A", objectList: items) { (object, index) in
                cell.titleLabel.text = attribute.attributeMatrix?[index].localizedName()
                if let id = attribute.id, let matrix = attribute.attributeMatrix?[index] {
                    self.selectedAttributes[id] = matrix
                }
            }
        } else {
            AppUtility.showAlertWithProperty("Alert".localized, messageString: "No attributes available for this category.".localized)
        }
    }
}

//MARK: ShareItemViewControllerDelegate methods
extension ProductDetailsViewController: ShareItemViewControllerDelegate {
    
    func clickedOnDone(with controller: ShareItemViewController) {
        self.sharedWithUserIDList = Array(Set(self.sharedWithUserIDList).union(controller.selectedIdList))
        
//        self.performSegue(withIdentifier: "AcceptSharedViewControllerSegue", sender: nil)
    }
}

// MARK: - ShoppingCartListViewDelegate methods
extension ProductDetailsViewController: ShoppingCartListViewDelegate {
    
    func clickedOnEdit(with view: ShoppingCartListView, indexPath: IndexPath) {
        orderListView.isHidden = true
        self.orderItemDetails = orderItemList[indexPath.row]
        self.updateProductWithItemDetails()
        addToCartButton.setTitle(orderItemDetails?.id != nil ? "UPDATE".localized : "ADD TO CART".localized, for: .normal)
    }
    
    func clickedOnCancel(with view: ShoppingCartListView, indexPath: IndexPath) {
        if let id = orderItemList[indexPath.row].id {
            TaskManager.cancelOrderItem(id: id) { (success) in
                if success, let orderId = self.orderDetails?.id {
                    TaskManager.getOrderItems(true, id: orderId) { (objects) in
                        self.orderItemList = objects
                    }
                    if id == self.orderItemDetails?.id {
                        self.orderItemDetails = nil
                        self.addToCartButton.setTitle(self.orderItemDetails?.id != nil ? "UPDATE".localized : "ADD TO CART".localized, for: .normal)
                    }
                }
            }
        }
    }
}

//MARK: UIGestureRecognizerDelegate methods
extension ProductDetailsViewController: UIGestureRecognizerDelegate {
    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizer.State.ended || gestureRecognizer.state == UIGestureRecognizer.State.changed {
            let translation = gestureRecognizer.translation(in: self.view)
            
            if orderListViewHeightConstraint.constant-translation.y > self.view.frame.height/1.5, !listViewOpen {
                orderListViewHeightConstraint.constant = orderListViewHeightConstraint.constant+translation.y
            }
            if gestureRecognizer.state == .ended {
                listViewOpen = (orderListViewHeightConstraint.constant > self.view.center.y)
            } else {
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
        }
    }
}

//MARK: Request methods
extension ProductDetailsViewController {
    
    func addProductToCart(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.addProductToCart(params: params, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func updateCartItem(_ showLoading: Bool = true, id: Int, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading {AppUtility.showProgress(nil, title: nil)}
        RequestManager.updateCartItem(id: id, params: params, completion: { (object) in
            if showLoading {AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading {AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension ProductDetailsViewController {
    
    func updateWithFoodItemDetails() {
        if let item = foodItemDetails {
            if let urlString = item.picture, let url = URL(string: urlString) {
                coverImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                coverImageView.sd_setImage(with: url, placeholderImage: UIImage.noImage(), options: .highPriority, completed: nil)
            } else {
                coverImageView.image = UIImage.noImage()
            }
            self.addOnList = item.addons ?? []
            addOnTableView.reloadData()
            productNameLabel.text = item.localizedName() ?? "N/A"
            productPriceLabel.text = String(format: "%@ "+"SAR".localized, item.price ?? "N/A")
            productTypeLabel.text = item.localizedCategory() ?? "N/A"
            productCaloryLabel.text = "\(item.calorie ?? 0) "+"Cal".localized
            //load existing attribute selection
            for attributeMatrix in self.orderItemDetails?.orderItemAttributeMatrices ?? [] {
                if let attributeDetails = self.foodItemDetails?.attributes?.filter({$0.id == attributeMatrix.foodAttributeMatrixDetails?.attribute}).first {
                    if let id = attributeDetails.id, let matrix = attributeDetails.attributeMatrix?.filter({$0.id == attributeMatrix.foodAttributeMatrix}).first {
                        selectedAttributes[id] = matrix
                    }
                }
            }
            attributesTableView.reloadData()
            self.viewDidLayoutSubviews()
        }
    }
    
    func getFoodAttributeMatrix(index: Int) -> [IDName] {
        var list = [IDName]()
        if index < foodItemDetails?.attributes?.count ?? 0 {
            for object in foodItemDetails?.attributes?[index].attributeMatrix ?? [] {
                if let id = object.id, let name = object.localizedName() {
                    list.append(IDName(id: id, name: name))
                }
            }
        }
        
        return list
    }
    
    func getAddCartRequestDetails() -> [String:Any] {
        let object = AddProductRequestDetails()
        object.order = orderDetails?.id
        object.foodItem = foodItemDetails?.id
        object.quantity = productQuantity
        //Participants
        var participantList = [Int]()
        let currentUserId = DataManager.shared.loginDetails?.user?.first?.id ?? -1
        for participant in orderDetails?.orderParticipants ?? [] {
            if let id = participant.user?.id, currentUserId != id, sharedWithUserIDList.contains(id) {
                participantList.append(id)
            }
        }
        object.invitedUsers = participantList
        //Add on list
        var requestAddOnList = [OrderItemRequestAddOn]()
        for addOn in addOnList {
            if selectedIdList.contains(addOn.id ?? -1) {
                let requestAddOn = OrderItemRequestAddOn()
                requestAddOn.foodAddOn = addOn.id
                requestAddOn.foodAddOnDetails = FoodAddOnRequestDetails.init(fromDictionary: addOn.toJSON())
                requestAddOnList.append(requestAddOn)
            }
        }
        if requestAddOnList.count > 0 {
            object.orderItemAddOns = requestAddOnList
        }
        //Attribute matrices list
        var attributes = [OrderItemRequestAttributeMatrice]()
        for attribute in selectedAttributes.values {
            let matrice = OrderItemRequestAttributeMatrice()
            matrice.foodAttributeMatrix = attribute.id
            matrice.foodAttributeMatrixDetails = FoodAttributeMatrixRequestDetails.init(fromDictionary: attribute.toJSON())
            attributes.append(matrice)
        }
        if attributes.count > 0 {
            object.orderItemAttributeMatrices = attributes
        }
        
        return object.toJSON()
    }
    
    func refreshOrderItems() {
        if let orderId = orderDetails?.id {
            TaskManager.getOrderItems(false, id: orderId) { (objects) in
                self.orderItemList = objects
            }
        }
    }
    
    func updateProductWithItemDetails() {
        self.productQuantity = orderItemDetails?.quantity ?? 1
        self.sharedWithUserIDList = orderItemDetails?.sharedWith ?? []
        var addOnList = [Int]()
        for addOn in orderItemDetails?.orderItemAddOns ?? [] {
            if let id = addOn.foodAddOn {
                addOnList.append(id)
            }
        }
        self.selectedIdList = addOnList
        self.addOnTableView.reloadData()
        //Update food item details
        updateWithFoodItemDetails()
        if let id = orderItemDetails?.foodItem {
            TaskManager.getRestaurantFoodItemDetails(id: id) { (object) in
                self.foodItemDetails = object
                self.updateWithFoodItemDetails()
            }
        }
        print("")
    }
}
