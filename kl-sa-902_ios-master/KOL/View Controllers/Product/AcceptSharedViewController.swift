//
//  AcceptSharedViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 20/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class AcceptSharedViewController: UIViewController {

    @IBOutlet weak var fromNameLabel: UILabel!
    @IBOutlet weak var fromNumberLabel: UILabel!
    @IBOutlet weak var fromProfileImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var itemCalorieLabel: UILabel!
    @IBOutlet weak var itemQuantityLabel: UILabel!
    
    var notificationDetails: PushNotificationDetails?
    var takenAction: ((Bool) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateWithNotification()
    }
}

//MARK: Action methods
extension AcceptSharedViewController {
    
    @IBAction func clickedOnClose(_ sender: Any) {
        if let invitedId = notificationDetails?.inviteId, let userId = notificationDetails?.toUser, let orderItem = notificationDetails?.itemId {
            let params: [String:Any] = ["invited_user" : userId,
                                        "order_item" : orderItem,
                                        "status" : 0]
            acceptOrderItem(id: invitedId, params: params) { (object) in
                if object?.id != nil {
                    self.dismiss(animated: true) {
                        self.takenAction?(false)
                    }
                }
            }
        }
    }
    
    @IBAction func clickedOnAccept(_ sender: Any) {
        if let invitedId = notificationDetails?.inviteId, let userId = notificationDetails?.toUser, let orderItem = notificationDetails?.itemId {
            let params: [String:Any] = ["invited_user" : userId,
                                        "order_item" : orderItem,
                                        "status" : 1]
            acceptOrderItem(id: invitedId, params: params) { (object) in
                if object?.id != nil {
                    self.dismiss(animated: true) {
                        self.takenAction?(true)
                    }
                }
            }
        }
    }
}

//MARK: Request methods
extension AcceptSharedViewController {
    
    func acceptOrderItem(id: String, params: [String:Any], _ completion: @escaping(_ object: AcceptOrderResponseDetails?) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.acceptOrderItem(id: id, params: params, completion: { (object) in
            AppUtility.hideProgress(nil)
            completion(object)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
}

//MARK: Other methods
extension AcceptSharedViewController {
    
    func updateWithNotification() {
        if let object = notificationDetails {
            if let urlString = object.fromUserProfilePicture, let url = URL(string: urlString) {
                fromProfileImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                fromProfileImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
            } else {
                fromProfileImageView.image = UIImage.defaultProfile()
            }
            fromProfileImageView.makeCircular = true
            fromNameLabel.text = object.fromUserName
            fromNumberLabel.text = object.fromUserPhoneNumber
            //item details
            if let urlString = object.itemPicture, let url = URL(string: urlString) {
                itemImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                itemImageView.sd_setImage(with: url, placeholderImage: UIImage.noImage(), options: .highPriority, completed: nil)
            } else {
                itemImageView.image = UIImage.noImage()
            }
            itemImageView.makeCircular = true
            itemNameLabel.text = object.itemName
            if let price = Double(object.itemPrice ?? "0") {
                itemPriceLabel.text = String(format: "%d "+"SAR".localized, Int(price))
            }
            itemCalorieLabel.text = "\(object.itemCalorie ?? "N/A") " + "Cal".localized
            itemQuantityLabel.text = "\(object.itemQuantity ?? "N/A") " + "pcs".localized
        }
    }
}
