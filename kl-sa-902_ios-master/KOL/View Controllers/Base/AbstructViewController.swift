//
//  AbstructViewController.swift
//  KOL
//
//  Created by Rupak Biswas on 2/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SideMenu

protocol AbstractViewControllerDelegate {
    func didChageDateWith(date: Date?, code: Int)
}

class BottomDelegate: NSObject, UITableViewDelegate, UITableViewDataSource{
    
    var idNames: [IDName] = []
    var itemSelected: ((_ result: Int)->())?
    
    override init() {
        super.init()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return idNames.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = idNames[indexPath.row].finalName?.localized
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itemSelected?(indexPath.row)
    }
}

class AbstructViewController: UIViewController {
    
    var datePicker: UIDatePicker?
    
    var selectedDateCode = 0
    var delegate: AbstractViewControllerDelegate?
    private var bottomDelegate = BottomDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_background"), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = .lightGray
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
}

//MARK: Date picker related methods
extension AbstructViewController {
    
    func createAndShowPickerView(_ locationView: UIView, selectedDate: Date?, minimumDate: Date? = nil, maximumDate: Date? = nil, pickerMode: UIDatePicker.Mode = .dateAndTime, code: Int = 0) {
        selectedDateCode = code
        let baseView = UIView.init()
        let pickerFrame = CGRect(x: 0, y: 0, width: self.view.frame.size.width/2.0, height: 135)
        let pView = UIDatePicker.init(frame: pickerFrame)
        pView.datePickerMode = pickerMode
        pView.locale = Locale(identifier: "en_GB")
        pView.minimumDate = minimumDate
        pView.maximumDate = maximumDate
        
        if selectedDate != nil {
            pView.setDate(selectedDate!, animated: false)
        }
        datePicker = pView
        
        self.delegate?.didChageDateWith(date: self.datePicker?.date, code: self.selectedDateCode)
        if AppUtility.isiPad() {
            datePicker!.addTarget(self, action: #selector(chnagedPickerViewDate), for: UIControl.Event.valueChanged)
            baseView.addSubview(self.datePicker!)
            
            let popoverContent = UIViewController.init()
            popoverContent.view = baseView
            popoverContent.modalPresentationStyle = UIModalPresentationStyle.popover
            popoverContent.popoverPresentationController?.sourceView = self.view
            popoverContent.popoverPresentationController?.permittedArrowDirections = .up
            popoverContent.popoverPresentationController?.sourceRect = CGRect(x: locationView.frame.origin.x, y: locationView.frame.size.height+28+locationView.frame.origin.y, width: locationView.frame.size.width, height: locationView.frame.size.height)
            popoverContent.preferredContentSize = CGSize(width: self.view.frame.size.width/2.0, height: 135)
            
            self.present(popoverContent, animated: true, completion: nil)
        } else {
            let actionSheet = UIAlertController(title: "\n\n\n\n\n", message: nil, preferredStyle: .actionSheet)
            pView.frame = CGRect(x: 0, y: 8.0, width: actionSheet.view.bounds.size.width - 20, height: 100.0)
            actionSheet.view.addSubview(pView)
            actionSheet.addAction(UIAlertAction.init(title: "Done".localized, style: .default, handler: { (alertAction) in
                self.delegate?.didChageDateWith(date: self.datePicker?.date, code: self.selectedDateCode)
            }))
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    @objc func chnagedPickerViewDate(completion: @escaping(_ date: Date?) -> Void) {
        DispatchQueue.main.async {
            self.delegate?.didChageDateWith(date: self.datePicker?.date, code: self.selectedDateCode)
        }
    }
}

//MARK: Custom methods
extension AbstructViewController {
    
    //Bottom sheet
    func showSelectionList(title: String, objectList: [IDName], completion: @escaping(_ item: IDName,_ index: Int) -> Void) {
        let controller = Bottomsheet.Controller()
        bottomDelegate.idNames = objectList
        bottomDelegate.itemSelected = { result in
            controller.dismiss()
            controller.dismiss(animated: true, completion: {
                completion(objectList[result], result)
            })
        }
        
        controller.addNavigationbar { navigationBar in
            let item = UINavigationItem(title: title)
            let rightButton = UIBarButtonItem(title: "Dismiss".localized, style: .plain, target: controller, action: #selector(BottomsheetController.dismiss(_:)))
            item.rightBarButtonItem = rightButton
            navigationBar.items = [item]
        }
        
        controller.addTableView { [weak self] tableView in
            tableView.delegate = self?.bottomDelegate
            tableView.dataSource = self?.bottomDelegate
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 100
            tableView.contentInset.top = 44
            tableView.scrollIndicatorInsets.top = 44
        }
        
        // customize
        controller.containerView.backgroundColor = .black
        controller.overlayBackgroundColor = UIColor.black.withAlphaComponent(0.6)
        controller.viewActionType = .tappedDismiss // .swipe
        controller.initializeHeight =  UIScreen.main.bounds.height / 2
        
        DispatchQueue.main.async {
            //            (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.present(controller, animated: true, completion: nil)
            UIApplication.topViewController()?.present(controller, animated: true, completion: nil)
        }
    }
}

