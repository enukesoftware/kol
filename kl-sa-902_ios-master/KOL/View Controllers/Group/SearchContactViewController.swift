//
//  SearchContactViewController.swift
//  KOL
//
//  Created by Rupak on 12/01/2020.
//  Copyright © 2020 Rupak. All rights reserved.
//

import UIKit

class SearchContactViewController: UIViewController {

    @IBOutlet weak var contactListTableView: UITableView!
    
    var contactList = [ContactDetails]()
    var filteredContactList = [ContactDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filteredContactList = contactList
        contactListTableView.reloadData()
    }
    
    @IBAction func clickedOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension SearchContactViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredContactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTableViewCell") as? ContactsTableViewCell else {
            return UITableViewCell.init()
        }
        cell.delegate = self
        cell.selectionStyle = .none
        cell.updateWith(contact: filteredContactList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

//MARK: ContactsTableViewCellDelegate methods
extension SearchContactViewController: ContactsTableViewCellDelegate {
    
    func clickedOnAdd(cell: ContactsTableViewCell) {
        self.view.endEditing(true)
        guard let indexPath = contactListTableView.indexPath(for: cell) else {return}
        if indexPath.row < filteredContactList.count {
            if filteredContactList[indexPath.row].kolUserDetails == nil {
                let activityViewController = UIActivityViewController(activityItems: ["Download KOL app & Join the Order!".localized, "\nhttps://apps.apple.com/sa/app/kol-app/id1495841798 \nhttps://play.google.com/store/apps/details?id=com.dtmweb.kol"] , applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            } else if let id = filteredContactList[indexPath.row].kolUserDetails?.id {
                if let orderId = DataManager.shared.runningOrderDetails?.id {
                    let params: [String:Any] = ["invited_users": [id],
                                                "order" : orderId]
                    TaskManager.inviteOrder(params: params) { (success) in
                        if success {
                            AppUtility.showAlertWithProperty("Success".localized, messageString: "Invitation sent successfully.".localized)
                        }
                    }
                } else {
                    AppUtility.showAlertWithProperty("Alert!".localized, messageString: "Scan the table QR code first or choose for pickup".localized)
                }
            }
        }
    }
}

//MARK: UITextFieldDelegate methods
extension SearchContactViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let textFieldString = textField.text, let swtRange = Range(range, in: textFieldString) {
            let fullString = textFieldString.replacingCharacters(in: swtRange, with: string)
            
            filteredContactList = contactList.filter({$0.name?.lowercased().contains(fullString) == true})
            contactListTableView.reloadData()
        }
        
        return true
    }
}
