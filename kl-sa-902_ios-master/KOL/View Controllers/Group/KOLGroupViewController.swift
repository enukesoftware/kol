//
//  KOLGroupViewController.swift
//  KOL
//
//  Created by Rupak on 30/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class KOLGroupViewController: UIViewController {
    
    @IBOutlet weak var groupCollectionView: UICollectionView!
    @IBOutlet weak var sendInvitationButton: UIButton!
    @IBOutlet weak var groupNameTextField: UITextField!
    @IBOutlet weak var createGroupView: UIView!
    @IBOutlet weak var kolUsersCollectionView: UICollectionView!
    @IBOutlet weak var kolUsersTableView: UITableView!
    @IBOutlet weak var emptyGroupView: UIView!
    @IBOutlet weak var otherContactsLabel: UILabel!
    
    @IBOutlet weak var kolUserCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var kolUserTabHeightConstraint: NSLayoutConstraint!
    
    let refreshControl = UIRefreshControl()
    var selectedIndexList = [String]()
    var groupListDetails: ContactGroupListDetails? {
        didSet {
            if let groups = groupListDetails?.groups {
                groupList = groups
                groupCollectionView.reloadData()
            }
            emptyGroupView.isHidden = (groupList.count != 0)
        }
    }
    var groupList = [ContactGroupDetails]()
    //KOL user list
    var contactList = [ContactDetails]() {
        didSet {
            if !AppUtility.isDebugMode() {
                numberList = []
                for contact in contactList {
                    numberList.append(contentsOf: contact.numbers)
                }
            }
            let params: [String: Any] = ["contacts" : numberList]
            syncContactList(params: params) { (userList) in
                DispatchQueue.main.async {
                    self.kolUsersCollectionView.reloadData()
                }
            }
        }
    }
    var numberList: [String] = ["913243746","1673166782","2233445566","3344556677","0455667788"]
    var kolUsers = [GroupContactDetails]() {
        didSet {
            if kolUsers.count > 0 {
                self.selectedIndexList = []
                self.createGroupView.isHidden = false
            } else {
                AppUtility.showAlertWithProperty("Alert".localized, messageString: "Sorry no kol users available in your contact list.".localized)
            }
        }
    }
    var otherKOLUsers = [GroupContactDetails]()
    var editingGroupDetails: ContactGroupDetails?
    var selectedEditGroupIdList: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        groupCollectionView.backgroundColor = .clear
        kolUsersCollectionView.backgroundColor = .clear
        
        refreshControl.addTarget(self, action: #selector(handleGroupRefresh(_:)), for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            groupCollectionView.refreshControl = refreshControl
        } else {
            groupCollectionView.addSubview(refreshControl)
        }
        getKOLGroupList { (success) in}
    }
}

//MARK: Action methods
extension KOLGroupViewController {
    
    @IBAction func clickedOnSendInvitation(_ sender: Any) {
        if let orderId = DataManager.shared.runningOrderDetails?.id {
            let params: [String:Any] = ["invited_users": self.getSelectUserIdList(isList: false),
                                        "order" : orderId]
            TaskManager.inviteOrder(params: params) { (success) in
                if success {
                    self.selectedIndexList = []
                    self.groupCollectionView.reloadData()
                    self.sendInvitationButton.isHidden = true
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Invitation sent successfully.".localized)
                }
            }
        } else {
            AppUtility.showAlertWithProperty("Alert!".localized, messageString: "Scan the table QR code first or choose for pickup".localized)
        }
    }
    
    @IBAction func clickedOnCreateGroup(_ sender: Any) {
        self.editingGroupDetails = nil
        self.selectedEditGroupIdList = []
        self.selectedIndexList = []
        TaskManager.getContactList { (contacts) in
            self.contactList = contacts
        }
    }
    
    @IBAction func clickedOnConfirmCreate(_ sender: Any) {
        self.view.endEditing(true)
        
        if let name = groupNameTextField.text, name.count > 0, selectedIndexList.count > 0, editingGroupDetails == nil {
            var params: [String:Any] = ["name" : name]
            AppUtility.showProgress(nil, title: nil)
            createGroup(false,params: params) { (groupId) in
                if let id = groupId {
                    params = ["ids" : self.getSelectUserIdList()]
                    self.addContacts(false, id: id, params: params) { (success) in
                        if success {
                            self.groupNameTextField.text = ""
                            self.selectedIndexList = []
                            self.sendInvitationButton.isHidden = true
                            self.groupCollectionView.reloadData()
                            self.createGroupView.isHidden = true
                            self.selectedEditGroupIdList = []
                            self.getKOLGroupList(false) { (success) in
                                AppUtility.hideProgress()
                                if success {
                                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Group created successfully.".localized)
                                }
                            }
                        } else {
                            AppUtility.hideProgress()
                        }
                    }
                } else {
                    AppUtility.hideProgress()
                }
            }
        } else if let name = groupNameTextField.text, name.count > 0, selectedEditGroupIdList.count > 0, let id = editingGroupDetails?.id {
            var params: [String:Any] = ["name" : name]
            AppUtility.showProgress(nil, title: nil)
            updateGroupName(false, id: id,params: params) { (success) in
                if success {
                    params = ["ids" : self.selectedEditGroupIdList]
                    self.updateGroupContacts(false, id: id, params: params) { (success) in
                        if success {
                            self.groupNameTextField.text = ""
                            self.selectedIndexList = []
                            self.sendInvitationButton.isHidden = true
                            self.groupCollectionView.reloadData()
                            self.createGroupView.isHidden = true
                            self.editingGroupDetails = nil
                            self.selectedEditGroupIdList = []
                            self.getKOLGroupList(false) { (success) in
                                AppUtility.hideProgress()
                                if success {
                                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Group updated successfully.".localized)
                                }
                            }
                        } else {
                            AppUtility.hideProgress()
                        }
                    }
                } else {
                    AppUtility.hideProgress()
                }
            }
        } else {
            groupNameTextField.text = ""
            selectedIndexList = []
            sendInvitationButton.isHidden = true
            groupCollectionView.reloadData()
            createGroupView.isHidden = true
        }
    }
}

//MARK: UICollectionViewDataSource and UICollectionViewDelegate methods
extension KOLGroupViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == kolUsersCollectionView {
            return 1
        }
        return groupList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == kolUsersCollectionView {
            return kolUsers.count
        }
        return groupList[section].contacts?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupCollectionViewCell", for: indexPath) as? GroupCollectionViewCell else {
            return UICollectionViewCell.init()
        }
        cell.delegate = self
        let path = "\(indexPath.section),\(indexPath.row)"
        cell.isSelected = selectedIndexList.contains(path)
        if collectionView == groupCollectionView {
            cell.closeButton.setImage(UIImage(named: "close_green"), for: .normal)
            let contactDetails = groupList[indexPath.section].contacts?[indexPath.row]
            if let urlString = contactDetails?.profilePicture, let url = URL(string: urlString) {
                cell.profilerImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.profilerImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
            } else {
                cell.profilerImageView.image = UIImage.defaultProfile()
            }
            cell.nameLabel.text = contactDetails?.name
        } else if collectionView == kolUsersCollectionView {
            cell.closeButton.setImage(UIImage(named: "remove_user"), for: .normal)
            if let id = kolUsers[indexPath.row].id {
                cell.isSelected = selectedEditGroupIdList.contains(id)
            }
            cell.updateWith(user: kolUsers[indexPath.row])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.width-60)/3
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let path = "\(indexPath.section),\(indexPath.row)"
        if !selectedIndexList.contains(path) {
            selectedIndexList.append(path)
            if collectionView == kolUsersCollectionView {
                if let id = kolUsers[indexPath.row].id {
                    if self.selectedEditGroupIdList.contains(id) {
                        self.selectedEditGroupIdList.remove(object: id)
                    } else {
                        self.selectedEditGroupIdList.append(id)
                    }
                }
                kolUsersCollectionView.reloadData()
            } else {
                groupCollectionView.reloadData()
                sendInvitationButton.isHidden = (selectedIndexList.count == 0)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView == kolUsersCollectionView {
            return CGSize(width: collectionView.frame.width, height: 0)
        }
        return CGSize(width: collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if collectionView == groupCollectionView, section == groupList.count - 1 {
            return CGSize(width: collectionView.frame.width, height: 108)
        }
        return CGSize(width: collectionView.frame.width, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if collectionView == kolUsersCollectionView {
            let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "EmptyHeaderView", for: indexPath)
            return sectionHeader
        }
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            
            guard let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "GroupHeaderCollectionReusableView", for: indexPath) as? GroupHeaderCollectionReusableView else {
                return UICollectionReusableView.init()
            }
            sectionHeader.headerLabel.text = groupList[indexPath.section].name ?? "N/A"
            sectionHeader.indexPath = indexPath
            sectionHeader.delegate = self
            return sectionHeader
        case UICollectionView.elementKindSectionFooter:
            
            guard let sectionFooter = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "GroupFooterCollectionReusableView", for: indexPath) as? GroupFooterCollectionReusableView else {
                return UICollectionReusableView.init()
            }
            return sectionFooter
        default:
            assert(false, "Unexpected element kind")
        }
        return UICollectionReusableView()
    }
}

//MARK: GroupCollectionViewCellDelegate methods
extension KOLGroupViewController: GroupCollectionViewCellDelegate {
    
    func clickedOnDelete(cell: GroupCollectionViewCell) {
        if createGroupView.isHidden {
            guard let indexPath = groupCollectionView.indexPath(for: cell) else {return}
            let path = "\(indexPath.section),\(indexPath.row)"
            selectedIndexList.remove(object: path)
            groupCollectionView.reloadData()
            sendInvitationButton.isHidden = (selectedIndexList.count == 0)
        } else {
            guard let indexPath = kolUsersCollectionView.indexPath(for: cell) else {return}
            if let id = kolUsers[indexPath.row].id {
                if selectedEditGroupIdList.contains(id) {
                    selectedEditGroupIdList.remove(object: id)
                    let path = "\(indexPath.section),\(indexPath.row)"
                    if self.selectedIndexList.contains(path) {
                        self.selectedIndexList.remove(object: path)
                    }
                }
            }
            kolUsersCollectionView.reloadData()
        }
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension KOLGroupViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return otherKOLUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTableViewCell") as? ContactsTableViewCell else {
            return UITableViewCell.init()
        }
        cell.delegate = self
        cell.selectionStyle = .none
        if let id = otherKOLUsers[indexPath.row].id {
            cell.makeSelected = selectedEditGroupIdList.contains(id)
        }
        cell.nameLabel.text = otherKOLUsers[indexPath.row].name
        cell.numberLabel.text = otherKOLUsers[indexPath.row].phoneNumber
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

//MARK: ContactsTableViewCellDelegate methods
extension KOLGroupViewController: ContactsTableViewCellDelegate {
    
    func clickedOnAdd(cell: ContactsTableViewCell) {
        guard let indexPath = kolUsersTableView.indexPath(for: cell) else {return}
        if let id = otherKOLUsers[indexPath.row].id {
            if !selectedEditGroupIdList.contains(id) {
                selectedEditGroupIdList.append(id)
            } else {
                selectedEditGroupIdList.remove(object: id)
            }
            kolUsersTableView.reloadData()
        }
    }
}

//MARK: Refresh methods
extension KOLGroupViewController {
    
    @objc func handleGroupRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        getKOLGroupList { (succcess) in}
    }
}

//MARK: GroupHeaderCollectionReusableViewDelegate methods
extension KOLGroupViewController: GroupHeaderCollectionReusableViewDelegate {
    
    func clickedOnMore(with view: GroupHeaderCollectionReusableView) {
        guard let indexPath = view.indexPath else {return}
        if indexPath.section < groupList.count {
            let group = groupList[indexPath.section]
            let actionController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            actionController.addAction(UIAlertAction.init(title: "Edit".localized, style: .default, handler: { (alertAction) in
                self.editingGroupDetails = group
                self.groupNameTextField.text = group.name
                TaskManager.getContactList { (contacts) in
                    self.contactList = contacts
                }
            }))
            actionController.addAction(UIAlertAction.init(title: "Delete".localized, style: .default, handler: { (alertAction) in
                AlertManager.showAlertWith(title: "Alert".localized, message: "Are you sure to delete this group?".localized, actions:
                    [CustomAction(index: 0, title: "Cancel".localized, style: .cancel),
                     CustomAction(index: 1, title: "Delete".localized, style: .default)]) { (action) in
                        if action.index == 1 {
                            if let id = group.id {
                                self.deleteGroup(id: id) { (success) in
                                    if success {
                                        self.getKOLGroupList { (success) in
                                            AlertManager.showAlertWith(title: "Success".localized, message: "Group deleted successfully.".localized, nil)
                                        }
                                    }
                                }
                            }
                        }
                }
            }))
            actionController.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
            self.present(actionController, animated: true, completion: nil)
        }
    }
}

//MARK: Request methods
extension KOLGroupViewController {
    
    func getKOLGroupList(_ showLoading: Bool = true,_ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading{AppUtility.showProgress(nil, title: nil)}
        RequestManager.getKOLGroupList(completion: { (object) in
            if showLoading{AppUtility.hideProgress(nil)}
            self.groupListDetails = object
            completion(object != nil)
        }) { (error, code, message) in
            if showLoading{AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func syncContactList(params: [String:Any], _ completion: @escaping(_ object: [GroupContactDetails]) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.syncContactList(params: params, completion: { (object) in
            AppUtility.hideProgress(nil)
            self.kolUsers = object
            if self.editingGroupDetails != nil {
                self.kolUsers = self.editingGroupDetails?.contacts ?? []
                self.selectedEditGroupIdList = []
                for user in self.kolUsers {
                    if let id = user.id {
                        self.selectedEditGroupIdList.append(id)
                    }
                }
                if let editingList = self.editingGroupDetails?.contacts {
                    self.otherKOLUsers = []
                    for item in object {
                        if let id = item.id, editingList.filter({$0.id == id}).count == 0 {
                            self.otherKOLUsers.append(item)
                        }
                    }
                    self.kolUsersTableView.reloadData()
                }
            }
            self.otherContactsLabel.isHidden = (self.editingGroupDetails == nil)
            let width = (self.view.frame.width-60)/3
            self.kolUserCollectionViewHeightConstraint.constant = CGFloat(Int(ceil((Double(self.kolUsers.count)/3.0))))*(width+10)
            self.kolUserTabHeightConstraint.constant = CGFloat(self.otherKOLUsers.count * 60)
            completion(object)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion([])
        }
    }
    
    func createGroup(_ showLoading: Bool = true, params: [String:Any], _ completion: @escaping(_ object: Int?) -> Void) {
        if showLoading{AppUtility.showProgress(nil, title: nil)}
        RequestManager.createContactGroup(params: params, completion: { (object) in
            if showLoading{AppUtility.hideProgress(nil)}
            completion(object)
        }) { (error, code, message) in
            if showLoading{AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(nil)
        }
    }
    
    func updateGroupName(_ showLoading: Bool = true, id: Int, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading{AppUtility.showProgress(nil, title: nil)}
        RequestManager.updateContactGroupName(id: id, params: params, completion: { (success) in
            if showLoading{AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading{AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func updateGroupContacts(_ showLoading: Bool = true, id: Int, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading{AppUtility.showProgress(nil, title: nil)}
        RequestManager.updateContactGroupContacts(id: id, params: params, completion: { (success) in
            if showLoading{AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading{AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func addContacts(_ showLoading: Bool = true, id: Int, params: [String:Any], _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading{AppUtility.showProgress(nil, title: nil)}
        RequestManager.addContactsIntoGroup(id: id, params: params, completion: { (success) in
            if showLoading{AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading{AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
    
    func deleteGroup(_ showLoading: Bool = true, id: Int, _ completion: @escaping(_ success: Bool) -> Void) {
        if showLoading{AppUtility.showProgress(nil, title: "Deleting...".localized)}
        RequestManager.deleteContactGroup(id: id, completion: { (success) in
            if showLoading{AppUtility.hideProgress(nil)}
            completion(success)
        }) { (error, code, message) in
            if showLoading{AppUtility.hideProgress(nil)}
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion(false)
        }
    }
}

//MARK: Other methods
extension KOLGroupViewController {
    
    func getSelectUserIdList(isList: Bool = true) -> [Int] {
        var idList = [Int]()
        for object in selectedIndexList {
            if let section = Int(object.components(separatedBy: ",").first ?? ""), let index = Int(object.components(separatedBy: ",").last ?? "") {
                if isList, index < kolUsers.count, let id = kolUsers[index].id {
                    idList.append(id)
                } else if !isList, section < groupList.count, index < (groupList[section].contacts?.count ?? 0), let id = groupList[section].contacts?[index].id {
                    idList.append(id)
                }
            }
        }
        return idList
    }
}
