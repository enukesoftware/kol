//
//  ContactListViewController.swift
//  KOL
//
//  Created by Rupak on 30/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol ContactListViewControllerDelegate {
    func shouldUpdateGroupPosition(with controller: ContactListViewController)
}

class ContactListViewController: UIViewController {

    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var kolUsersCollectionView: UICollectionView!
    @IBOutlet weak var emptyUserView: UIView!
    @IBOutlet weak var sendInvitationButton: UIButton!
    
    var contactList = [ContactDetails]() {
        didSet {
            self.contactsTableView.reloadData()
        }
    }
    var numberList: [String] = ["913243746","1673166782","2233445566","3344556677","0455667788"]
    var kolUsers = [GroupContactDetails]() {
        didSet {
            emptyUserView.isHidden = (kolUsers.count > 0)
        }
    }
    var delegate: ContactListViewControllerDelegate?
    var selectedUserIdList = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        kolUsersCollectionView.backgroundColor = .clear
        TaskManager.getContactList { (contacts) in
            DispatchQueue.main.async {
                self.contactList = contacts.sorted(by: {($0.name ?? "") < ($1.name ?? "")})
                //sync list
                if !AppUtility.isDebugMode() {
                    self.numberList = []
                    for contact in self.contactList {
                        self.numberList.append(contentsOf: contact.numbers)
                    }
                }
                let params: [String: Any] = ["contacts" : self.numberList]
                self.syncContactList(params: params) { (userList) in
                    DispatchQueue.main.async {
                        self.kolUsersCollectionView.reloadData()
                    }
                }
                self.delegate?.shouldUpdateGroupPosition(with: self)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        if kolUsers.count == 1 {
//            if var frame = contactsTableView.tableHeaderView?.frame {
//                frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height/1.7)
//                contactsTableView.tableHeaderView?.frame = frame
//                contactsTableView.reloadData()
//            }
//        }
    }
}

//MARK: Action methods
extension ContactListViewController {
    
    @IBAction func clickedOnSendInvitation(_ sender: Any) {
        if let orderId = DataManager.shared.runningOrderDetails?.id {
            let params: [String:Any] = ["invited_users": self.selectedUserIdList,
                                        "order" : orderId]
            TaskManager.inviteOrder(params: params) { (success) in
                if success {
                    self.selectedUserIdList = []
                    self.kolUsersCollectionView.reloadData()
                    self.sendInvitationButton.isHidden = true
                    AppUtility.showAlertWithProperty("Success".localized, messageString: "Invitation sent successfully.".localized)
                }
            }
        } else {
            AppUtility.showAlertWithProperty("Alert!".localized, messageString: "Scan the table QR code first or choose for pickup".localized)
        }
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension ContactListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTableViewCell") as? ContactsTableViewCell else {
            return UITableViewCell.init()
        }
        cell.delegate = self
        cell.selectionStyle = .none
        cell.updateWith(contact: contactList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

//MARK: UICollectionViewDataSource and UICollectionViewDelegate methods
extension ContactListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return kolUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupCollectionViewCell", for: indexPath) as? GroupCollectionViewCell else {
            return UICollectionViewCell.init()
        }
        cell.delegate = self
        if let id = kolUsers[indexPath.row].id {
            cell.isSelected = selectedUserIdList.contains(id)
        }
        
        cell.updateWith(user: kolUsers[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.height-10)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let id = kolUsers[indexPath.row].id {
            if !selectedUserIdList.contains(id) {
                selectedUserIdList.append(id)
            } else {
                selectedUserIdList.remove(object: id)
            }
            kolUsersCollectionView.reloadData()
            sendInvitationButton.isHidden = (selectedUserIdList.count == 0)
        }
    }
}

//MARK: ContactsTableViewCellDelegate methods
extension ContactListViewController: ContactsTableViewCellDelegate {
    
    func clickedOnAdd(cell: ContactsTableViewCell) {
        self.view.endEditing(true)
        guard let indexPath = contactsTableView.indexPath(for: cell) else {return}
        if indexPath.row < contactList.count {
            if contactList[indexPath.row].kolUserDetails == nil {
                let activityViewController = UIActivityViewController(activityItems: ["Download KOL app & Join the Order!".localized, "\nhttps://apps.apple.com/sa/app/kol-app/id1495841798 \nhttps://play.google.com/store/apps/details?id=com.dtmweb.kol"] , applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            } else if let id = contactList[indexPath.row].kolUserDetails?.id {
                if let orderId = DataManager.shared.runningOrderDetails?.id {
                    let params: [String:Any] = ["invited_users": [id],
                                                "order" : orderId]
                    TaskManager.inviteOrder(params: params) { (success) in
                        if success {
                            AppUtility.showAlertWithProperty("Success".localized, messageString: "Invitation sent successfully.".localized)
                        }
                    }
                } else {
                    AppUtility.showAlertWithProperty("Alert!".localized, messageString: "Scan the table QR code first or choose for pickup".localized)
                }
            }
        }
    }
}

//MARK: GroupCollectionViewCellDelegate methods
extension ContactListViewController: GroupCollectionViewCellDelegate {
    
    func clickedOnDelete(cell: GroupCollectionViewCell) {
        guard let indexPath = kolUsersCollectionView.indexPath(for: cell) else {return}
        if let id = kolUsers[indexPath.row].id {
            if selectedUserIdList.contains(id) {
                selectedUserIdList.remove(object: id)
            }
            kolUsersCollectionView.reloadData()
        }
    }
}

//MARK: Request methods
extension ContactListViewController {
    
    func syncContactList(params: [String:Any], _ completion: @escaping(_ object: [GroupContactDetails]) -> Void) {
        AppUtility.showProgress(nil, title: nil)
        RequestManager.syncContactList(params: params, completion: { (object) in
            AppUtility.hideProgress(nil)
            self.kolUsers = object
            
            var finalContacts = self.contactList
            for index in 0..<finalContacts.count {
                if let number = finalContacts[index].numbers.first?.digits, let userDetaills = self.kolUsers.filter({number.hasSuffix($0.phoneNumber?.digits.suffix(9) ?? "N/A") == true}).first {
                    finalContacts[index].kolUserDetails = userDetaills
                }
            }
            self.contactList = finalContacts.sorted(by: {($0.name ?? "") < ($1.name ?? "")})
            
            completion(object)
        }) { (error, code, message) in
            AppUtility.hideProgress(nil)
            if let errorMessage = message {
                AppUtility.showAlertWithProperty("Alert!".localized, messageString: errorMessage)
            }
            completion([])
        }
    }
}
