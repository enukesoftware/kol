//
//  GroupDashboardViewController.swift
//  KOL
//
//  Created by Rupak on 30/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class GroupDashboardViewController: UIViewController {

    @IBOutlet weak var kolControl: UIControl!
    @IBOutlet weak var contactControl: UIControl!
    @IBOutlet weak var activeControlIndicatorView: UIView!
    
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    
    var kolIsSelected = false {
        didSet {
            if kolIsSelected {
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                    if LanguageManger.shared.currentLanguage == .en {
                        self.activeControlIndicatorView.frame = CGRect(x: 0, y: self.activeControlIndicatorView.frame.origin.y, width: self.view.frame.width/2.0, height: 4)
                    } else {
                        self.activeControlIndicatorView.frame = CGRect(x: self.kolControl.frame.origin.x, y: self.activeControlIndicatorView.frame.origin.y, width: self.view.frame.width/2.0, height: 4)
                    }
                }) { (success) in}
                groupTabBarController?.selectedIndex = 0
            } else {
                if LanguageManger.shared.currentLanguage == .en {
                    UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                        self.activeControlIndicatorView.frame = CGRect(x: self.contactControl.frame.origin.x, y: self.activeControlIndicatorView.frame.origin.y, width: self.view.frame.width/2.0, height: 4)
                    }) { (success) in}
                } else {
                    UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                        self.activeControlIndicatorView.frame = CGRect(x: 0, y: self.activeControlIndicatorView.frame.origin.y, width: self.view.frame.width/2.0, height: 4)
                    }) { (success) in}
                }
                
                groupTabBarController?.selectedIndex = 1
            }
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    var groupTabBarController: UITabBarController?
    var isFromInviteFriend = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        kolIsSelected = isFromInviteFriend
        topViewHeightConstraint.constant = UIApplication.shared.statusBarFrame.height+106
        (groupTabBarController?.viewControllers?.last as? ContactListViewController)?.delegate = self
    }
}

//MARK: Action methods
extension GroupDashboardViewController {
    
    @IBAction func clickedOnKol(_ sender: Any) {
        kolIsSelected = true
    }
    
    @IBAction func clickedOnContact(_ sender: Any) {
        kolIsSelected = false
    }
    
    @IBAction func clickedOnSearch(_ sender: Any) {
        
        if let controller = Storyboard.controllerWith(name: "SearchContactViewController") as? SearchContactViewController {
            controller.modalPresentationStyle = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            controller.contactList = (groupTabBarController?.viewControllers?.last as? ContactListViewController)?.contactList ?? []
            self.present(controller, animated: true, completion: nil)
        }
    }
}

//MARK: ContactListViewControllerDelegate methods
extension GroupDashboardViewController: ContactListViewControllerDelegate {
    
    func shouldUpdateGroupPosition(with controller: ContactListViewController) {
        if !isFromInviteFriend, ((groupTabBarController?.viewControllers?.last as? ContactListViewController)?.contactList.count ?? 0) > 0 {
            (groupTabBarController?.viewControllers?.last as? ContactListViewController)?.contactsTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
    }
}

//MARK: Segue related methods
extension GroupDashboardViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GroupTabBarControllerSegue", let controller = segue.destination as? UITabBarController {
            groupTabBarController = controller
        }
    }
}
