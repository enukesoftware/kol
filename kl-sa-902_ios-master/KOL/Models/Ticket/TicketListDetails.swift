//
//  TicketListDetails.swift
//  KOL
//
//  Created by Rupak on 14/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class TicketListDetails: Mappable {

    var count : Int?
    var next : String?
    var previous : String?
    var tickets : [TicketDetails]?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        count <- map["count"]
        next <- map["next"]
        previous <- map["previous"]
        tickets <- map["results"]
    }
}

class TicketDetails: Mappable {
    
    var id : Int?
    var text : String?
    var textInAr : String?
    
    func localizedText() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? textInAr : text
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        text <- map["text"]
        textInAr <- map["text_in_ar"]
    }
}
