//
//  IDName.swift
//  KOL
//
//  Created by Rupak Biswas on 2/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class IDName: NSObject, Mappable {
    
    var id : Int?
    var name : String?
    var arName : String?
    var country : IDName?
    
    var finalName: String? {
        if arName != nil && arName != "" && LanguageManger.shared.currentLanguage == .ar {
            return arName
        }
        return name
    }
    
    required init?(map: Map){}
    required override init() {}
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        arName <- map["ar_name"]
        country <- map["country"]
    }
}

