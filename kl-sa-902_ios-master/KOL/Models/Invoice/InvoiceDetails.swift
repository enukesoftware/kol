//
//  InvoiceDetails.swift
//  KOL
//
//  Created by Rupak on 18/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class InvoiceDetails: Mappable {

    var createdAt : String?
    var id : Int?
    var invoiceItems : [InvoiceItemDetails]?
    var order : Int?
    var restaurantName : String?
    var hasRestaurantAccepted : Bool?

    required init?(map: Map){}

    func mapping(map: Map)
    {
        createdAt <- map["created_at"]
        id <- map["id"]
        invoiceItems <- map["invoice_items"]
        order <- map["order"]
        restaurantName <- map["restaurant_name"]
        hasRestaurantAccepted <- map["has_restaurant_accepted"]
    }
    
    func getUserList() -> [String] {
        var list = [String]()
        for items in invoiceItems ?? [] {
            if let name = items.user?.localizedName() {
                list.append(name)
            }
        }
        return list
    }
    
    func getTotalCalorie() -> Int {
        var amount = 0
        for items in invoiceItems ?? [] {
            for food in items.foodItems ?? [] {
                amount += food.foodItemCalorie ?? 0
            }
        }
        return amount
    }
    
    func getTotalPrice() -> Double {
        var amount = 0.0
        for items in invoiceItems ?? [] {
            amount += Double(items.amount ?? "0") ?? 0.0
        }
        return amount
    }
}

class InvoiceItemDetails: Mappable {

    var amount : String?
    var generalAmount : String?
    var taxAmount : String?
    var foodItems : [OrderItemDetails]?
    var id : Int?
    var invoice : Int?
    var paid : Bool?
    var user : UserDetails?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        amount <- map["amount"]
        generalAmount <- map["general_amount"]
        taxAmount <- map["tax_amount"]
        foodItems <- map["food_items"]
        id <- map["id"]
        invoice <- map["invoice"]
        paid <- map["paid"]
        user <- map["user"]
    }
}
