//
//  TransactionResponseDetails.swift
//  KOL
//
//  Created by Rupak on 18/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class TransactionResponseDetails: Mappable {

    var amount : String?
    var createdAt : String?
    var currency : String?
    var id : Int?
    var invoiceItems : [Int]?
    var order : Int?
    var ptOrderId : String?
    var ptTransactionId : String?
    var transactionStatus : Int?
    var user : Int?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        amount <- map["amount"]
        createdAt <- map["created_at"]
        currency <- map["currency"]
        id <- map["id"]
        invoiceItems <- map["invoice_items"]
        order <- map["order"]
        ptOrderId <- map["pt_order_id"]
        ptTransactionId <- map["pt_transaction_id"]
        transactionStatus <- map["transaction_status"]
        user <- map["user"]
    }
}
