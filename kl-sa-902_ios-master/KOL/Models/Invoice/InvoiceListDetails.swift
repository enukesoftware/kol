//
//  InvoiceListDetails.swift
//  KOL
//
//  Created by Rupak on 20/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class InvoiceListDetails: Mappable {

    var count : Int?
    var next : String?
    var previous : String?
    var items : [InvoiceDetails]?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        count <- map["count"]
        next <- map["next"]
        previous <- map["previous"]
        items <- map["results"]
    }
}

struct InvoiceGroupDetails {
    var dateString = ""
    var date: Date {
        return dateString.convertDate("dd-MM-yyyy")
    }
    var items = [InvoiceDetails]()
}
