//
//  FileDetails.swift
//  KOL
//
//  Created by Rupak on 17/08/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

struct FileDetails {
    var fileName = ""
    var mimeType = ""
    var fileData: Data?
    var image: UIImage?
    var fileUrl: URL?
    var id = -1
    
    init(fileName: String, mimeType: String, fileData: Data?) {
        self.fileData = fileData
        self.mimeType = mimeType
        self.fileName = fileName
    }
}
