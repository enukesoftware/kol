//
//  ParticipantListDetails.swift
//  KOL
//
//  Created by Rupak on 28/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class ParticipantListDetails: Mappable {

    var count : Int?
    var next : String?
    var previous : String?
    var users : [ParticipantDetails]?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        count <- map["count"]
        next <- map["next"]
        previous <- map["previous"]
        users <- map["results"]
    }
}

class ParticipantDetails: Mappable {

    var createdAt : String?
    var order : Int?
    var user : UserDetails?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        createdAt <- map["created_at"]
        order <- map["order"]
        user <- map["user"]
    }
}
