//
//  AddProductRequestDetails.swift
//  KOL
//
//  Created by Rupak on 12/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class AddProductRequestDetails: NSObject, Mappable {

    var foodItem : Int?
    var invitedUsers : [Int]?
    var order : Int?
    var orderItemAddOns : [OrderItemRequestAddOn]?
    var orderItemAttributeMatrices : [OrderItemRequestAttributeMatrice]?
    var quantity : Int?
    var status : Int?
    
    required init?(map: Map){}
    override init() {}

    func mapping(map: Map)
    {
        foodItem <- map["food_item"]
        invitedUsers <- map["invited_users"]
        order <- map["order"]
        orderItemAddOns <- map["order_item_add_ons"]
        orderItemAttributeMatrices <- map["order_item_attribute_matrices"]
        quantity <- map["quantity"]
        status <- map["status"]
    }
}

class OrderItemRequestAddOn: NSObject, Mappable {

    var foodAddOn : Int?
    var foodAddOnDetails : FoodAddOnRequestDetails?
    var id : Int?
    var createdAt : String?

    required init?(map: Map){}
    override init() {}

    func mapping(map: Map)
    {
        foodAddOn <- map["food_add_on"]
        foodAddOnDetails <- map["food_add_on_details"]
        id <- map["id"]
        createdAt <- map["created_at"]
    }
}

class FoodAddOnRequestDetails: Mappable {

    var id : Int?
    var food : Int?
    var name : String?
    var price : String?

    required init?(map: Map){}

    func mapping(map: Map)
    {
        id <- map["id"]
        food <- map["food"]
        name <- map["name"]
        price <- map["price"]
    }
    
    init(fromDictionary dictionary: [String:Any]){
        food = dictionary["food"] as? Int
        name = dictionary["name"] as? String
        price = dictionary["price"] as? String
    }
}

class OrderItemRequestAttributeMatrice: NSObject, Mappable {

    var foodAttributeMatrix : Int?
    var foodAttributeMatrixDetails : FoodAttributeMatrixRequestDetails?
    var createdAt : String?
    var id : Int?

    required init?(map: Map){}
    override init() {}

    func mapping(map: Map)
    {
        foodAttributeMatrix <- map["food_attribute_matrix"]
        foodAttributeMatrixDetails <- map["food_attribute_matrix_details"]
        createdAt <- map["created_at"]
        id <- map["id"]
    }
}

class FoodAttributeMatrixRequestDetails: Mappable {

    var id : Int?
    var attribute : Int?
    var name : String?

    required init?(map: Map){}

    func mapping(map: Map)
    {
        id <- map["id"]
        attribute <- map["attribute"]
        name <- map["name"]
    }
    
    init(fromDictionary dictionary: [String:Any]){
        attribute = dictionary["attribute"] as? Int
        name = dictionary["name"] as? String
    }
}
