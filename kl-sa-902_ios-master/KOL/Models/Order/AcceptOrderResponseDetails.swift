//
//  AcceptOrderResponseDetails.swift
//  KOL
//
//  Created by Rupak on 01/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class AcceptOrderResponseDetails: Mappable {

    var createdAt : String?
    var id : Int?
    var invitedBy : Int?
    var invitedUser : Int?
    var order : Int?
    var restaurantId : Int?
    var status : Int?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        createdAt <- map["created_at"]
        id <- map["id"]
        invitedBy <- map["invited_by"]
        invitedUser <- map["invited_user"]
        order <- map["order"]
        restaurantId <- map["restaurant_id"]
        status <- map["status"]
    }
}
