//
//  OrderDetails.swift
//  KOL
//
//  Created by Rupak on 10/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class OrderDetails: Mappable {

    var confirmed : Bool?
    var createdAt : String?
    var createdBy : Int?
    var id : Int?
    var orderItemSet : [Int]?
    var orderParticipants : [ParticipantDetails]?
    var orderType : Int?
    var restaurant : Int?
    var table : Int?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        confirmed <- map["confirmed"]
        createdAt <- map["created_at"]
        createdBy <- map["created_by"]
        id <- map["id"]
        orderItemSet <- map["order_item_set"]
        orderParticipants <- map["order_participants"]
        orderType <- map["order_type"]
        restaurant <- map["restaurant"]
        table <- map["table"]
    }
}

class OrderItemDetails: Mappable {

    var addedBy : Int?
    var createdAt : String?
    var foodItem : Int?
    var foodItemCalorie : Int?
    var foodItemName : String?
    var foodItemPrice : String?
    var id : Int?
    var order : Int?
    var orderItemAddOns : [OrderItemRequestAddOn]?
    var orderItemAttributeMatrices : [OrderItemRequestAttributeMatrice]?
    var quantity : Int?
    var sharedWith : [Int]?
    var status : Int?
    
    var sharedPriceWithTax : Double?
    var sharedPriceWithoutTax : Double?
    var sharedTotalTax : Double?
    var totalPriceWithTax : Double?
    var totalPriceWithoutTax : Double?
    var totalTax : Double?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        addedBy <- map["added_by"]
        createdAt <- map["created_at"]
        foodItem <- map["food_item"]
        foodItemCalorie <- map["food_item_calorie"]
        foodItemName <- map["food_item_name"]
        foodItemPrice <- map["food_item_price"]
        id <- map["id"]
        order <- map["order"]
        orderItemAddOns <- map["order_item_add_ons"]
        orderItemAttributeMatrices <- map["order_item_attribute_matrices"]
        quantity <- map["quantity"]
        sharedWith <- map["shared_with"]
        status <- map["status"]
        
        sharedPriceWithTax <- map["shared_price_with_tax"]
        sharedPriceWithoutTax <- map["shared_price_without_tax"]
        sharedTotalTax <- map["shared_total_tax"]
        totalPriceWithTax <- map["total_price_with_tax"]
        totalPriceWithoutTax <- map["total_price_without_tax"]
        totalTax <- map["total_tax"]
    }
}
