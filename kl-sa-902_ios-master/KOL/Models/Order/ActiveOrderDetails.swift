//
//  ActiveOrderDetails.swift
//  KOL
//
//  Created by Rupak on 10/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class ActiveOrderDetails: Mappable {

    var lastOrder : Int?
    var lastOrderInCheckout : Bool?
    var lastOrderInRating : Bool?
    var lastOrderType : Int?
    var lastRestaurant : Int?
    var state : String?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        lastOrder <- map["last_order"]
        lastOrderInCheckout <- map["last_order_in_checkout"]
        lastOrderInRating <- map["last_order_in_rating"]
        lastOrderType <- map["last_order_type"]
        lastRestaurant <- map["last_restaurant"]
        state <- map["state"]
    }
}
