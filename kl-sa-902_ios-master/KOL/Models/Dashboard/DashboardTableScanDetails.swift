//
//  DashboardTableScanDetails.swift
//  KOL
//
//  Created by Rupak on 21/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class DashboardTableScanDetails: NSObject, Mappable {

    var restaurantId : Int?
    var tableId : Int?
    
    required init?(map: Map){}
    init(restaurantId: Int?, tableId: Int?) {
        self.restaurantId = restaurantId
        self.tableId = tableId
    }
    
    func mapping(map: Map)
    {
        restaurantId <- map["restaurant_id"]
        tableId <- map["table_id"]
    }
}
