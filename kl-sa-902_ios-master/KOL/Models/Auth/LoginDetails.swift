//
//  LoginDetails.swift
//  KOL
//
//  Created by Rupak on 17/08/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginDetails: Mappable {
    
    var access : String?
    var accessTokenLifetime : Float?
    var customer : CustomerDetails?
    var profileType : String?
    var refresh : String?
    var refreshTokenLifetime : Float?
    var user : [UserDetails]?
    var fetchedTime = Date().dateStringWithFormat(AppConstants.DateFormate.Default, local: false)
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        access <- map["access"]
        accessTokenLifetime <- map["access_token_lifetime"]
        customer <- map["customer"]
        profileType <- map["profile_type"]
        refresh <- map["refresh"]
        refreshTokenLifetime <- map["refresh_token_lifetime"]
        user <- map["user"]
        fetchedTime <- map["fetchedTime"]
    }
}

class UserDetails: Mappable {
    
    var email : String?
    var id : Int?
    var locale : String?
    var name : String?
    var nameInAr : String?
    var phoneNumber : String?
    var phoneNumberVerified : Bool?
    var profilePicture : String?
    
    func localizedName() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? (nameInAr != nil ? nameInAr : name) : name
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        email <- map["email"]
        id <- map["id"]
        locale <- map["locale"]
        name <- map["name"]
        nameInAr <- map["name_in_ar"]
        phoneNumber <- map["phone_number"]
        phoneNumberVerified <- map["phone_number_verified"]
        profilePicture <- map["profile_picture"]
    }
}

class CustomerDetails: Mappable {
    
    var qrCode : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        qrCode <- map["qr_code"]
    }
}
