//
//  RefreshTokenDetails.swift
//  KOL
//
//  Created by Rupak on 06/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class RefreshTokenDetails: Mappable {
    
    var access : String?
    var refresh : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        access <- map["access"]
        refresh <- map["refresh"]
    }
}
