//
//  RegisterResponseDetails.swift
//  KOL
//
//  Created by Rupak on 17/08/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class RegisterResponseDetails: Mappable {

    var qrCode : String?
    var user : UserDetails?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        qrCode <- map["qr_code"]
        user <- map["user"]
    }
}
