//
//  ContactGroupListDetails.swift
//  KOL
//
//  Created by Rupak on 25/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class ContactGroupListDetails: Mappable {

    var count : Int?
    var next : String?
    var previous : String?
    var groups : [ContactGroupDetails]?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        count <- map["count"]
        next <- map["next"]
        previous <- map["previous"]
        groups <- map["results"]
    }
}

class ContactGroupDetails: Mappable {

    var contacts : [GroupContactDetails]?
    var createdAt : String?
    var id : Int?
    var name : String?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        contacts <- map["contacts"]
        createdAt <- map["created_at"]
        id <- map["id"]
        name <- map["name"]
    }
}

class GroupContactDetails: Mappable {

    var id : Int?
    var name : String?
    var phoneNumber : String?
    var profilePicture : String?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        phoneNumber <- map["phone_number"]
        profilePicture <- map["profile_picture"]
    }
}
