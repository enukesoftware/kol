//
//  FCMDetails.swift
//  KOL
//
//  Created by Rupak on 30/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class FCMDetails: Mappable {

    var active : Bool?
    var dateCreated : String?
    var deviceId : String?
    var id : Int?
    var name : String?
    var registrationId : String?
    var type : String?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        active <- map["active"]
        dateCreated <- map["date_created"]
        deviceId <- map["device_id"]
        id <- map["id"]
        name <- map["name"]
        registrationId <- map["registration_id"]
        type <- map["type"]
    }
}
