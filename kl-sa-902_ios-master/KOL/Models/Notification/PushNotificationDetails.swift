//
//  PushNotificationDetails.swift
//  KOL
//
//  Created by Rupak on 30/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class PushNotificationDetails: Mappable {

    var body : String?
    var fromUserId : String?
    var fromUserName : String?
    var fromUserProfilePicture : String?
    var gcmmessageId : String?
    var googlecae : String?
    var inviteId : String?
    var notificationAction : String?
    var notificationId : String?
    var title : String?
    var toUser : String?
    
    var addedByName : String?
    var orderId : String?
    var orderItemId : String?
    
    var fromUserPhoneNumber : String?
    var itemCalorie : String?
    var itemId : String?
    var itemName : String?
    var itemPicture : String?
    var itemPrice : String?
    var itemQuantity : String?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        body <- map["body"]
        fromUserId <- map["from_user_id"]
        fromUserName <- map["from_user_name"]
        fromUserProfilePicture <- map["from_user_profile_picture"]
        gcmmessageId <- map["gcm.message_id"]
        googlecae <- map["google.c.a.e"]
        inviteId <- map["invite_id"]
        notificationAction <- map["notification_action"]
        notificationId <- map["notification_id"]
        title <- map["title"]
        toUser <- map["to_user"]
        
        addedByName <- map["added_by_name"]
        orderId <- map["order_id"]
        orderItemId <- map["order_item_id"]
        
        fromUserPhoneNumber <- map["from_user_phone_number"]
        itemCalorie <- map["item_calorie"]
        itemId <- map["item_id"]
        itemName <- map["item_name"]
        itemPicture <- map["item_picture"]
        itemPrice <- map["item_price"]
        itemQuantity <- map["item_quantity"]
    }
}

struct StringTransform: TransformType {
    
    func transformFromJSON(_ value: Any?) -> String? {
        return value.flatMap(String.init(describing:))
    }
    
    func transformToJSON(_ value: String?) -> Any? {
        return value
    }
}
