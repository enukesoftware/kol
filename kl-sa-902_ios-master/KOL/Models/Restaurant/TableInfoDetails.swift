//
//  TableInfoDetails.swift
//  KOL
//
//  Created by Rupak on 21/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class TableInfoDetails: Mappable {

    var id : Int?
    var name : String?
    var publicField : Bool?
    var qrCode : String?
    var user : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        publicField <- map["public"]
        qrCode <- map["qr_code"]
        user <- map["user"]
    }
}
