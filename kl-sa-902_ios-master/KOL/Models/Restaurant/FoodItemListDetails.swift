//
//  FoodItemListDetails.swift
//  KOL
//
//  Created by Rupak on 23/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class FoodItemListDetails: Mappable {

    var count : Int?
    var next : String?
    var previous : String?
    var items : [FoodItemDetails]?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        count <- map["count"]
        next <- map["next"]
        previous <- map["previous"]
        items <- map["results"]
    }
}

class FoodItemDetails: Mappable {
    
    var addons : [FoodAddonDetails]?
    var attributes : [FoodAttributeDetails]?
    var calorie : Int?
    var category : Int?
    var categoryName : String?
    var categoryNameInAr : String?
    var id : Int?
    var isActive : Bool?
    var name : String?
    var nameInAr : String?
    var picture : String?
    var price : String?
    var user : UserDetails?
    
    func localizedName() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? nameInAr : name
    }
    
    func localizedCategory() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? categoryNameInAr : categoryName
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        addons <- map["addons"]
        attributes <- map["attributes"]
        calorie <- map["calorie"]
        category <- map["category"]
        categoryName <- map["category_name"]
        categoryNameInAr <- map["category_name_in_ar"]
        id <- map["id"]
        isActive <- map["is_active"]
        name <- map["name"]
        nameInAr <- map["name_in_ar"]
        picture <- map["picture"]
        price <- map["price"]
        user <- map["user"]
    }
}

class FoodAddonDetails: Mappable {
    
    var food : Int?
    var id : Int?
    var name : String?
    var nameInAr : String?
    var price : String?
    
    func localizedName() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? nameInAr : name
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        food <- map["food"]
        id <- map["id"]
        name <- map["name"]
        nameInAr <- map["name_in_ar"]
        price <- map["price"]
    }
}

class FoodAttributeDetails: Mappable {
    
    var attributeMatrix : [FoodAttributeMatrixDetails]?
    var food : Int?
    var id : Int?
    var name : String?
    var nameInAr : String?
    
    func localizedName() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? nameInAr : name
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        attributeMatrix <- map["attribute_matrix"]
        food <- map["food"]
        id <- map["id"]
        name <- map["name"]
        nameInAr <- map["name_in_ar"]
    }
}

class FoodAttributeMatrixDetails: Mappable {
    
    var attribute : Int?
    var id : Int?
    var name : String?
    var nameInAr : String?
    
    func localizedName() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? nameInAr : name
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        attribute <- map["attribute"]
        id <- map["id"]
        name <- map["name"]
        nameInAr <- map["name_in_ar"]
    }
}
