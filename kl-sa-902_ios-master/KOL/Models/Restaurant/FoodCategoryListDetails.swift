//
//  FoodCategoryListDetails.swift
//  KOL
//
//  Created by Rupak on 24/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class FoodCategoryListDetails: Mappable {

    var count : Int?
    var next : String?
    var previous : String?
    var categories : [FoodCategoryDetails]?
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        count <- map["count"]
        next <- map["next"]
        previous <- map["previous"]
        categories <- map["results"]
    }
}

class FoodCategoryDetails: Mappable {

    var createdAt : String?
    var id : Int?
    var name : String?
    var nameInAr : String?
    var user : Int?
    
    func localizedName() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? nameInAr : name
    }
    
    required init?(map: Map){}

    func mapping(map: Map)
    {
        createdAt <- map["created_at"]
        id <- map["id"]
        name <- map["name"]
        nameInAr <- map["name_in_ar"]
        user <- map["user"]
    }
}
