//
//  RestaurantListDetails.swift
//  KOL
//
//  Created by Rupak on 18/09/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import ObjectMapper

class RestaurantListDetails: Mappable {

    var count : Int?
    var next : String?
    var previous : String?
    var restaurants : [RestaurantDetails]?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        count <- map["count"]
        next <- map["next"]
        previous <- map["previous"]
        restaurants <- map["results"]
    }
}

class RestaurantDetails: Mappable {
    
    var coverPicture : String?
    var fullAddress : String?
    var lat : Double?
    var lng : Double?
    var online : Bool?
    var restaurantType : RestaurantTypeDetails?
    var user : UserDetails?
    var rating : Double?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        coverPicture <- map["cover_picture"]
        fullAddress <- map["full_address"]
        lat <- map["lat"]
        lng <- map["lng"]
        online <- map["online"]
        restaurantType <- map["restaurant_type"]
        user <- map["user"]
        rating <- map["rating"]
    }
}

class RestaurantTypeDetails: Mappable {
    
    var id : Int?
    var name : String?
    var nameInAr : String?
    
    func finalName() -> String? {
        return LanguageManger.shared.currentLanguage == .ar ? nameInAr : name
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        nameInAr <- map["name_in_ar"]
    }
}
