//
//  OrderHistoryTableViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 11/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol OrderHistoryTableViewCellDelegate {
    func clickedOnView(with cell: OrderHistoryTableViewCell)
}

class OrderHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var calorieLabel: UILabel!
    @IBOutlet weak var sharedWithLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var acceptStatusLabel: UILabel!
    
    var delegate: OrderHistoryTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWith(item: InvoiceDetails) {
        restaurantNameLabel.text = item.restaurantName ?? "N/A"
        productPriceLabel.text = String(format: "%.2f \(DataManager.shared.currency.localized)", item.getTotalPrice())
        var sharedWith = ""
        let sharedWithList = item.getUserList()
        for (index, userName) in sharedWithList.enumerated() {
            sharedWith += userName
            if index != sharedWithList.count - 1 {
                sharedWith += ", "
            }
        }
        if sharedWith == "" {
            sharedWith += "N/A"
        }
        sharedWithLabel.text = "Shared With:".localized + " \(sharedWith)"
        calorieLabel.text = String(format: "%d "+"Cal".localized, item.getTotalCalorie())
        timeLabel.text = "Time: ".localized + (item.createdAt?.convertedDate().dateStringWithFormat("hh:mm a", local: false, true) ?? "N/A")
        acceptStatusLabel.text = item.hasRestaurantAccepted == false ? "Denied".localized : ""
    }
}

//MARK: Action methods
extension OrderHistoryTableViewCell {
    
    @IBAction func clickedOnView(_ sender: Any) {
        delegate?.clickedOnView(with: self)
    }
}
