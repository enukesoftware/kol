//
//  OrderListTableViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 19/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol OrderListTableViewCellDelegate {
    func clickedOnEdit(with cell: OrderListTableViewCell)
    func clickedOnCancel(with cell: OrderListTableViewCell)
}

class OrderListTableViewCell: UITableViewCell {

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceAndQuantityLabel: UILabel!
    @IBOutlet weak var calorieLabel: UILabel!
    @IBOutlet weak var sharedWithLabel: UILabel!
    @IBOutlet weak var editOrCancelView: UIView!
    
    var delegate: OrderListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWith(item: OrderItemDetails, participants: [ParticipantDetails]) {
        productNameLabel.text = item.foodItemName ?? "N/A"
        productPriceAndQuantityLabel.text = String(format: "%.2f \(DataManager.shared.currency.localized)\n%d " + "pcs".localized, item.totalPriceWithTax ?? 0, item.quantity ?? 1)
        var sharedWith = ""
        let sharedWithList = item.sharedWith ?? []
        for (index, participant) in participants.enumerated() {
            if sharedWithList.contains(participant.user?.id ?? -1) {
                sharedWith += participant.user?.name ?? "N/A"
                if index != sharedWithList.count - 1 {
                    sharedWith += ", "
                }
            }
        }
        if sharedWith == "" {
            sharedWith += "N/A"
        }
        sharedWithLabel.text = "Shared With:".localized + "\n\(sharedWith)"
        calorieLabel.text = String(format: "%d "+"Cal".localized, item.foodItemCalorie ?? 0)
        editOrCancelView.isHidden = (item.status != 0) || (item.addedBy != DataManager.shared.loginDetails?.user?.first?.id)
    }
}

//MARK: Action methods
extension OrderListTableViewCell {
    
    @IBAction func clickedOnEdit(_ sender: Any) {
        delegate?.clickedOnEdit(with: self)
    }
    
    @IBAction func clickedOnCancel(_ sender: Any) {
        delegate?.clickedOnCancel(with: self)
    }
}
