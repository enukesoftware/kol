//
//  CheckoutGroupFooterTableViewCell.swift
//  KOL
//
//  Created by Rupak on 19/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class CheckoutGroupFooterTableViewCell: UITableViewCell {

    @IBOutlet weak var contentsView: UIView!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var subtotalCalorieLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
