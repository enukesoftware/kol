//
//  ReceiptTableViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 11/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class ReceiptTableViewCell: UITableViewCell {

    @IBOutlet weak var contentsView: UIView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var calorieLabel: UILabel!
    @IBOutlet weak var sharedWithLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWith(item: OrderItemDetails, participants: [ParticipantDetails], userList: [String] = []) {
        productNameLabel.text = item.foodItemName ?? "N/A"
        productPriceLabel.text = String(format: "%.2f \(DataManager.shared.currency.localized)", item.sharedPriceWithTax ?? 0)
        productQuantityLabel.text = String(format: "%d " + "pcs".localized, item.quantity ?? 1)
        var sharedWith = ""
        if userList.count > 0 {
            let sharedWithList = userList
            for (index, userName) in sharedWithList.enumerated() {
                sharedWith += userName
                if index != sharedWithList.count - 1 {
                    sharedWith += ", "
                }
            }
        } else {
            let sharedWithList = item.sharedWith ?? []
            for (index, participant) in participants.enumerated() {
                if sharedWithList.contains(participant.user?.id ?? -1) {
                    sharedWith += participant.user?.localizedName() ?? "N/A"
                    if index != sharedWithList.count - 1 {
                        sharedWith += ", "
                    }
                }
            }
        }
        if sharedWith == "" {
            sharedWith += "N/A"
        }
        sharedWithLabel.text = "Shared With:".localized + "\n\(sharedWith)"
        calorieLabel.text = String(format: "%d "+"Cal".localized, item.foodItemCalorie ?? 0)
        timeLabel.text = "Time: ".localized + (item.createdAt?.convertedDate().dateStringWithFormat("hh:mm a", local: false, true) ?? "N/A")
    }
}
