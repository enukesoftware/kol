//
//  CategoryCollectionViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 12/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBGView: UIView!
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                titleLabel.textColor = .white
                titleBGView.backgroundColor = UIColor.hexStringToUIColor("449AA3")
            } else {
                titleLabel.textColor = UIColor.hexStringToUIColor("449AA3")
                titleBGView.backgroundColor = .white
            }
        }
    }
}
