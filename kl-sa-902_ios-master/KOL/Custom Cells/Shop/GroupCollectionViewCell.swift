//
//  GroupCollectionViewCell.swift
//  KOL
//
//  Created by Rupak on 4/8/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

protocol GroupCollectionViewCellDelegate {
    func clickedOnDelete(cell: GroupCollectionViewCell)
}

class GroupCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var profilerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            self.layer.borderColor = UIColor.hexStringToUIColor("449AA3").cgColor
            if isSelected {
                self.layer.borderWidth = 3
                closeButton.isHidden = false
            } else {
                self.layer.borderWidth = 0
                closeButton.isHidden = true
            }
        }
    }
    var delegate: GroupCollectionViewCellDelegate?
    
    @IBAction func clickedOnRemove(_ sender: Any) {
        delegate?.clickedOnDelete(cell: self)
    }
    
    func updateWith(user: GroupContactDetails) {
        if let urlString = user.profilePicture, let url = URL(string: urlString) {
            profilerImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            profilerImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
        } else {
            profilerImageView.image = UIImage.defaultProfile()
        }
        nameLabel.text = user.name
    }
}
