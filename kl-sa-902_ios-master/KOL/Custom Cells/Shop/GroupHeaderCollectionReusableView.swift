//
//  GroupHeaderCollectionReusableView.swift
//  KOL
//
//  Created by Rupak on 4/8/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol GroupHeaderCollectionReusableViewDelegate {
    func clickedOnMore(with view: GroupHeaderCollectionReusableView)
}

class GroupHeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    var indexPath: IndexPath?
    var delegate: GroupHeaderCollectionReusableViewDelegate?
    
    @IBAction func clickedOnMore(_ sender: Any) {
        delegate?.clickedOnMore(with: self)
    }
}
