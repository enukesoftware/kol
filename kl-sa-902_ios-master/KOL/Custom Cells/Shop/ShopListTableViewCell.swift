//
//  ShopListTableViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 13/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class ShopListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWith(object: RestaurantDetails) {
        logoImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        if let urlString = object.user?.profilePicture, let url = URL(string: urlString) {
            logoImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
            logoImageView.makeCircular = true
        } else {
            logoImageView.image = UIImage.defaultProfile()
            logoImageView.makeCircular = true
        }
        nameLabel.text = object.user?.localizedName()
        categoryLabel.text = object.restaurantType?.finalName()
        addressLabel.text = object.fullAddress ?? "N/A"
    }
}
