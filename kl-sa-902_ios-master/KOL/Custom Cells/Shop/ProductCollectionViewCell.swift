//
//  ProductCollectionViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 12/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var caloryLabel: UILabel!
    
    func updateWith(item: FoodItemDetails) {
        if let urlString = item.picture, let url = URL(string: urlString) {
            productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            productImageView.sd_setImage(with: url, placeholderImage: UIImage.noImage(), options: .highPriority, completed: nil)
        } else {
            productImageView.image = UIImage.noImage()
        }
        nameLabel.text = item.localizedName()
        priceLabel.text = "\(item.price ?? "N/A") " + DataManager.shared.currency.localized
        caloryLabel.text = "\(item.calorie ?? 0) " + "Cal".localized
    }
}
