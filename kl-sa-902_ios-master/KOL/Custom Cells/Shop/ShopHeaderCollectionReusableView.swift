//
//  ShopHeaderCollectionReusableView.swift
//  KOL
//
//  Created by Rupak Biswas on 12/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol ShopHeaderCollectionReusableViewDelegate {
    func clickedOnItem(with view: ShopHeaderCollectionReusableView, indexPath: IndexPath)
    func updatedScrollPosition(with view: ShopHeaderCollectionReusableView)
}

class ShopHeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var shopLogoImageView: UIImageView!
    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var shopCategoryLabel: UILabel!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var categoryList = [FoodCategoryDetails]() {
        didSet {
            categoryCollectionView.reloadData()
        }
    }
    var selectedTabIndex = 0
    var delegate: ShopHeaderCollectionReusableViewDelegate?
}

//MARK: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout related
extension ShopHeaderCollectionReusableView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as? CategoryCollectionViewCell else {
            return UICollectionViewCell.init()
        }
        cell.titleLabel.text = categoryList[indexPath.row].localizedName()
        if selectedTabIndex == indexPath.row {
            cell.isSelected = true
        } else {
            cell.isSelected = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.clickedOnItem(with: self, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = ((categoryList[indexPath.row].localizedName() ?? "N/A") as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .medium)])
        let itemWidth = size.width + 60
        return CGSize(width: itemWidth, height: collectionView.frame.size.height)
    }
}

// MARK: - UIScrollViewDelegate
extension ShopHeaderCollectionReusableView: UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (!decelerate) {
            delegate?.updatedScrollPosition(with: self)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.updatedScrollPosition(with: self)
    }
}
