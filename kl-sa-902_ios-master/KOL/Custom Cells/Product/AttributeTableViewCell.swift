//
//  AttributeTableViewCell.swift
//  KOL
//
//  Created by Rupak on 20/01/2020.
//  Copyright © 2020 Rupak. All rights reserved.
//

import UIKit

protocol AttributeTableViewCellDelegate {
    func clickedOnAttribute(with cell: AttributeTableViewCell)
}

class AttributeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    var delegate: AttributeTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickedOnAttribute(_ sender: Any) {
        delegate?.clickedOnAttribute(with: self)
    }
}
