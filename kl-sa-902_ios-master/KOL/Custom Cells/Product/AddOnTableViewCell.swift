//
//  AddOnTableViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 20/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class AddOnTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            checkImageView.image = UIImage(named: isSelected ? "check_circle" : "uncheck")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
