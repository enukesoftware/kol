//
//  GroupMemberTableViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 20/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class GroupMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var checkImageView: UIImageView!
    
    override var isSelected: Bool {
        didSet {
            checkImageView.image = UIImage(named: isSelected ? "check_circle" : "uncheck")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWith(object: ParticipantDetails) {
        if let urlString = object.user?.profilePicture, let url = URL(string: urlString) {
            profileImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            profileImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority) { (image, error, cache, url) in
                self.profileImageView.makeCircular = true
            }
        } else {
            profileImageView.image = UIImage.defaultProfile()
        }
        profileImageView.makeCircular = true
        nameLabel.text = object.user?.localizedName()
        phoneLabel.text = object.user?.phoneNumber
    }
}
