//
//  KolUsersCollectionViewCell.swift
//  KOL
//
//  Created by Rupak on 6/8/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class KolUsersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileOneImageView: UIImageView!
    @IBOutlet weak var nameOneLabel: UILabel!
    @IBOutlet weak var firstView: UIView!
    
    func updateWith(object: UserDetails) {
        profileOneImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        if let urlString = object.profilePicture, let url = URL(string: urlString) {
            profileOneImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
        } else {
            profileOneImageView.image = UIImage.defaultProfile()
        }
        nameOneLabel.text = object.localizedName()
    }
}
