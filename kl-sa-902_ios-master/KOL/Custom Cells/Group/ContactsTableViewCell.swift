//
//  ContactsTableViewCell.swift
//  KOL
//
//  Created by Rupak on 6/8/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol ContactsTableViewCellDelegate {
    func clickedOnAdd(cell: ContactsTableViewCell)
}

class ContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var delegate: ContactsTableViewCellDelegate?
    var otherKolUserType = false
    var makeSelected: Bool = false {
        didSet {
            if makeSelected {
                addButton?.setTitle("Cancel".localized, for: .normal)
                addButton?.setTitleColor(.red, for: .normal)
            } else {
                addButton?.setTitle("Add".localized, for: .normal)
                addButton?.setTitleColor(UIColor.hexStringToUIColor("4EBAC5"), for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickedOnAdd(_ sender: Any) {
        delegate?.clickedOnAdd(cell: self)
    }
    
    func updateWith(contact: ContactDetails) {
        nameLabel.text = contact.name
        numberLabel.text = contact.numbers.first
        if contact.kolUserDetails != nil {
            print("")
        }
        addButton.setTitle(contact.kolUserDetails == nil ? "ADD".localized : "INVITE".localized, for: .normal)
    }
}
