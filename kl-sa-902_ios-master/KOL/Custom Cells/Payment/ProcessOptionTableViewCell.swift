//
//  ProcessOptionTableViewCell.swift
//  KOL
//
//  Created by Rupak on 26/03/2020.
//  Copyright © 2020 Rupak. All rights reserved.
//

import UIKit

class ProcessOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var makeSelected = false {
        didSet {
            checkImageView.image = UIImage(named: makeSelected ? "check" : "uncheck")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
