//
//  PaymentTableViewCell.swift
//  KOL
//
//  Created by Rupak on 6/8/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

enum PaymentCellType {
    case person, payment
}

class PaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var paymentImageView: UIImageView!
    @IBOutlet weak var contentsView: UIView!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var payForKolView: UIView!
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                checkImageView.image = UIImage(named: type == .person ? "check_circle" : "check")
            } else {
                checkImageView.image = UIImage(named: "uncheck")
            }
        }
    }
    var type: PaymentCellType = .person
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWith(object: MemberDetails, index: Int) {
        if index == 0 {
            paymentImageView.image = UIImage(named: "kol_pay")
        } else {
            if let urlString = object.profileImage, let url = URL(string: urlString) {
                paymentImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                paymentImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority, completed: nil)
            } else {
                paymentImageView.image = UIImage.defaultProfile()
            }
        }
        paymentImageView.cornerRadius = 10
        amountLabel.text = String(format: "%.2lf \(DataManager.shared.currency.localized)", object.amount)
        titleLabel.text = object.name
    }
}
