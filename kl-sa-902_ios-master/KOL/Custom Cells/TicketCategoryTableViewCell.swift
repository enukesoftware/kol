//
//  TicketCategoryTableViewCell.swift
//  KOL
//
//  Created by Rupak Biswas on 8/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class TicketCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
