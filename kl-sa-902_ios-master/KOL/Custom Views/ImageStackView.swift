//
//  ImageStackView.swift
//  KOL
//
//  Created by Rupak Biswas on 19/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import SDWebImage

class ImageStackView: UIView {

    @IBOutlet weak var addNewButton: UIButton!
    var imageViewList: [UIImageView] = []
    var imageWidth: CGFloat = 64.0
    var leftPadding: CGFloat = 8.0
    var participantList = [ParticipantDetails]() {
        didSet {
            imageViewList = []
            for _ in 0 ..< participantList.count {
                imageViewList.append(UIImageView())
            }
            for (index, participant) in participantList.enumerated() {
                if index < imageViewList.count {
                    imageViewList[index].contentMode = .scaleAspectFill
                    imageViewList[index].clipsToBounds = true
                    if let urlString = participant.user?.profilePicture, let url = URL(string: urlString) {
                        imageViewList[index].sd_imageIndicator = SDWebImageActivityIndicator.gray
                        imageViewList[index].sd_setImage(with: url, placeholderImage: UIImage.defaultProfile(), options: .highPriority) { (image, error, cache, url) in
                            self.imageViewList[index].makeCircular = true
                        }
                    } else {
                        imageViewList[index].image = UIImage.defaultProfile()
                    }
                    imageViewList[index].makeCircular = true
                }
            }
            updateUIWithImages()
        }
    }

    func addNewImage(image: UIImage?) {
        imageViewList.append(UIImageView(image: image))
        updateUIWithImages()
    }
    
    func updateUIWithImages() {
        let totalWidth = (self.frame.size.width-CGFloat(leftPadding*2.0))-imageWidth
        var leadingSpace = totalWidth/CGFloat(imageViewList.count)
        if leadingSpace > (imageWidth/2.0) {
            leadingSpace = (imageWidth/2.0)
        }
        let topPadding = (self.frame.size.height-64)/2.0
        for (index, imageView) in imageViewList.enumerated() {
            imageView.backgroundColor = .white
            imageView.tag = index
            imageView.borderWidth = 4
            imageView.borderColor = .white
            imageView.frame = CGRect(x: CGFloat(8.0+(leadingSpace*CGFloat(index))), y: topPadding, width: imageWidth, height: imageWidth)
            imageView.makeAutoRound = true
            if self.subviews.contains(imageView) == false {
                self.addSubview(imageView)
            }
        }
        addNewButton.frame = CGRect(x: CGFloat(8.0+(leadingSpace*CGFloat(imageViewList.count))), y: topPadding, width: imageWidth, height: imageWidth)
        self.bringSubviewToFront(addNewButton)
    }
}
