//
//  ShoppingCartListView.swift
//  KOL
//
//  Created by Rupak on 17/10/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol ShoppingCartListViewDelegate {
    func clickedOnEdit(with view: ShoppingCartListView, indexPath: IndexPath)
    func clickedOnCancel(with view: ShoppingCartListView, indexPath: IndexPath)
}

class ShoppingCartListView: UIView {

    @IBOutlet weak var orderListTableView: UITableView!
    @IBOutlet weak var listBackButton: UIButton!
    @IBOutlet weak var listTopIndicatorView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var orderItemCountLabel: UILabel!
    
    var delegate: ShoppingCartListViewDelegate?
    var orderItemList = [OrderItemDetails]() {
        didSet {
//            orderListTableView.reloadData()
            orderListGroups = []
            var items = orderItemList.filter({$0.status == 0})
            if items.count > 0 {
                orderListGroups.append(items)
            }
            items = orderItemList.filter({$0.status == 1})
            if items.count > 0 {
                orderListGroups.append(items)
            }
            orderListTableView.reloadData()
        }
    }
    var orderListGroups = [[OrderItemDetails]]()
    var participantList = [ParticipantDetails]()
}

//MARK: UITableViewDataSource, UITableViewDelegate methods
extension ShoppingCartListView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return orderListGroups.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderListGroups[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderListTableViewCell") as? OrderListTableViewCell else {
            return UITableViewCell.init()
        }
        
        cell.selectionStyle = .none
        cell.delegate = self
        cell.updateWith(item: orderListGroups[indexPath.section][indexPath.row], participants: participantList)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = Bundle.main.loadNibNamed("ShoppingCartItemsHeaderView", owner: nil, options: [:])?.first as? ShoppingCartItemsHeaderView {
            headerView.totalAmountView.isHidden = true
            if orderListGroups.count == 1 {
                headerView.titleLabel.isHidden = true
            } else if orderListGroups.count == 2 {
                headerView.titleLabel.isHidden = false
                headerView.titleLabel.text = section == 0 ? "New Item" : "Confirmed Item"
            }
            return headerView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let headerView = Bundle.main.loadNibNamed("ShoppingCartItemsHeaderView", owner: nil, options: [:])?.first as? ShoppingCartItemsHeaderView {
            headerView.amountLabel.text = String(format: "%.2f "+"SAR".localized, getPriceWith(items: orderListGroups[section]))
            return headerView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return orderListGroups.count > 1 ? 44 : 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
}

//MARK: - OrderListTableViewCellDelegate methods
extension ShoppingCartListView: OrderListTableViewCellDelegate {
    
    func clickedOnEdit(with cell: OrderListTableViewCell) {
        guard let indexPath = orderListTableView.indexPath(for: cell) else {return}
        delegate?.clickedOnEdit(with: self, indexPath: indexPath)
    }
    
    func clickedOnCancel(with cell: OrderListTableViewCell) {
        guard let indexPath = orderListTableView.indexPath(for: cell) else {return}
        delegate?.clickedOnCancel(with: self, indexPath: indexPath)
    }
}

//MARK: Other methods
extension ShoppingCartListView {
    
    func getPriceWith(items: [OrderItemDetails]) -> Double {
        var amount = 0.0
        for item in items {
            amount += item.totalPriceWithTax ?? 0
        }
        return amount
    }
}
