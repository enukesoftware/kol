//
//  MapMarkerWindow.swift
//  KOL
//
//  Created by Rupak Biswas on 14/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol MapMarkerWindowDelegate: class {
    func clickedInside(with view: MapMarkerWindow, data: NSDictionary?)
}

class MapMarkerWindow: UIView {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var restaurentTypeLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    weak var delegate: MapMarkerWindowDelegate?
    var spotData: NSDictionary?
    
    @IBAction func clickedInside(_ sender: Any) {
        delegate?.clickedInside(with: self, data: spotData)
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapMarkerWindow", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
}
