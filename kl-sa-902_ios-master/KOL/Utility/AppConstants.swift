//
//  AppConstants.swift
//  KOL
//
//  Created by Rupak on 17/08/2019.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

struct AppConstants {
    
    struct Credentials {
        static let googlePlaceApiKey = "AIzaSyDG9v9UPIbYdRsHx30ZLAZoFDYHgM-3Eh0"
    }
    
    struct DateFormate {
        static let Default = "yyyy-MM-dd HH:mm:ss"
        static let Database = "yyyy-MM-ddTHH:mm:ss"
    }
    
    struct BaseURL {
        //static let Development = "https://api.kol.com.sa"
    static let Development = "https://kol.com.sa"

       // static let Development = "http://13.49.89.106"

        static let Current = BaseURL.Development
    }
    
    struct ApplicationXIBs {
        static let kPTSRLauncherView = "PTSRLauncherView"
        
        static let kPTFWInitialSetupView = "PTFWInitialSetupView"
    }
    
    struct ApplicationResources {
        static let kFrameworkResourcesBundle = "Resources"
    }
    
    struct Api {
        //Auth
        struct Auth {
            static let login = "/api/auth/api-token-auth/"
            static let regitration = "/api/account/customer/"
            static let verifyAccount = "/api/account/verify-phone-number/"
            static let refreshToken = "/api/auth/api-token-refresh/"
            static let userDetails = "/api/account/customer/%d/"
        }
        //User
        struct User {
            static let update = "/api/account/customer/%d/"
            static let forgotPassword = "/api/account/forgot-password/"
            static let forgotPasswordVerifyAccount = "/api/account/verify-phone-number/"
            static let resendVerificationCode = "/api/account/resend-verification-code/"
            static let resetPassword = "/api/account/reset-password/"
            static let changePassword = "/api/account/change-password/"
            static let changePhone = "/api/account/change-phone-number/"
            static let changePhoneVerify = "/api/account/verify-new-phone-number/"
        }
        //Ticket
        struct Ticket {
            static let list = "/api/customer-ticket-topics/"
            static let raise = "/api/customer-ticket/"
        }
        //Restaurant
        struct Restaurant {
            static let listByLocation = "/api/account/restaurant/?lat=%lf&lng=%lf&limit=500"
            static let listByKeyword = "/api/account/restaurant/?name=%@&listing=True&limit=100&offset=0"
            static let tableInfo = "/api/restaurant-table/%d/"
            static let foodItems = "/api/food/item/?user=%d"
            static let foodItemDetails = "/api/food/item/%d/"
            static let foodCategories = "/api/food/category/?user=%d&limit=50&offset=0"
            static let details = "/api/account/restaurant/%d/"
        }
        //Order
        struct Order {
            static let create = "/api/order/"
            static let details = "/api/order/%d/"
            static let participants = "/api/order-participant/"
            static let activeOrder = "/api/account/customer-misc/"
            static let invite = "/api/order-invite/"
            static let leave = "/api/order/%d/leave/"
            static let acceptInvitation = "/api/order-invite/%@/"
            static let addToCart = "/api/order-item/"
            static let updateCartItem = "/api/order-item/%d/"
            static let items = "/api/order-item/?order=%d&limit=50"
            static let confirmOrder = "/api/order/%d/confirm_current_items/"
            static let orderItemInvite = "/api/order-item-invite/"
            static let acceptOrderItem = "/api/order-item-invite/%@/"
            static let cancelOrderItem = "/api/order-item/%d/"
            static let rating = "/api/order-rating/"
        }
        struct Invoice {
            static let create = "/api/invoice/"
            static let details = "/api/invoice/%d/"
            static let transactionDetails = "/api/transaction/"
            static let transactionVerification = "/api/invoice-transaction-verification/"
            static let list = "/api/invoice/?limit=100&offset=0"
            static let reportIssue = "/api/report-issue/"
        }
        //Contact
        struct Contact {
            static let sync = "/api/contact-list/"
            static let groupList = "/api/contact-group/"
            static let createGroup = "/api/contact-group/"
            static let addContacts = "/api/contact-group/%d/contacts/"
            static let deleteGroup = "/api/contact-group/%d/"
            static let updateGroupContacts = "/api/contact-group/%d/contacts/"
        }
        struct Payment {
            static let list = "/api/v1/me/payments/"
            static let details = "/api/v1/lease/%d/payments/"
            static let verify = "/api/v1/me/payments/verify/"
        }
        
        struct General {
            static let countryList = "/api/v1/location/countries/"
            static let cityList = "/api/v1/location/cities/?Country=%d"
            //notification
            static let registerNotification = "/api/account/fcm/"
            static let updateNotification = "/api/account/fcm/%@/"
            static let deleteNotification = "/api/account/fcm/"
            //Review
            static let reviewList = "/api/review/?on=%d"
            static let addReview = "/api/review/"
        }
    }
}
