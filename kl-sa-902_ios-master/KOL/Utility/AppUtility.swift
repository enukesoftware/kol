//
//  AppUtility.swift
//  KOL
//
//  Created by Rupak Biswas on 2/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit
import MBProgressHUD
import FirebaseMessaging
import IQKeyboardManagerSwift

class AppUtility: NSObject {
    
    class func showProgress(_ view:  UIView? = nil, title: String?){
        var hud: MBProgressHUD?
        if view != nil {
            hud = MBProgressHUD.showAdded(to: view!, animated: true)
        }else {
            if let window = UIApplication.shared.keyWindow {
                hud = MBProgressHUD.showAdded(to: window, animated: true)
            } else if let window = UIApplication.shared.windows.last {
                hud = MBProgressHUD.showAdded(to: window, animated: true)
            }
        }
        
        hud?.bezelView.color = UIColor.black.withAlphaComponent(0.4)
        hud?.bezelView.style = .solidColor
        hud?.label.textColor = UIColor.white
        hud?.contentColor = .white
        hud?.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        
        if title != nil {
            hud!.label.text = title
        }
        hud!.show(animated: true)
    }
    
    class func hideProgress(_ view: UIView? = nil){
        if view != nil {
            MBProgressHUD.hide(for: view!, animated: true)
        }else {
            if let window = UIApplication.shared.keyWindow {
                MBProgressHUD.hide(for: window, animated: true)
            } else if let window = UIApplication.shared.windows.last {
                MBProgressHUD.hide(for: window, animated: true)
            }
        }
    }
    
    class func showAlertWithProperty(_ title: String, messageString: String) {
        let alertController = UIAlertController.init(title: title, message: messageString, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok".localized, style: .cancel, handler: nil))
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
    
    class func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    class func hasNoSpecialCharacter(text: String) -> Bool {
        let specialRegEx = "\\p{L}+(?:[\\W_]\\p{L}+)*"
        
        let stringTest = NSPredicate(format:"SELF MATCHES %@", specialRegEx)
        return stringTest.evaluate(with: text)
    }
    
    class func isStrongPassword(text: String) -> Bool {
        let regex = "(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@\\$%^&*-]).{8,}"
        
        let stringTest = NSPredicate(format:"SELF MATCHES %@", regex)
        return stringTest.evaluate(with: text)
    }
    
    class func isValidPhone(phone: String) -> Bool {
        let phoneRegEx = "^((\\+)|(00))[0-9]{6,14}$"
        
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: phone)
    }
    
    class func matchedWithTopViewControllerType(classType: AnyClass) -> Bool {
        return (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.children.last?.isKind(of: classType) ?? false
    }
    
    class func changeLanguage() {
        UIView.appearance().semanticContentAttribute = (LanguageManger.shared.currentLanguage == .ar) ? .forceRightToLeft : .forceLeftToRight
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.instantiateAppropriateViewController()
        UserDefaults.standard.set(LanguageManger.shared.currentLanguage == .ar ? "ar" : "en", forKey: "currentLanguage")
//        if DataManager.manager.fullToken != "JWT " {
//            TaskManager.updateLanguage(showLoading: false) { (success) in}
//        }
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized
    }
    
    class func isiPad() -> Bool {
        if UIDevice.current.model == "iPad" || UIDevice.current.model == "iPad Simulator" {
            return true
        }
        return false
    }
    
    class func isSimulator() -> Bool {
        var simulatorStatus = false
        #if targetEnvironment(simulator)
        simulatorStatus = true
        #endif
        return simulatorStatus
    }
    
    class func isDebugMode() -> Bool {
        if DEBUG == 1 {
            return true
        }
        return false
    }
    
    static func getPhoneWithCountryCode(value: String) -> String {
        if value.count >= 10 {
            let finalValue = value.suffix(10)
            return ""+finalValue
        }
        return ""
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func isValidUrl (urlString: String?) -> Bool {
        if let urlString = urlString, let url = URL(string: urlString) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
}

//MARK: Notification methods
extension AppUtility {
    
    class func subscribeNotification() {
        if let refreshedToken = Messaging.messaging().fcmToken, let deviceId = UIDevice.current.identifierForVendor?.uuidString {
            
            let params: [String:Any] = ["registration_id" : refreshedToken,
                                        "device_id" : deviceId,
                                        "type" : "ios",
                                        "active" : 1]
            
            RequestManager.subscribeNotification(params: params, completion: { (fcmDetails) in
                print("Register notification successfully")
                DataManager.shared.fcmDetails = fcmDetails
                if fcmDetails == nil {
                    getFCMDetails()
                }
            }) { (error, code, message) in
                print("Failed to register.")
                getFCMDetails()
            }
        }
    }
    
    class func unsubscribeNotification() {
        if let registerId = DataManager.shared.fcmDetails?.registrationId {
            RequestManager.unsubscribeNotification(registerId: registerId, completion: { (success) in
                print("Unsubscribed notification successfully")
            }) { (error, code, message) in
                print("Failed to delete reg id.")
            }
        }
    }
    
    class func getFCMDetails() {
        if let refreshedToken = Messaging.messaging().fcmToken {
            RequestManager.getFCMDetails(id: refreshedToken, completion: { (fcmDetails) in
                DataManager.shared.fcmDetails = fcmDetails
            }) { (error, code, message) in
                print("Failed to get fcm details.")
            }
        }
    }
    
    class func updateNotification(isActive: Bool = true, _ completion: @escaping(_ success: Bool) -> Void) {
        if let refreshedToken = Messaging.messaging().fcmToken, let deviceId = UIDevice.current.identifierForVendor?.uuidString {
            
            let updateObject: [String:Any] = ["registration_id" : refreshedToken,
                                                "device_id" : deviceId,
                                                "type" : "ios",
                                                "active" : isActive]
            
            RequestManager.updateNotificationStatus(id: refreshedToken, bodyObject: updateObject, completion: { (fcmDetails) in
                print("Updated notification successfully")
                DataManager.shared.fcmDetails = fcmDetails
                completion(true)
            }) { (error, code, message) in
                completion(false)
                print("Failed to update.")
            }
        }
    }
}

//MARK: QR code methods
extension AppUtility {
    
    class func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
}

//MARK: Navigation methods
extension AppUtility {
    
    static func navigationContains(controllerClass: AnyClass) -> Bool {
        var isContains = false
        for controller in UIApplication.topViewController()?.navigationController?.viewControllers ?? [] {
            if controller.isKind(of: controllerClass) {
                isContains = true
                break
            }
        }
        return isContains
    }
}
