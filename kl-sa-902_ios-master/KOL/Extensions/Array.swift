//
//  Array.swift
//  KOL
//
//  Created by Rupak Biswas on 20/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

extension Array where Element: Equatable {
    
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }
}
