//
//  CodeNumberInputView.swift
//  KOL
//
//  Created by Rupak Biswas on 5/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

protocol CodeNumberInputViewDelegate {
    func otpAutofilledWith(view: CodeNumberInputView?)
}

class CodeNumberInputView: UIView {
    
    @IBInspectable var inputFieldCount: Int = 0
    var delegate: CodeNumberInputViewDelegate?
}

//MARK: UITextFieldDelegate methods
extension CodeNumberInputView: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (self.viewWithTag(1) as? UITextField)?.text?.count == 0 {
            (self.viewWithTag(1) as? UITextField)?.becomeFirstResponder()
        }
        if textField.text!.count < 1  && string.count > 0 {
            let nextTag = textField.tag + 1
            // get next responder
            var nextResponder = textField.superview?.viewWithTag(nextTag)
            if (nextResponder == nil){
                nextResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = string
            if nextTag > inputFieldCount {
                textField.resignFirstResponder()
                delegate?.otpAutofilledWith(view: self)
            } else {
                nextResponder?.becomeFirstResponder()
            }
            return false
        } else if textField.text!.count >= 1  && string.count == 0 {
            let previousTag = textField.tag - 1
            // get next responder
            var previousResponder = textField.superview?.viewWithTag(previousTag)
            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = ""
            previousResponder?.becomeFirstResponder()
            return false
        } else if textField.text?.count == 1 {
            let nextTag = textField.tag + 1
            var nextResponder = textField.superview?.viewWithTag(nextTag)
            if (nextResponder == nil){
                nextResponder = textField.superview?.viewWithTag(1)
            }
            if nextTag > inputFieldCount {
                textField.resignFirstResponder()
            } else {
                nextResponder?.becomeFirstResponder()
            }
            return false
        }
        return true
    }
}

//MARK: Custom methods
extension CodeNumberInputView {
    
    func updateResponderWith(tag: Int, becomeFirstResponder: Bool) {
        if becomeFirstResponder {
            (self.viewWithTag(1) as? UITextField)?.becomeFirstResponder()
        } else {
            (self.viewWithTag(1) as? UITextField)?.resignFirstResponder()
        }
    }
    
    func getPinNumber() -> String {
        var pinNumber = ""
        for i in 1...6 {
            if let textField = self.viewWithTag(i) as? UITextField, let text = textField.text {
                pinNumber += text
            }
        }
        return pinNumber
    }
    
    func emptyPinNumber() {
        
        for i in 1...6 {
            if let textField = self.viewWithTag(i) as? UITextField {
                textField.text = ""
            }
        }
    }
}
