//
//  Storyboard.swift
//  KOL
//
//  Created by Rupak Biswas on 4/7/19.
//  Copyright © 2019 Rupak. All rights reserved.
//

import UIKit

class Storyboard: UIStoryboard {
    
    class func controllerWith(name: String) -> UIViewController? {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
    }
}

